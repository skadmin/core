@echo off

setlocal
SET parent=%~dp0
FOR %%a IN ("%parent:~0%") DO SET grandparent=%%~dpa

title PROJECT UPDATER 2.0 - %grandparent%

set conf=12

call echo Project home:: %%grandparent%%
echo --------------------
echo Updater menu
echo Docker
echo - up: 1
echo - down-build-up: 9
echo Composer
echo - update: 2
echo - install: 3
echo Migration
echo - diff: 4
echo - generate: 5
echo - validate schema: 6
echo Testing
echo - phpstan: q
echo - phpcs: w
echo - phpcbf: e
echo --------------------
echo For composer update - simply press enter
set /p conf= Enter the selected configuration (e.g. 3 for composer update):

:rozcestnik
if not defined conf goto konec
if %conf%==0 goto default
if %conf:~0,1%==1 goto dockerUp
if %conf:~0,1%==9 goto dockerDownBuildUp
if %conf:~0,1%==2 goto composerUpdate
if %conf:~0,1%==3 goto composerInstall
if %conf:~0,1%==4 goto migrationDiff
if %conf:~0,1%==5 goto migrationGenerate
if %conf:~0,1%==6 goto ormValidateSchema

if %conf:~0,1%==q goto testingPhpstan
if %conf:~0,1%==w goto testingPhpcs
if %conf:~0,1%==e goto testingPhpcbf

:default
set conf=1
goto rozcestnik

:dockerUp
set conf=%conf:~1%
echo.
echo ----------------- Executing docker up
docker-compose up -d
goto rozcestnik

:dockerDownBuildUp
set conf=%conf:~1%
echo.
echo ----------------- Executing docker down - build - up
docker-compose down && docker-compose build && docker-compose up -d
goto rozcestnik

:composerUpdate
set conf=%conf:~1%
echo.
echo ----------------- Executing composer update
docker-compose exec -T web php /var/www/html/composer.phar update
goto rozcestnik

:composerInstall
set conf=%conf:~1%
echo.
echo ----------------- Executing composer install
docker-compose exec -T web php /var/www/html/composer.phar install
goto rozcestnik

:migrationDiff
set conf=%conf:~1%
echo.
echo ----------------- Executing migration diff
docker-compose exec -T web php /var/www/html/bin/console.php migration:diff
goto rozcestnik

:migrationGenerate
set conf=%conf:~1%
echo.
echo ----------------- Executing migration generate
docker-compose exec -T web php /var/www/html/bin/console.php migration:generate
goto rozcestnik

:ormValidateSchema
set conf=%conf:~1%
echo.
echo ----------------- Executing orm validate schema
docker-compose exec -T web php /var/www/html/bin/console.php orm:validate-schema
goto rozcestnik

:testingPhpstan
set conf=%conf:~1%
echo.
echo ----------------- Testing phpstan
docker-compose exec -T web php vendor/bin/phpstan analyse -c tests/phpstan.neon
goto rozcestnik

:testingPhpcs
set conf=%conf:~1%
echo.
echo ----------------- Testing phpcs
docker-compose exec -T web php vendor/bin/phpcs
goto rozcestnik

:testingPhpcbf
set conf=%conf:~1%
echo.
echo ----------------- Testing phpcbf
docker-compose exec -T web php vendor/bin/phpcbf
goto rozcestnik

:konec
echo.
echo.------------------------------------------------------