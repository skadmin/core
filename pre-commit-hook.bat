@echo off

echo ---------
echo ---------
echo Run command 'vendor\bin\phpcbf' for automatic fix
echo Spuste prikaz 'vendor\bin\phpcbf' pro automatickou opravu
echo ---------
echo ---------
echo run: vendor\bin\tester tests\tests
echo run: vendor\bin\phpcbf
echo run: vendor\bin\phpstan analyse -l 7 -c tests\phpstan.neon app
echo run: vendor\bin\phpcs
echo ---------
echo ---------

:: vendor\bin\tester tests\tests && vendor\bin\phpcbf && vendor\bin\phpstan analyse -l 7 -c tests\phpstan.neon app && vendor\bin\phpcs
vendor\bin\tester tests\tests && vendor\bin\phpcbf && vendor\bin\phpcs