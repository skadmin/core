<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190729102025 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE privilege ADD p_read TINYINT(1) DEFAULT \'0\' NOT NULL, ADD p_write TINYINT(1) DEFAULT \'0\' NOT NULL, ADD p_delete TINYINT(1) DEFAULT \'0\' NOT NULL, DROP `delete`, DROP `read`, DROP `write`');
        $this->addSql('ALTER TABLE role CHANGE is_locked is_locked TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_active is_active TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE is_locked is_locked TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_active is_active TINYINT(1) DEFAULT \'1\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE privilege ADD `delete` INT DEFAULT 0 NOT NULL, ADD `read` INT DEFAULT 0 NOT NULL, ADD `write` INT DEFAULT 0 NOT NULL, DROP p_read, DROP p_write, DROP p_delete');
        $this->addSql('ALTER TABLE role CHANGE is_locked is_locked INT DEFAULT 0 NOT NULL, CHANGE is_active is_active INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE is_locked is_locked INT DEFAULT 0 NOT NULL, CHANGE is_active is_active INT DEFAULT 1 NOT NULL');
    }
}
