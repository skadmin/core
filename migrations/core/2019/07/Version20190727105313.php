<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190727105313 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE privilege (id INT AUTO_INCREMENT NOT NULL, role_id INT DEFAULT NULL, resource_id INT DEFAULT NULL, `read` INT DEFAULT 0 NOT NULL, `write` INT DEFAULT 0 NOT NULL, `delete` INT DEFAULT 0 NOT NULL, INDEX IDX_87209A87D60322AC (role_id), INDEX IDX_87209A8789329D25 (resource_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, is_master_admin INT DEFAULT 0 NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, is_locked INT DEFAULT 0 NOT NULL, is_active INT DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resource (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, user_name VARCHAR(255) NOT NULL, user_password VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, is_locked INT DEFAULT 0 NOT NULL, is_active INT DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role_rel (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_A055B7CFA76ED395 (user_id), INDEX IDX_A055B7CFD60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE privilege ADD CONSTRAINT FK_87209A87D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE privilege ADD CONSTRAINT FK_87209A8789329D25 FOREIGN KEY (resource_id) REFERENCES resource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role_rel ADD CONSTRAINT FK_A055B7CFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role_rel ADD CONSTRAINT FK_A055B7CFD60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');

        // USERS
        $users = [
            [
                'user_name'     => 'skala2524@gmail.com',
                'user_password' => '$2y$10$8jkBK..iz0AFIANC1vlnc..GlmYBQ2pa7N.mfoABrjR3uf45JkU/u',
                'name'          => 'David',
                'surname'       => 'Skála',
                'email'         => 'skala2524@gmail.com',
                'phone'         => '724316086',
                'is_locked'     => 1,
            ],
        ];

        foreach ($users as $user) {
            $this->addSql(
                'INSERT INTO user (user_name, user_password, name, surname, email, phone, is_locked) VALUES (:user_name, :user_password, :name, :surname, :email, :phone, :is_locked)',
                $user
            );
        }

        // ROLES
        $roles = [
            [
                'name'            => 'Master admin',
                'webalize'        => 'master-admin',
                'is_locked'       => 1,
                'is_master_admin' => 1,
            ],
            [
                'name'            => 'Admin',
                'webalize'        => 'admin',
                'is_locked'       => 0,
                'is_master_admin' => 0,
            ],
        ];

        foreach ($roles as $role) {
            $this->addSql('INSERT INTO role (name, webalize, is_locked, is_master_admin) VALUES (:name, :webalize, :is_locked, :is_master_admin)', $role);
        }

        // USER_ROLE_REL
        $userRoleRels = [
            [
                'user_id' => 1,
                'role_id' => 1,
            ],
        ];

        foreach ($userRoleRels as $userRoleRel) {
            $this->addSql('INSERT INTO user_role_rel (user_id, role_id) VALUES (:user_id, :role_id)', $userRoleRel);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE privilege DROP FOREIGN KEY FK_87209A87D60322AC');
        $this->addSql('ALTER TABLE user_role_rel DROP FOREIGN KEY FK_A055B7CFD60322AC');
        $this->addSql('ALTER TABLE privilege DROP FOREIGN KEY FK_87209A8789329D25');
        $this->addSql('ALTER TABLE user_role_rel DROP FOREIGN KEY FK_A055B7CFA76ED395');
        $this->addSql('DROP TABLE privilege');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE resource');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_role_rel');
    }
}
