<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805075322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE section (id INT AUTO_INCREMENT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting (id INT AUTO_INCREMENT NOT NULL, section_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_9F74B898D823E37A (section_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE setting ADD CONSTRAINT FK_9F74B898D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE');

        $sections = [
            [
                'id'          => 1,
                'name'        => 'setting-section.google.name',
                'webalize'    => 'google',
                'description' => 'setting-section.google.description',
            ],
        ];

        foreach ($sections as $section) {
            $this->addSql('INSERT INTO section (id, webalize, name, description) VALUES (:id, :webalize, :name, :description)', $section);
        }

        $settings = [
            [
                'section_id'  => 1,
                'name'        => 'Google Analytics',
                'code'        => 'googleAnalytics',
                'value'       => '',
                'description' => 'setting-setting.google-analytics.description',
            ],
            [
                'section_id'  => 1,
                'name'        => 'Google Tag Manager',
                'code'        => 'googleTagManager',
                'value'       => '',
                'description' => 'setting-setting.google-tag-manager.description',
            ],
        ];

        foreach ($settings as $setting) {
            $this->addSql('INSERT INTO setting (section_id, name, code, value, description) VALUES (:section_id, :name, :code, :value, :description)', $setting);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting DROP FOREIGN KEY FK_9F74B898D823E37A');
        $this->addSql('DROP TABLE section');
        $this->addSql('DROP TABLE setting');
    }
}
