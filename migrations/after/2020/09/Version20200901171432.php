<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901171432 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'form.url.rule.not-found-domain - %s','hash' => '532bed1e3b5a044dd82b04e9860f1f53','module' => 'admin','language_id' => 1,'singular' => 'Odkaz musí obsahovat - %s','plural1' => '','plural2' => ''],
            ['original' => 'form.url.rule.not-found-protocol','hash' => 'ce969f02acfa7e035b663e352f05ee5c','module' => 'admin','language_id' => 1,'singular' => 'Nebyl nalezen protokol - [http, https]','plural1' => '','plural2' => ''],
            ['original' => 'form.dropzone.default','hash' => '32a52e49eb60f26b4bda930cc1008414','module' => 'admin','language_id' => 1,'singular' => 'klikněte nebo přetáhněte soubory sem','plural1' => '','plural2' => ''],
            ['original' => 'privilege.publish', 'hash' => 'ffe416411b75e82f99c9dd681dea759f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'publikování', 'plural1' => '', 'plural2' => ''],
            ['original' => 'privilege.lock', 'hash' => 'eee9a742273356c16dbbff1c85ad32af', 'module' => 'admin', 'language_id' => 1, 'singular' => 'zamykání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'privilege.tags','hash' => '3842c714031233ed9594e61ef394f272','module' => 'admin','language_id' => 1,'singular' => 'štítky','plural1' => '','plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
