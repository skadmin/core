<?php

declare(strict_types=1);

namespace Migrations;

use App\Model\Mail\Type\CMailRecoveryPassword;
use App\Model\Mail\Type\CMailRegistration;
use App\Model\Mail\Type\CMailUserAuthorize;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190821092424 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailRecoveryPassword::getModelForSerialize()),
                'type'               => CMailRecoveryPassword::TYPE,
                'class'              => CMailRecoveryPassword::class,
                'name'               => 'mail.recovery-password.name',
                'subject'            => 'mail.recovery-password.subject',
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
            [
                'content'            => '',
                'parameters'         => serialize(CMailRegistration::getModelForSerialize()),
                'type'               => CMailRegistration::TYPE,
                'class'              => CMailRegistration::class,
                'name'               => sprintf('mail.%s.name', CMailRegistration::TYPE),
                'subject'            => sprintf('mail.%s.subject', CMailRegistration::TYPE),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
            [
                'content'            => '',
                'parameters'         => serialize(CMailUserAuthorize::getModelForSerialize()),
                'type'               => CMailUserAuthorize::TYPE,
                'class'              => CMailUserAuthorize::class,
                'name'               => sprintf('mail.%s.name', CMailUserAuthorize::TYPE),
                'subject'            => sprintf('mail.%s.subject', CMailUserAuthorize::TYPE),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql('INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
                $mailTemplate);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
