$.nette.ext({
    success: function () {
        appInitSkadminForms();
    },
    init: function () {
        appInitSkadminForms();
    }
});


class SkadminForms {
    constructor(elementId) {
        this.elementId = elementId;
        this.root = document.getElementById(this.elementId);
    }

    // private function
    _formText(type, key, value, classInput, attrs = []) {
        let inputId = this.elementId + '__' + key

        let attrsString = [];
        $.each(attrs, function (in_key, in_value) {
            attrsString.push(in_key + '="' + in_value + '"');
        });

        let label = '<label for="' + inputId + '">' + key + '</label>';
        let input = '<input type="' + type + '" id="' + inputId + '" name="' + key + '" value="' + value + '" class="' + classInput + '" ' + attrsString.join(' ') + '>';
        return label + input
    }

    _formSelect(key, value, options, classInput, attrs = []) {
        let inputId = this.elementId + '__' + key

        let attrsString = [];
        $.each(attrs, function (in_key, in_value) {
            attrsString.push(in_key + '="' + in_value + '"');
        });

        let htmlOptions = '';
        $.each(options, function (in_key, in_value) {
            if (value == in_key) {
                htmlOptions += '<option value="' + in_key + '" selected="selected">' + in_value + '</option>';
            } else {
                htmlOptions += '<option value="' + in_key + '">' + in_value + '</option>';
            }
        });

        let label = '<label for="' + inputId + '">' + key + '</label>';
        let input = '<select id="' + inputId + '" name="' + key + '" class="' + classInput + '" ' + attrsString.join(' ') + '>' + htmlOptions + '</select>';
        return label + input
    }

    // public function
    appendHtml(html) {
        this.root.outerHTML += html;
    }

    addColor(key, value = '', divRenderAndClass = 'form-group', classInput = 'form-control') {
        let attrs = {
            'data-create-colorpicker': '',
            'data-format': 'hex'
        };

        if (divRenderAndClass) {
            var inputHtml = '<div class="' + divRenderAndClass + '">' + this._formText('text', key, value, classInput, attrs) + '</div>';
        } else {
            var inputHtml = this._formText('text', key, value, classInput, attrs);
        }

        return inputHtml;
    }

    addText(key, value = '', insertDiv = true, classInput = 'form-control') {
        if (insertDiv) {
            var inputHtml = '<div class="form-group">' + this._formText('text', key, value, classInput) + '</div>';
        } else {
            var inputHtml = this._formText('text', key, value, classInput);
        }

        return inputHtml;
    }

    addSelect(key, value = '', options = [], divRenderAndClass = 'form-group', classInput = 'form-control') {
        if (divRenderAndClass) {
            var inputHtml = '<div class="' + divRenderAndClass + '">' + this._formSelect(key, value, options, classInput) + '</div>';
        } else {
            var inputHtml = this._formSelect(key, value, options, classInput);
        }

        return inputHtml;
    }

    getNameAddFunction = (s) => {
        if (typeof s !== 'string') return ''
        return 'add' + s.charAt(0).toUpperCase() + s.slice(1)
    }
}

function appInitSkadminForms() {
    const SEPARATOR_VALUE = '::::';
    const SEPARATOR_ITEM = ':!!:';

    $.nette.ext({
        success: function () {
            skadminFormInit();
        },
        init: function () {
            skadminFormInit();
        }
    });

    function skadminFormInit() {
        $('[data-inline-array]:not(.inline-array-init)').each(function () {
            let $that = $(this);
            $that.addClass('inline-array-init');

            let inputId = $that.attr('id')
            let $input = $('#' + inputId);
            let options = SKADMIN_FORM_OPTIONS[inputId]
            let form = new SkadminForms(inputId);

            // parse default value
            let defaultValue = [];
            $.each($input.val().split(SEPARATOR_ITEM), function (i, val) {
                let defVal = val.split(SEPARATOR_VALUE);
                defaultValue[defVal[0]] = defVal[1];
            });

            let html = '';
            $.each(options, function (key, type) {
                if (typeof type == 'object') {
                    let className = form.getNameAddFunction(type.type);

                    if (typeof form[className] == 'function') {
                        if (type.type == 'select') {
                            html += form.addSelect(key, defaultValue[key], type.options, type.div);
                        } else {
                            html += form[className](key, defaultValue[key], type.div);
                        }
                    }
                } else {
                    let className = form.getNameAddFunction(type);

                    if (typeof form[className] == 'function') {
                        html += form[className](key, defaultValue[key]);
                    }
                }
            });
            form.appendHtml(html);

            $('[id^="' + inputId + '__"]')
                .unbind('change')
                .on('change', function () {
                    let newValue = [];
                    $('[id^="' + inputId + '__"]').each(function () {
                        newValue.push($(this).attr('name') + SEPARATOR_VALUE + $(this).val());
                    });
                    $('#' + inputId).val(newValue.join(SEPARATOR_ITEM));
                });

        });
    }
}