// https://www.martyfriedel.com/blog/tinymce-5-creating-a-plugin-with-a-dialog-and-custom-icons
// https://www.tiny.cloud/docs/ui-components/dialog/#configurationoptions
(function () {
    var smartTemplate = (function () {
        'use strict';
        tinymce.PluginManager.add("smartTemplate", function (editor) {
            var selectType = {
                type: 'selectbox',
                name: 'type',
                label: 'Typ',
                items: [
                    {text: 'Řádek', value: 'row'},
                    {text: 'Sloupec', value: 'col'}
                ],
                flex: true
            };

            var selectCol = {
                type: 'selectbox',
                name: 'col',
                label: 'Typ sloupce',
                items: [
                    {text: '-', value: ''},
                    {text: 'sm', value: 'sm'},
                    {text: 'md', value: 'md'},
                    {text: 'lg', value: 'lg'},
                    {text: 'xl', value: 'xl'}
                ],
                flex: true
            };

            var selectSizeCol = {
                type: 'selectbox',
                name: 'colSize',
                label: 'Šířka sloupce',
                items: [
                    {text: '-', value: ''},
                    {text: '1', value: '1'},
                    {text: '2', value: '2'},
                    {text: '3', value: '3'},
                    {text: '4', value: '4'},
                    {text: '5', value: '5'},
                    {text: '6', value: '6'},
                    {text: '7', value: '7'},
                    {text: '8', value: '8'},
                    {text: '9', value: '9'},
                    {text: '10', value: '10'},
                    {text: '11', value: '11'},
                    {text: '12', value: '12'}
                ],
                flex: true
            };

            var _bodyItems = [selectType];
            var _itemsData = [];

            function _onAction() {
                editor.windowManager.open({
                    title: 'Vložení šablony',
                    body: {
                        type: 'panel',
                        items: _bodyItems
                    },
                    initialData: _itemsData,
                    onChange: (api, el) => {
                        if (el.name === 'type') {
                            _itemsData = [];
                            _bodyItems = [selectType];
                            if (api.getData().type === 'col') {
                                _bodyItems.push(selectCol);
                                _bodyItems.push(selectSizeCol);
                            }

                            _itemsData['type'] = api.getData().type;
                            api.close();
                            _onAction();
                        } else if (el.name === 'col') {
                            _itemsData['col'] = api.getData().col;
                        } else if (el.name === 'colSize') {
                            _itemsData['colSize'] = api.getData().colSize;
                        }
                    },
                    onSubmit: (api) => {
                        if (api.getData().type === 'row') {
                            editor.insertContent('<div class="' + api.getData().type + '"></div>&nbsp;');
                        } else {
                            var specificCol = '';

                            if (api.getData().col) {
                                specificCol += '-' + api.getData().col;
                            }

                            if (api.getData().colSize) {
                                specificCol += '-' + api.getData().colSize;
                            }
                            editor.insertContent('<div class="' + api.getData().type + specificCol + '"></div>&nbsp;');
                        }
                        api.close();
                    },
                    buttons: [{
                        text: 'Close',
                        type: 'cancel',
                        onclick: 'close'
                    }, {
                        text: 'Insert',
                        type: 'submit',
                        primary: true,
                        enabled: false
                    }]
                });
            }

            // Define the Toolbar button
            editor.ui.registry.addButton('smartTemplate', {
                text: "Vložit šablonu",
                icon: 'select-all',
                onAction: _onAction
            });

            // Define the Menu Item
            /*editor.ui.registry.addMenuItem('smartTemplate', {
                text: 'Vložit šablonu',
                context: 'insert',
                onAction: _onAction
            });*/
        });
    }());
})();