$(function () {

    $.nette.ext({
        success: function () {
            tinyInit();
        },
        init: function () {
            tinyInit();
        }
    });

    function tinyInit() {
        var options = {
            'default': {
                language_url: '/3rd/tinymce-5.2.2/langs/cs.js',
                language: 'cs',
                height: 400,
                plugins: ['link', 'image', 'media', 'paste', 'lists', 'print', 'preview', 'table', 'code', 'fullscreen', 'imagetools', 'insertdatetime', 'searchreplace', 'responsivefilemanager', 'smartTemplate'],
                toolbar: 'styleselect smartTemplate | undo redo | alignleft aligncenter alignright alignjustify | bold italic | insertfile image link | bullist numlist outdent indent | forecolor backcolor | preview help | responsivefilemanager',

                relative_urls: false,

                content_css: _cssFront,
                content_style: "body {padding: 15px;} .row {border: 2px dashed #cccccc; padding: 10px; margin: 0px;} .row:hover {border: 2px dashed #335371;} [class*=col] {border: 1px dashed #cccccc; padding: 20px;} [class*=col]:hover {border: 1px dashed #335371;}",

                image_title: true,
                image_advtab: true,
                /*image_class_list: [
                    {title: '--', value: ''},
                    {title: 'Oranžový rámeček', value: 'border-3-orange-light'}
                ],*/

                external_filemanager_path: '/3rd/responsive-filemanager-9.14.0/filemanager/',
                filemanager_title: 'SkAdmin Filemanager',
                external_plugins: {'filemanager': '/3rd/responsive-filemanager-9.14.0/filemanager/plugin.min.js'}
            },
            'mail': {
                language_url: '/3rd/tinymce-5.2.2/langs/cs.js',
                language: 'cs',
                height: 400,
                plugins: ['link', 'image', 'media', 'paste', 'lists', 'print', 'preview', 'table', 'code', 'fullscreen', 'imagetools', 'insertdatetime', 'searchreplace'],
                toolbar: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic | insertfile image link | bullist numlist | preview | responsivefilemanager',

                relative_urls: false,

                //content_css: _frontCss,
                //content_style: "body {padding: 15px;}",

                image_title: true,
                image_advtab: true,
                /*image_class_list: [
                    {title: '--', value: ''},
                    {title: 'Oranžový rámeček', value: 'border-3-orange-light'}
                ],*/

                external_filemanager_path: '/3rd/responsive-filemanager-9.14.0/filemanager/',
                filemanager_title: 'SkAdmin filemanager',
                external_plugins: {'filemanager': '/3rd/responsive-filemanager-9.14.0/filemanager/plugin.min.js'}
            }
        };

        $('[data-tinymce]:not(.tinymce-init)').each(function () {
            var key = $(this).data('tinymce');

            var currentOption = options['default'];
            if (key in options) {
                var currentOption = options[key];
            }

            let delay = $(this).data('tinymce-delay');
            if (delay) {
                let $that = $(this);
                setTimeout(function () {
                    $that.addClass('tinymce-init')
                        .tinymce(currentOption);
                }, delay);
            } else {
                $(this).addClass('tinymce-init')
                    .tinymce(currentOption);
            }
        });

        $('[data-tinymce-unbind]').each(function () {
            tinymce.remove('#' + $(this).attr('id'));

            var key = $(this).data('data-tinymce-unbind]');

            var currentOption = options['default'];
            if (key in options) {
                var currentOption = options[key];
            }

            $(this).tinymce(currentOption);
        });
    }
});