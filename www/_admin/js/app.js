var dataFormChangeLinkTimeout = [];
var dataFormChangeLinkLastVal = '';
$(document).on('change keyup', 'form .init-form-change-link[data-form-change-link]', function () {
    var $that = $(this);
    if (dataFormChangeLinkLastVal != $that.val()) {
        dataFormChangeLinkLastVal = $that.val();
        var $form = $that.closest('form');
        var componentName = $form.attr('id').slice(4, -$form.data('id-length'));

        var attrName_val = componentName + 'val';
        var data = {};
        data[attrName_val] = $that.val();

        clearTimeout(dataFormChangeLinkTimeout[$that.attr('id')]);
        dataFormChangeLinkTimeout[$that.attr('id')] = setTimeout(function () {
            $.nette.ajax({
                url: $that.data('form-change-link'),
                data: data
            });
        }, 200);
    }
});

$(document).on('click', '[data-confirm]', function () {
    if (confirm($(this).data('confirm'))) {
        if ($(this).data('link')) {
            $.nette.ajax({
                url: $(this).data('link')
            });
        }
    }
    return false;
});

$(document).on('keydown', '.js-ctrl-enter-submit', function (e) {
    if (e.ctrlKey && e.keyCode == 13) {
        $(this).closest('form').submit();
    }
});

var inputFileUploadRfmInput = [];
setInterval(function () {
    $('.inputfile-upload-rfm-input').each(function () {
        if (inputFileUploadRfmInput[$(this).attr('id')] !== $(this).val() && $(this).val()) {
            inputFileUploadRfmInput[$(this).attr('id')] = $(this).val();

            let $root = $(this).closest('.rfm-root');
            $root.find('label').hide();
            $(this).show();

            $root.find('.inputfile-upload-rfm').addClass('d-none');
            $root.find('.inputfile-upload-rfm-remove').removeClass('d-none');
        }
    });
}, 1000);

$(document).on('click', '.inputfile-upload-rfm-remove', function () {
    let $root = $(this).closest('.rfm-root');
    $root.find('label').show();
    $root.find('.inputfile-upload-rfm-input')
        .val('')
        .hide();

    $root.find('.inputfile-upload-rfm-remove').addClass('d-none');
    $root.find('.inputfile-upload-rfm').removeClass('d-none');
});

$(function () {
    LiveForm.setOptions({
        wait: 500,
        messageErrorPrefix: ''
    });

    if (location.hash) {
        $('a[href="' + location.hash + '"]').tab('show');
    }

    $('body').append('<input type="text" id="copy-to-clipboard" style="position: absolute; top: -200px;">');
    var $copyToClipboard = $('#copy-to-clipboard');
    $(document).on('click', '[data-copy-to-clipboard]', function () {

        // tady případně na target...
        var $el = $(this);

        if ($(this).data('copy-to-clipboard') === 'text') {
            console.log($el.text());
            $copyToClipboard.val($el.text());
        } else {
            console.log($el.val());
            $copyToClipboard.val($el.val());
        }

        /* Select the text field */
        $copyToClipboard.select();
        //$copyToClipboard.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
    });

    $.nette.ext({
        success: function (payload) {
            if (payload.redirect || payload.isModal === false) {
                $('.modal-backdrop').fadeOut(300);
                $("#main-modal").modal("hide");
            } else if (payload.isModal === true) {
                $('.modal-backdrop').fadeOut(300);
                $("#main-modal").modal('show');
            }

            if (payload._datagrid_toggle_detail) {
                var $tr = $('a[data-toggle-detail="' + payload._datagrid_toggle_detail + '"]')
                    .closest('tr');

                $tr.next('tr')
                    .css('background-color', $tr.css('background-color'))
                    .css('border-bottom', '2px solid');
            }

            appInit();
        },
        init: function () {
            appInit();
        }
    });

    $.nette.init();

    $(document).on('click', 'a[href^="#"]', function (e) {
        e.preventDefault()
        $(this).tab('show');
    })

    function appInit() {
        $('form [data-form-change-link]:not(.init-form-change-link)').addClass('init-form-change-link');

        var $chosenOptionDefault = {
            width: "100%",
        };

        var chosenBox = [];
        $('.chosen-select:not(.chosen-select-init)').each(function (i, that) {
            var $chosenOption = {};

            if ($(that).data('chosen-placeholder-text')) {
                $chosenOption['placeholder_text_multiple'] = $(that).data('chosen-placeholder-text');
                $chosenOption['placeholder_text_single'] = $(that).data('chosen-placeholder-text');
            }

            if ($(that).data('chosen-no-result-text')) {
                $chosenOption['no_results_text'] = $(that).data('chosen-no-result-text');
                $chosenOption['no_results_text'] = $(that).data('chosen-no-result-text');
            }

            $.extend($chosenOption, $chosenOptionDefault);

            $(that).addClass('.chosen-select-init')
                .chosen($chosenOption);

            $(that).change(function () {
                Nette.toggleForm($(this).closest('form')[0]);
            });
        });

        $('[data-toggle="popover"]:not(.popover-init)')
            .addClass('popover-init')
            .popover();

        $('[data-toggle="tooltip"]:not(.tooltip-init)')
            .addClass('tooltip-init')
            .tooltip();

        $('[data-daterange]:not(.daterange-init)').each(function () {
            var format = $(this).data('daterange') ? $(this).data('daterange') : 'DD.MM.YYYY';

            var setting = {
                locale: {
                    format: format,
                    applyLabel: "Potvrdit",
                    cancelLabel: "Zrušit"
                }
            };

            if ($(this).data('daterange-mindate')) {
                setting.minDate = $(this).data('daterange-mindate');
            }

            if ($(this).data('daterange-timepicker') !== undefined) {
                setting.timePicker = true;
                setting.timePicker24Hour = true;
            }

            if ($(this).data('date-opens')) {
                setting.opens = $(this).data('date-opens');
            }

            if ($(this).data('date-drops')) {
                setting.drops = $(this).data('date-drops');
            }

            $(this)
                .addClass('daterange-init')
                .daterangepicker(setting);

            // icons
            $(this).wrap('<div class="input-group"></div>');
            $('<div class="input-group-prepend"><label class="input-group-text" for="' + $(this).attr('id') + '"><i class="far fa-calendar-alt"></i></label></div>').insertBefore($(this));
        });

        $('[data-date]:not(.date-init)').each(function () {
            var clearInput = false;

            if (!$(this).val() && $(this).data('date-clear') !== undefined) {
                clearInput = true;
            }

            var format = $(this).data('date') ? $(this).data('date') : 'DD.MM.YYYY';

            var setting = {
                locale: {
                    format: format,
                    applyLabel: "Potvrdit",
                    cancelLabel: "Zrušit"
                }
            };

            if ($(this).data('date-mindate')) {
                setting.minDate = $(this).data('date-mindate');
            }

            setting.singleDatePicker = true;
            setting.showDropdowns = true;

            if ($(this).data('date-opens')) {
                setting.opens = $(this).data('date-opens');
            }

            if ($(this).data('date-drops')) {
                setting.drops = $(this).data('date-drops');
            }

            $(this)
                .addClass('date-init')
                .daterangepicker(setting);

            // icons
            if ($(this).data('date-noicon') === undefined) {
                $(this).wrap('<div class="input-group"></div>');
                $('<div class="input-group-prepend"><label class="input-group-text" for="' + $(this).attr('id') + '"><i class="far fa-fw fa-calendar-alt"></i></label></div>').insertBefore($(this));
            }

            if (clearInput) {
                $(this).val('');
            }
        });

        // color picker
        setTimeout(function () {
            $('[data-create-colorpicker]:not(.colorpicker-init)').each(function () {
                $(this).addClass('colopicker-init');

                $(this).wrap('<div class="input-group"></div>');
                $('<div class="input-group-append"><span class="input-group-text colorpicker-input-addon"><i></i></span></div>').insertAfter($(this));
                $(this).closest('.input-group').colorpicker();
            });
        }, 200);

        $('span[data-color-view]:not(.color-view-init)').each(function () {
            $(this).addClass('color-view-init');

            let value = $(this).text();

            $(this).text('');

            if (value) {
                $(this).attr('style', 'background-color: ' + value + '; width: 22px; height: 22px; display: inline-block; position: relative; top: 6px; margin-top: -4px; border: 1px solid black;')
            }
        });

        // responsive file manager fancy
        $('.inputfile-upload-rfm:not(.inputfile-upload-rfm-init)').each(function () {
            $(this).addClass('inputfile-upload-rfm-init')
                .fancybox({
                    'type': 'iframe',
                    'autoScale': false
                });
        });
        //autoUpdateInput
    }
});