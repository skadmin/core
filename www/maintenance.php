<?php
header('HTTP/1.1 503 Service Temporarily Unavailable');
header('Status: 503 Service Temporarily Unavailable');
header('Retry-After: 60');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Expires" content="-1">

    <meta name="theme-color" content="#335371">

    <title>Odstávka webových stránek</title>

    <style>
        body {
            text-align: center;
            padding: 150px;
        }

        h1 {
            font-size: 50px;
        }

        body {
            font: 20px Helvetica, sans-serif;
            color: #333;
        }

        article {
            position: relative;
            text-align: left;
            display: block;
            margin: 0 auto;
            width: 650px;
        }

        p:first-of-type {
            padding-top: 25px;
        }

        #logo {
            max-width: 100%;
            width: 120px;
            height: auto;

            animation-iteration-count: infinite;
            animation-name: rorate-axis-x;
            animation-duration: 5s;

            position: absolute;
            top: -20px;
            right: 20px;
        }

        @keyframes rorate-axis-x {
            from {
                -webkit-transform: rotateY(0deg);
                -moz-transform: rotateY(0deg);
                -o-transform: rotateY(0deg);
                transform: rotateY(0deg);
            }
            to {
                -webkit-transform: rotateY(360deg);
                -moz-transform: rotateY(360deg);
                -o-transform: rotateY(360deg);
                transform: rotateY(360deg);
            }
        }

    </style>
</head>
<body>

<article>
    <h1>Brzy se vrátíme!</h1> <img id="logo" src="/_admin/style/image/logo-pure.svg" alt="logo">
    <div>
        <p>Velmi se omlouváme, ale právě dochází k úpravě webových stránek. </p>
        <p>&mdash; tým SkAdmin</p>
    </div>
</article>

<script type="text/javascript">
    setTimeout(function () {
        location.reload(true);
    }, 10000);
</script>
</body>
</html>