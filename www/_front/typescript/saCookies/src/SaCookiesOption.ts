class SaCookiesOption {

    private _code: string;
    private _title: string;
    private _description: string;
    private _onlyRead: boolean;
    private _enable: boolean;

    constructor(code: string, title: string, description: string, onlyRead: boolean = false, defaultEnable: boolean = true) {
        this._code = code;
        this._title = title;
        this._description = description;
        this._onlyRead = onlyRead;

        // set enable
        this.enable = this.getCookie(this._code, defaultEnable);
    }

    get code(): string {
        return this._code;
    }

    get title(): string {
        return this._title;
    }

    get description(): string {
        return this._description;
    }

    get onlyRead(): boolean {
        return this._onlyRead;
    }

    get enable(): boolean {
        return this._enable;
    }

    set enable(value: boolean) {
        this._enable = value;
        this.setCookie(this._code, this._enable);
    }

    // COOKIE
    private _prefixCookie: string = 'wa_cookies_option';

    private setCookie(name: string, val: boolean) {
        let date = new Date();
        let expire = 31; // Set it expire in X days

        let value = String(val);

        date.setTime(date.getTime() + (expire * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=" + value + "; expires=" + date.toUTCString() + "; path=/";
    }

    private getCookie(name: string, defaultValue: boolean): boolean {
        let value = "; " + document.cookie;
        let parts = value.split("; " + this.getNameCookie(name) + "=");

        if (parts.length == 2) {
            return parts.pop().split(";").shift().toLowerCase() == 'true';
        }

        return defaultValue;
    }

    private deleteCookie(prefix: string, name: string) {
        let date = new Date();
        // Set it expire in -1 days
        date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=; expires=" + date.toUTCString() + "; path=/";
    }

    private getNameCookie(name: string): string {
        return this._prefixCookie + '.' + name;
    }
}