class SaCookies {

    private _title: string;
    private _titleOption: string;
    private _titleOptionMore: string;
    private _description: string;
    private _options: SaCookiesOption[] = [];
    private _isAllEnable: boolean = true;

    private _cofirmText: string = 'Souhlasím';
    private _btnColor: string = 'white';
    private _btnBgColor: string = 'black';
    private _settingsText: string = 'Cookies';

    constructor(title: string, titleOption: string, titleOptionMore: string, description: string) {
        this._title = title;
        this._titleOption = titleOption;
        this._titleOptionMore = titleOptionMore;
        this._description = description;
    }

    set cofirmText(value: string) {
        this._cofirmText = value;
    }

    set btnColor(value: string) {
        this._btnColor = value;
    }

    set btnBgColor(value: string) {
        this._btnBgColor = value;
    }

    set settingsText(value: string) {
        this._settingsText = value;
    }

    public addOption(code: string, title: string, description: string = '', onlyRead: boolean = false, defaultEnable: boolean = true): SaCookiesOption {
        let option = new SaCookiesOption(code, title, description, onlyRead, defaultEnable);
        this._options[code] = option;

        if (!option.enable) {
            this._isAllEnable = false;
        }

        return option;
    }

    public resetOptions(): void {
        this._options = [];
    }

    get options(): SaCookiesOption[] {
        return this._options;
    }

    public isEnable(code: string = null): boolean {
        if (code === null) {
            return this._isAllEnable;
        } else if (typeof this._options[code] !== 'undefined') {
            return this._options[code].enable;
        }

        return false;
    }

    public setEnable(code: string, enable: boolean): boolean {
        if (typeof this._options[code] !== 'undefined') {
            this._options[code].enable = enable;

            if (enable) { // pokud je nová hodnota true, tak musíme projít ostatní
                this.checkEnableOption();
            } else { // pokud je nová hodnota false, tak nemusím nic procházet a vím že ne všechny jsou true
                this._isAllEnable = false;
            }

            return true;
        }

        return false;
    }

    private checkEnableOption(): void {
        this._isAllEnable = true;
        for (let key in this._options) {
            let option = this._options[key];

            if (!option.enable) {
                this._isAllEnable = false;
            }
        }
    }

    public render(): void {
        let renderSmall = this.getCookie('renderSmall', false);

        let waCookiesBox: HTMLElement = document.getElementById('wa-cookies-box');

        if (waCookiesBox === null) {
            if (renderSmall) {
                this.renderBigBox('none');
                this.renderSmallBox();
            } else {
                this.renderBigBox();
                this.renderSmallBox('none');
            }
        } else {
            let waCookiesBoxMini: HTMLElement = document.getElementById('wa-cookies-box-mini');

            if (renderSmall) {
                waCookiesBox.style.display = 'none';
                waCookiesBoxMini.style.display = 'block';
            } else {
                waCookiesBox.style.display = 'block';
                waCookiesBoxMini.style.display = 'none';
            }
        }
    }

    private renderBigBox(display: string = 'block'): void {
        // box itself
        let waCookiesBox = this.createWaCookiesBox(display);


        // create a title
        let waCookiesBoxTitle = document.createElement('h5') as HTMLHeadingElement;
        waCookiesBoxTitle.textContent = this._title;
        waCookiesBox.appendChild(waCookiesBoxTitle);

        // create description
        let waCookiesBoxDescription = document.createElement('div') as HTMLDivElement;
        waCookiesBoxDescription.innerHTML = this._description;
        waCookiesBox.appendChild(waCookiesBoxDescription);

        // create cookie title
        let waCookiesBoxOptionTitle = document.createElement('h6') as HTMLHeadingElement;
        waCookiesBoxOptionTitle.textContent = this._titleOption;
        waCookiesBoxOptionTitle.style.cssText = 'display: none;'
        waCookiesBox.appendChild(waCookiesBoxOptionTitle);

        // create table of cookie
        let waCookiesBoxOptionTable = document.createElement('table') as HTMLTableElement;
        waCookiesBoxOptionTable.style.cssText = 'width: 100%; display: none;';
        for (let key in this._options) {
            let option = this._options[key];
            waCookiesBoxOptionTable.appendChild(this.createOptionTr(option));
        }

        // create a show option button
        waCookiesBox.appendChild(this.createBtnShowOption(waCookiesBoxOptionTitle, waCookiesBoxOptionTable));

        // create a confirmation button
        waCookiesBox.appendChild(this.createBtnConfirm());

        // add table of option
        waCookiesBox.appendChild(waCookiesBoxOptionTable);

        // append to the end of the body
        document.body.appendChild(waCookiesBox);
    }

    private renderSmallBox(display: string = 'block'): void {
        let styleConfirm = [
            'padding: 3px 10px 0px',
            'font-size: 13px',
            'position: fixed',
            'z-index: 9',
            'bottom: 0px',
            'left: 15px',
            'background: ' + this._btnBgColor,
            'border: 1px solid ' + this._btnBgColor,
            'color: ' + this._btnColor,
            'border-top-left-radius: 10px',
            'border-top-right-radius: 10px',
            'display: ' + display
        ];
        let waCookiesBoxMini = document.createElement('button') as HTMLButtonElement;
        waCookiesBoxMini.id = 'wa-cookies-box-mini';
        waCookiesBoxMini.textContent = this._settingsText;
        waCookiesBoxMini.style.cssText = styleConfirm.join(';');

        // i want show only small btn
        let self = this;
        waCookiesBoxMini.addEventListener('click', function (event) {
            self.setCookie('renderSmall', false);
            self.render();
        });


        document.body.appendChild(waCookiesBoxMini);
    }

    private createWaCookiesBox(display: string): HTMLDivElement {
        let styleBox = [
            'position: fixed',
            'color: black',
            'font-size: 13px',
            'bottom: 0px',
            'left: 0px',
            'height: auto',
            'width: 100%',
            'background-color: white',
            'z-index: 999999',
            'box-shadow: 0px 0px 13px -2px rgb(0, 0, 0)',
            'padding: 15px',
            'max-height: 100%',
            'overflow: auto',
            'display: ' + display
        ];
        let waCookiesBox = document.createElement('div') as HTMLDivElement;
        waCookiesBox.id = 'wa-cookies-box';
        waCookiesBox.style.cssText = styleBox.join(';');

        return waCookiesBox;
    }

    private createBtnConfirm(): HTMLButtonElement {
        let style = [
            'margin-left: 5px',
            'background: ' + this._btnBgColor,
            'border: 1px solid ' + this._btnBgColor,
            'color: ' + this._btnColor
        ];
        let waCookiesBoxConfirm = document.createElement('button') as HTMLButtonElement;
        waCookiesBoxConfirm.textContent = this._cofirmText;
        waCookiesBoxConfirm.style.cssText = style.join(';');

        // i want show only small btn
        let self = this;
        waCookiesBoxConfirm.addEventListener('click', function () {
            self.setCookie('renderSmall', true);
            self.render();
        });

        return waCookiesBoxConfirm;
    }

    private createBtnShowOption(waCookiesBoxOptionTitle: HTMLHeadingElement, waCookiesBoxOptionTable: HTMLTableElement): HTMLButtonElement {
        let style = [
            'background: ' + this._btnColor,
            'border: 0px solid ' + this._btnBgColor,
            'color: ' + this._btnBgColor
        ];
        let waCookiesBoxShowOption = document.createElement('button') as HTMLButtonElement;
        waCookiesBoxShowOption.textContent = this._titleOptionMore;
        waCookiesBoxShowOption.style.cssText = style.join(';');

        // i want show only small btn
        let self = this;
        waCookiesBoxShowOption.addEventListener('click', function () {
            waCookiesBoxOptionTitle.style.display = 'inline-block';
            waCookiesBoxOptionTable.style.display = 'block';
            waCookiesBoxShowOption.style.display = 'none';
        });

        return waCookiesBoxShowOption;
    }

    private createOptionTr(option: SaCookiesOption): HTMLTableRowElement {
        let waCookiesBoxOptionTr = document.createElement('tr') as HTMLTableRowElement;

        let waCookiesBoxOptionTdInput = document.createElement('td') as HTMLTableCellElement;
        waCookiesBoxOptionTdInput.style.cssText = 'vertical-align: top; padding-top: 3px;';
        waCookiesBoxOptionTdInput.appendChild(this.createOptionInputCheckbox(option));
        waCookiesBoxOptionTr.appendChild(waCookiesBoxOptionTdInput);

        let waCookiesBoxOptionTdTitle = document.createElement('td') as HTMLTableCellElement;
        waCookiesBoxOptionTdTitle.style.cssText = 'vertical-align: top;';
        let waCookiesBoxOptionTdTitleLabel = document.createElement('label') as HTMLLabelElement;
        waCookiesBoxOptionTdTitleLabel.setAttribute('for', 'wa-cookies-' + option.code);
        waCookiesBoxOptionTdTitleLabel.textContent = option.title;
        waCookiesBoxOptionTdTitleLabel.style.cssText = 'margin-bottom: 0px; white-space: nowrap;';
        waCookiesBoxOptionTdTitle.appendChild(waCookiesBoxOptionTdTitleLabel);
        waCookiesBoxOptionTr.appendChild(waCookiesBoxOptionTdTitle);

        let waCookiesBoxOptionTdDescription = document.createElement('td') as HTMLTableCellElement;
        waCookiesBoxOptionTdDescription.innerHTML = option.description;
        waCookiesBoxOptionTdDescription.style.cssText = 'vertical-align: top; padding-bottom: 8px; padding-left: 10px;';
        waCookiesBoxOptionTr.appendChild(waCookiesBoxOptionTdDescription);

        return waCookiesBoxOptionTr;
    }

    private createOptionInputCheckbox(option: SaCookiesOption): HTMLInputElement {
        let checkbox = document.createElement('input') as HTMLInputElement;
        checkbox.setAttribute('type', 'checkbox');
        checkbox.setAttribute('id', 'wa-cookies-' + option.code);

        if (option.enable) {
            checkbox.setAttribute('checked', 'checked');
        }

        if (option.onlyRead) {
            checkbox.setAttribute('disabled', 'disabled');
            checkbox.setAttribute('readonly', 'readonly');

            checkbox.addEventListener('change', function () {
                this['checked'] = true;
            });
        } else {
            checkbox.addEventListener('change', function () {
                let code = this.getAttribute('data-wa-cookies');
                option.enable = this['checked'];
            });
        }

        return checkbox;
    }

    // COOKIE
    private _prefixCookie: string = 'wa_cookies';

    private setCookie(name: string, val: boolean) {
        let date = new Date();
        let expire = 31; // Set it expire in X days

        let value = String(val);

        date.setTime(date.getTime() + (expire * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=" + value + "; expires=" + date.toUTCString() + "; path=/";
    }

    private getCookie(name: string, defaultValue: boolean): boolean {
        let value = "; " + document.cookie;
        let parts = value.split("; " + this.getNameCookie(name) + "=");

        if (parts.length == 2) {
            return parts.pop().split(";").shift().toLowerCase() == 'true';
        }

        return defaultValue;
    }

    private deleteCookie(prefix: string, name: string) {
        let date = new Date();
        // Set it expire in -1 days
        date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=; expires=" + date.toUTCString() + "; path=/";
    }

    private getNameCookie(name: string): string {
        return this._prefixCookie + '.' + name;
    }

}