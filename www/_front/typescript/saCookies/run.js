let saCookies = new SaCookies('Tato webová stránka používá cookies', 'Cookies na webu', 'Zobrazit cookies na webu', '<p>Ke statistice a analýze naší návštěvnosti využíváme soubory cookie. Informace o tom, jak náš web používáte, sdílíme se svými partnery pro sociální média a analýzy. Partneři tyto údaje mohou zkombinovat s dalšími informacemi, které jste jim poskytli nebo které získali v důsledku toho, že používáte jejich služby.</p>');
saCookies.btnBgColor = '#007bff';
saCookies.cofirmText = 'Souhlasím';
saCookies.settingsText = 'Cookies';

saCookies.addOption('nutne', 'SESSION ID', 'Uchovává údaje o uživateli napříč požadavky na stránkách.', true);
//saCookies.addOption('gtm', 'Google Tag Manager', '(zkráceně GTM) je nástroj od společnosti Google, který nám umožňuje získávat statistická data o uživatelích na našem webu. Díky této službě můžeme sledovat aktuální i historickou návštěvnost a chování uživatelů');

window.onload = function () {
    saCookies.render();
};

