var SaCookies = (function () {
    function SaCookies(title, titleOption, titleOptionMore, description) {
        this._options = [];
        this._isAllEnable = true;
        this._cofirmText = 'Souhlasím';
        this._btnColor = 'white';
        this._btnBgColor = 'black';
        this._settingsText = 'Cookies';
        this._prefixCookie = 'wa_cookies';
        this._title = title;
        this._titleOption = titleOption;
        this._titleOptionMore = titleOptionMore;
        this._description = description;
    }
    Object.defineProperty(SaCookies.prototype, "cofirmText", {
        set: function (value) {
            this._cofirmText = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookies.prototype, "btnColor", {
        set: function (value) {
            this._btnColor = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookies.prototype, "btnBgColor", {
        set: function (value) {
            this._btnBgColor = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookies.prototype, "settingsText", {
        set: function (value) {
            this._settingsText = value;
        },
        enumerable: true,
        configurable: true
    });
    SaCookies.prototype.addOption = function (code, title, description, onlyRead, defaultEnable) {
        if (description === void 0) { description = ''; }
        if (onlyRead === void 0) { onlyRead = false; }
        if (defaultEnable === void 0) { defaultEnable = true; }
        var option = new SaCookiesOption(code, title, description, onlyRead, defaultEnable);
        this._options[code] = option;
        if (!option.enable) {
            this._isAllEnable = false;
        }
        return option;
    };
    SaCookies.prototype.resetOptions = function () {
        this._options = [];
    };
    Object.defineProperty(SaCookies.prototype, "options", {
        get: function () {
            return this._options;
        },
        enumerable: true,
        configurable: true
    });
    SaCookies.prototype.isEnable = function (code) {
        if (code === void 0) { code = null; }
        if (code === null) {
            return this._isAllEnable;
        }
        else if (typeof this._options[code] !== 'undefined') {
            return this._options[code].enable;
        }
        return false;
    };
    SaCookies.prototype.setEnable = function (code, enable) {
        if (typeof this._options[code] !== 'undefined') {
            this._options[code].enable = enable;
            if (enable) {
                this.checkEnableOption();
            }
            else {
                this._isAllEnable = false;
            }
            return true;
        }
        return false;
    };
    SaCookies.prototype.checkEnableOption = function () {
        this._isAllEnable = true;
        for (var key in this._options) {
            var option = this._options[key];
            if (!option.enable) {
                this._isAllEnable = false;
            }
        }
    };
    SaCookies.prototype.render = function () {
        var renderSmall = this.getCookie('renderSmall', false);
        var waCookiesBox = document.getElementById('wa-cookies-box');
        if (waCookiesBox === null) {
            if (renderSmall) {
                this.renderBigBox('none');
                this.renderSmallBox();
            }
            else {
                this.renderBigBox();
                this.renderSmallBox('none');
            }
        }
        else {
            var waCookiesBoxMini = document.getElementById('wa-cookies-box-mini');
            if (renderSmall) {
                waCookiesBox.style.display = 'none';
                waCookiesBoxMini.style.display = 'block';
            }
            else {
                waCookiesBox.style.display = 'block';
                waCookiesBoxMini.style.display = 'none';
            }
        }
    };
    SaCookies.prototype.renderBigBox = function (display) {
        if (display === void 0) { display = 'block'; }
        var waCookiesBox = this.createWaCookiesBox(display);
        var waCookiesBoxTitle = document.createElement('h5');
        waCookiesBoxTitle.textContent = this._title;
        waCookiesBox.appendChild(waCookiesBoxTitle);
        var waCookiesBoxDescription = document.createElement('div');
        waCookiesBoxDescription.innerHTML = this._description;
        waCookiesBox.appendChild(waCookiesBoxDescription);
        var waCookiesBoxOptionTitle = document.createElement('h6');
        waCookiesBoxOptionTitle.textContent = this._titleOption;
        waCookiesBoxOptionTitle.style.cssText = 'display: none;';
        waCookiesBox.appendChild(waCookiesBoxOptionTitle);
        var waCookiesBoxOptionTable = document.createElement('table');
        waCookiesBoxOptionTable.style.cssText = 'width: 100%; display: none;';
        for (var key in this._options) {
            var option = this._options[key];
            waCookiesBoxOptionTable.appendChild(this.createOptionTr(option));
        }
        waCookiesBox.appendChild(this.createBtnShowOption(waCookiesBoxOptionTitle, waCookiesBoxOptionTable));
        waCookiesBox.appendChild(this.createBtnConfirm());
        waCookiesBox.appendChild(waCookiesBoxOptionTable);
        document.body.appendChild(waCookiesBox);
    };
    SaCookies.prototype.renderSmallBox = function (display) {
        if (display === void 0) { display = 'block'; }
        var styleConfirm = [
            'padding: 3px 10px 0px',
            'font-size: 13px',
            'position: fixed',
            'z-index: 9',
            'bottom: 0px',
            'left: 15px',
            'background: ' + this._btnBgColor,
            'border: 1px solid ' + this._btnBgColor,
            'color: ' + this._btnColor,
            'border-top-left-radius: 10px',
            'border-top-right-radius: 10px',
            'display: ' + display
        ];
        var waCookiesBoxMini = document.createElement('button');
        waCookiesBoxMini.id = 'wa-cookies-box-mini';
        waCookiesBoxMini.textContent = this._settingsText;
        waCookiesBoxMini.style.cssText = styleConfirm.join(';');
        var self = this;
        waCookiesBoxMini.addEventListener('click', function (event) {
            self.setCookie('renderSmall', false);
            self.render();
        });
        document.body.appendChild(waCookiesBoxMini);
    };
    SaCookies.prototype.createWaCookiesBox = function (display) {
        var styleBox = [
            'position: fixed',
            'color: black',
            'font-size: 13px',
            'bottom: 0px',
            'left: 0px',
            'height: auto',
            'width: 100%',
            'background-color: white',
            'z-index: 999999',
            'box-shadow: 0px 0px 13px -2px rgb(0, 0, 0)',
            'padding: 15px',
            'max-height: 100%',
            'overflow: auto',
            'display: ' + display
        ];
        var waCookiesBox = document.createElement('div');
        waCookiesBox.id = 'wa-cookies-box';
        waCookiesBox.style.cssText = styleBox.join(';');
        return waCookiesBox;
    };
    SaCookies.prototype.createBtnConfirm = function () {
        var style = [
            'margin-left: 5px',
            'background: ' + this._btnBgColor,
            'border: 1px solid ' + this._btnBgColor,
            'color: ' + this._btnColor
        ];
        var waCookiesBoxConfirm = document.createElement('button');
        waCookiesBoxConfirm.textContent = this._cofirmText;
        waCookiesBoxConfirm.style.cssText = style.join(';');
        var self = this;
        waCookiesBoxConfirm.addEventListener('click', function () {
            self.setCookie('renderSmall', true);
            self.render();
        });
        return waCookiesBoxConfirm;
    };
    SaCookies.prototype.createBtnShowOption = function (waCookiesBoxOptionTitle, waCookiesBoxOptionTable) {
        var style = [
            'background: ' + this._btnColor,
            'border: 0px solid ' + this._btnBgColor,
            'color: ' + this._btnBgColor
        ];
        var waCookiesBoxShowOption = document.createElement('button');
        waCookiesBoxShowOption.textContent = this._titleOptionMore;
        waCookiesBoxShowOption.style.cssText = style.join(';');
        var self = this;
        waCookiesBoxShowOption.addEventListener('click', function () {
            waCookiesBoxOptionTitle.style.display = 'inline-block';
            waCookiesBoxOptionTable.style.display = 'block';
            waCookiesBoxShowOption.style.display = 'none';
        });
        return waCookiesBoxShowOption;
    };
    SaCookies.prototype.createOptionTr = function (option) {
        var waCookiesBoxOptionTr = document.createElement('tr');
        var waCookiesBoxOptionTdInput = document.createElement('td');
        waCookiesBoxOptionTdInput.style.cssText = 'vertical-align: top; padding-top: 3px;';
        waCookiesBoxOptionTdInput.appendChild(this.createOptionInputCheckbox(option));
        waCookiesBoxOptionTr.appendChild(waCookiesBoxOptionTdInput);
        var waCookiesBoxOptionTdTitle = document.createElement('td');
        waCookiesBoxOptionTdTitle.style.cssText = 'vertical-align: top;';
        var waCookiesBoxOptionTdTitleLabel = document.createElement('label');
        waCookiesBoxOptionTdTitleLabel.setAttribute('for', 'wa-cookies-' + option.code);
        waCookiesBoxOptionTdTitleLabel.textContent = option.title;
        waCookiesBoxOptionTdTitleLabel.style.cssText = 'margin-bottom: 0px; white-space: nowrap;';
        waCookiesBoxOptionTdTitle.appendChild(waCookiesBoxOptionTdTitleLabel);
        waCookiesBoxOptionTr.appendChild(waCookiesBoxOptionTdTitle);
        var waCookiesBoxOptionTdDescription = document.createElement('td');
        waCookiesBoxOptionTdDescription.innerHTML = option.description;
        waCookiesBoxOptionTdDescription.style.cssText = 'vertical-align: top; padding-bottom: 8px; padding-left: 10px;';
        waCookiesBoxOptionTr.appendChild(waCookiesBoxOptionTdDescription);
        return waCookiesBoxOptionTr;
    };
    SaCookies.prototype.createOptionInputCheckbox = function (option) {
        var checkbox = document.createElement('input');
        checkbox.setAttribute('type', 'checkbox');
        checkbox.setAttribute('id', 'wa-cookies-' + option.code);
        if (option.enable) {
            checkbox.setAttribute('checked', 'checked');
        }
        if (option.onlyRead) {
            checkbox.setAttribute('disabled', 'disabled');
            checkbox.setAttribute('readonly', 'readonly');
            checkbox.addEventListener('change', function () {
                this['checked'] = true;
            });
        }
        else {
            checkbox.addEventListener('change', function () {
                var code = this.getAttribute('data-wa-cookies');
                option.enable = this['checked'];
            });
        }
        return checkbox;
    };
    SaCookies.prototype.setCookie = function (name, val) {
        var date = new Date();
        var expire = 31;
        var value = String(val);
        date.setTime(date.getTime() + (expire * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=" + value + "; expires=" + date.toUTCString() + "; path=/";
    };
    SaCookies.prototype.getCookie = function (name, defaultValue) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + this.getNameCookie(name) + "=");
        if (parts.length == 2) {
            return parts.pop().split(";").shift().toLowerCase() == 'true';
        }
        return defaultValue;
    };
    SaCookies.prototype.deleteCookie = function (prefix, name) {
        var date = new Date();
        date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=; expires=" + date.toUTCString() + "; path=/";
    };
    SaCookies.prototype.getNameCookie = function (name) {
        return this._prefixCookie + '.' + name;
    };
    return SaCookies;
}());
var SaCookiesOption = (function () {
    function SaCookiesOption(code, title, description, onlyRead, defaultEnable) {
        if (onlyRead === void 0) { onlyRead = false; }
        if (defaultEnable === void 0) { defaultEnable = true; }
        this._prefixCookie = 'wa_cookies_option';
        this._code = code;
        this._title = title;
        this._description = description;
        this._onlyRead = onlyRead;
        this.enable = this.getCookie(this._code, defaultEnable);
    }
    Object.defineProperty(SaCookiesOption.prototype, "code", {
        get: function () {
            return this._code;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookiesOption.prototype, "title", {
        get: function () {
            return this._title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookiesOption.prototype, "description", {
        get: function () {
            return this._description;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookiesOption.prototype, "onlyRead", {
        get: function () {
            return this._onlyRead;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SaCookiesOption.prototype, "enable", {
        get: function () {
            return this._enable;
        },
        set: function (value) {
            this._enable = value;
            this.setCookie(this._code, this._enable);
        },
        enumerable: true,
        configurable: true
    });
    SaCookiesOption.prototype.setCookie = function (name, val) {
        var date = new Date();
        var expire = 31;
        var value = String(val);
        date.setTime(date.getTime() + (expire * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=" + value + "; expires=" + date.toUTCString() + "; path=/";
    };
    SaCookiesOption.prototype.getCookie = function (name, defaultValue) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + this.getNameCookie(name) + "=");
        if (parts.length == 2) {
            return parts.pop().split(";").shift().toLowerCase() == 'true';
        }
        return defaultValue;
    };
    SaCookiesOption.prototype.deleteCookie = function (prefix, name) {
        var date = new Date();
        date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
        document.cookie = this.getNameCookie(name) + "=; expires=" + date.toUTCString() + "; path=/";
    };
    SaCookiesOption.prototype.getNameCookie = function (name) {
        return this._prefixCookie + '.' + name;
    };
    return SaCookiesOption;
}());
//# sourceMappingURL=app.js.map