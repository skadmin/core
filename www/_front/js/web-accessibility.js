$(function () {
    $(document).on('click', '.js-slider-toogle-play', function () {
        if ($(this).hasClass('js-slider-stoped')) {
            $('#main-slider').carousel('cycle');
            $('.js-slider-toogle-play').text($(this).data('slider-text-stop'));
            $('.js-slider-toogle-play').removeClass('js-slider-stoped');
        } else {
            $('#main-slider').carousel('pause');
            $('.js-slider-toogle-play').text($(this).data('slider-text-play'));
            $('.js-slider-toogle-play').addClass('js-slider-stoped');
        }
    });
});