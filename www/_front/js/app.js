var dataFormChangeLinkTimeout = [];
var dataFormChangeLinkLastVal = '';
$(document).on('change keyup', 'form .init-form-change-link[data-form-change-link]', function () {
    var $that = $(this);
    if (dataFormChangeLinkLastVal != $that.val()) {
        dataFormChangeLinkLastVal = $that.val();
        var $form = $that.closest('form');
        var componentName = $form.attr('id').slice(4, -$form.data('id-length'));

        var attrName_val = componentName + 'val';
        var data = {};
        data[attrName_val] = $that.val();

        clearTimeout(dataFormChangeLinkTimeout[$that.attr('id')]);
        dataFormChangeLinkTimeout[$that.attr('id')] = setTimeout(function () {
            $.nette.ajax({
                url: $that.data('form-change-link'),
                data: data
            });
        }, 200);
    }
});

$(document).on('keydown', '.js-ctrl-enter-submit', function (e) {
    if (e.ctrlKey && e.keyCode == 13) {
        $(this).closest('form').submit();
    }
});

$(function () {
    LiveForm.setOptions({
        wait: 0,
        messageErrorPrefix: ''
    });

    $.nette.ext({
        success: function (payload) {
            appInit();

            if (payload.url) {
                window.history.pushState("", "", payload.url);
            }
        },
        init: function () {
            appInit();
        }
    });

    $.nette.init();

    function appInit() {
        $('form [data-form-change-link]:not(.init-form-change-link)').addClass('init-form-change-link');

        $('[data-daterange]:not(.daterange-init)').each(function () {
            var format = $(this).data('daterange') ? $(this).data('daterange') : 'DD.MM.YYYY';

            var setting = {
                locale: {
                    format: format,
                    applyLabel: "Potvrdit",
                    cancelLabel: "Zrušit"
                }
            };

            if ($(this).data('daterange-mindate')) {
                setting.minDate = $(this).data('daterange-mindate');
            }

            if ($(this).data('daterange-timepicker') !== undefined) {
                setting.timePicker = true;
                setting.timePicker24Hour = true;
            }

            if ($(this).data('date-opens')) {
                setting.opens = $(this).data('date-opens');
            }

            if ($(this).data('date-drops')) {
                setting.drops = $(this).data('date-drops');
            }

            $(this)
                .addClass('daterange-init')
                .daterangepicker(setting);

            // icons
            $(this).wrap('<div class="input-group"></div>');
            $('<div class="input-group-prepend"><label class="input-group-text" for="' + $(this).attr('id') + '"><i class="far fa-calendar-alt"></i></label></div>').insertBefore($(this));
        });
    }
});