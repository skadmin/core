$('form.dropzone').each(function () {
    var $that = $(this);

    Dropzone.options[$that.attr('id')] = {
        dictDefaultMessage: $that.data('dropzone-placeholder'),
        queuecomplete: function () {
            $(document).trigger('dropzone:queuecomplete');
            this.removeAllFiles();

            $.nette.ajax({
                url: $that.data('dropzone-refresh-link')
            });
        }
    };
});
