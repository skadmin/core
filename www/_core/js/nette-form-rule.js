Nette.validators.AppModelSystemFormRuleValidator_isSelected = function (elem, val) {
    return $.inArray(val, $(elem).val()) !== -1 || $.inArray(val + '', $(elem).val()) !== -1;
};