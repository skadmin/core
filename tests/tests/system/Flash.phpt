<?php

declare(strict_types=1);

namespace Tests\System;

use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../Bootstrap.php';

class Flash extends TestCase
{
    public function testFlash() : void
    {
        Assert::same(\App\Model\System\Flash::INFO, 'info');
        Assert::same(\App\Model\System\Flash::SUCCESS, 'success');
        Assert::same(\App\Model\System\Flash::DANGER, 'danger');
        Assert::same(\App\Model\System\Flash::WARNING, 'warning');
        Assert::same(\App\Model\System\Flash::DARK, 'dark');
        Assert::same(\App\Model\System\Flash::LIGHT, 'light');
        Assert::same(\App\Model\System\Flash::PRIMARY, 'primary');
        Assert::same(\App\Model\System\Flash::SECONDARY, 'secondary');
    }
}

// run tests
(new Flash())->run();
