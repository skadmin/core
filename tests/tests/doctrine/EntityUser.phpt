<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\User\UserFacade;
use Nette\DI\Container;
use Tester\Assert;
use Tester\TestCase;

$container = require __DIR__ . '/../Bootstrap.php';

class EntityUser extends TestCase
{
    /** @var Container */
    private $container;

    /** @var UserFacade */
    private $facadeUser;

    public function __construct(Container $container)
    {
        $this->container  = $container;
        $this->facadeUser = $this->container->getByType('\App\Model\Doctrine\User\UserFacade');
    }

    public function testUser() : void
    {
        Assert::same($this->facadeUser->get()->getId(), null);
    }
}

// run tests
(new EntityUser($container))->run();
