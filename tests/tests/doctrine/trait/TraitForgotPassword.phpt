<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\ForgotPassword;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitForgotPassword extends TestCase
{
    public function testForgotPassword() : void
    {
        $forgotPassword = new class
        {
            use ForgotPassword;
        };
        $hash           = md5(sprintf('%s%s', time(), Random::generate()));
        $forgotPassword->setForgotPasswordHash($hash);

        Assert::same($forgotPassword->getForgotPasswordHash(), $hash);
        Assert::true($forgotPassword->getForgotPasswordDate() instanceof \DateTimeInterface);

        $forgotPassword->resetForgotPassword();

        Assert::null($forgotPassword->getForgotPasswordHash());
        Assert::null($forgotPassword->getForgotPasswordDate());
    }
}

// run tests
(new TraitForgotPassword())->run();
