<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\LastUpdate;
use Nette\Utils\DateTime;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitLastUpdate extends TestCase
{
    public function testLastUpdate() : void
    {
        $lastUpdate = new class
        {
            use LastUpdate;

            public function set(string $lastUpdateAuthor, \DateTimeInterface $lastUpdateAt) : void
            {
                $this->lastUpdateAuthor = $lastUpdateAuthor;
                $this->lastUpdateAt     = $lastUpdateAt;
            }
        };

        $lastUpdateAt     = new DateTime();
        $lastUpdateAuthor = Random::generate();
        $lastUpdate->set($lastUpdateAuthor, $lastUpdateAt);

        Assert::same($lastUpdate->getLastUpdateAuthor(), $lastUpdateAuthor);
        Assert::same($lastUpdate->getLastUpdateAt(), $lastUpdateAt);

        $lastUpdate->onPreUpdateLastUpdate();

        Assert::notSame($lastUpdate->getLastUpdateAt(), $lastUpdateAt);
        Assert::true($lastUpdate->getLastUpdateAt() instanceof \DateTimeInterface);
    }
}

// run tests
(new TraitLastUpdate())->run();
