<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\HumanName;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitHumanName extends TestCase
{
    public function testHumanName() : void
    {
        $humanName = new class
        {
            use HumanName;

            public function set(string $name, string $surname) : void
            {
                $this->name    = $name;
                $this->surname = $surname;
            }
        };
        $name      = Random::generate();
        $surname   = Random::generate();
        $humanName->set($name, $surname);

        Assert::same($humanName->getName(), $name);
        Assert::same($humanName->getSurname(), $surname);

        Assert::notSame($humanName->getName(), $surname);

        Assert::same($humanName->getFullName(), $name . ' ' . $surname);
        Assert::same($humanName->getFullName(true), $surname . ' ' . $name);
        Assert::same($humanName->getFullNameReverse(), $surname . ' ' . $name);
    }
}

// run tests
(new TraitHumanName())->run();
