<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\IsLocked;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitIsLocked extends TestCase
{
    public function testIsLocked() : void
    {
        $isLocked = new class
        {
            use IsLocked;
        };

        $_isLocked = Random::generate(1, '0-1');
        $isLocked->setIsLocked($_isLocked);

        Assert::same($isLocked->isLocked(), boolval($_isLocked));

        $isLocked->setIsLocked(true);
        Assert::true($isLocked->isLocked());

        $isLocked->setIsLocked(false);
        Assert::false($isLocked->isLocked());

        $isLocked->setIsLocked(2);
        Assert::true($isLocked->isLocked());

        $isLocked->setIsLocked(0);
        Assert::false($isLocked->isLocked());
    }
}

// run tests
(new TraitIsLocked())->run();
