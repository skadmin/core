<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\WebalizeName;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitWebalize extends TestCase
{
    public function testWebalizeName() : void
    {
        $webalizeName = new class {
            use WebalizeName;

            public function set(string $name) : void
            {
                $this->name = $name;
            }
        };
        $name         = sprintf('%s %s %s', Random::generate(), Random::generate(), Random::generate());
        $webalizeName->set($name);

        Assert::same($webalizeName->getName(), $name);

        $webalize = Strings::webalize($name);
        $webalizeName->setWebalize($webalize);
        Assert::same($webalizeName->getWebalize(), $webalize);

        $webalize = Strings::webalize($name . $name);
        $webalizeName->setWebalize($webalize);
        Assert::notSame($webalizeName->getWebalize(), $webalize);
    }
}

// run tests
(new TraitWebalize())->run();
