<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Contact;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitContact extends TestCase
{
    public function testContact() : void
    {
        $contact = new class
        {
            use Contact;

            public function set(string $email, string $phone) : void
            {
                $this->setEmail($email);
                $this->phone = $phone;
            }
        };
        $phone   = Random::generate(9, '0-9');
        $contact->set('skala2524@gmail.com', $phone);

        Assert::same($contact->getEmail(), 'skala2524@gmail.com');
        Assert::same($contact->getPhone(), $phone);

        $contact->setEmail('skala2524gmail.com.cz');

        Assert::same($contact->getEmail(), '');
    }

}

// run tests
(new TraitContact())->run();
