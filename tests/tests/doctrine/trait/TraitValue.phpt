<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Value;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitValue extends TestCase
{
    public function testValue() : void
    {
        $value  = new class
        {
            use Value;

            public function set(string $value) : void
            {
                $this->value = $value;
            }
        };
        $_value = Random::generate();
        $value->set($_value);

        Assert::same($value->getValue(), $_value);
    }
}

// run tests
(new TraitValue())->run();
