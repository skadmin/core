<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Name;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitName extends TestCase
{
    public function testName() : void
    {
        $name  = new class
        {
            use Name;

            public function set(string $name) : void
            {
                $this->name = $name;
            }
        };
        $_name = Random::generate();
        $name->set($_name);

        Assert::same($name->getName(), $_name);
    }
}

// run tests
(new TraitName())->run();
