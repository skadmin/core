<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Code;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitCode extends TestCase
{
    public function testCode() : void
    {
        $code  = new class
        {
            use Code;

            public function set(string $code) : void
            {
                $this->code = $code;
            }
        };
        $_code = Random::generate();
        $code->set($_code);

        Assert::same($code->getCode(), $_code);
    }
}

// run tests
(new TraitCode())->run();
