<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\ImagePreview;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitImagePreview extends TestCase
{
    public function testName() : void
    {
        $title         = new class
        {
            use ImagePreview;

            public function set(string $imagePreview) : void
            {
                $this->imagePreview = $imagePreview;
            }
        };
        $_imagePreview = Random::generate();
        $title->set($_imagePreview);

        Assert::same($title->getImagePreview(), $_imagePreview);
    }
}

// run tests
(new TraitImagePreview())->run();
