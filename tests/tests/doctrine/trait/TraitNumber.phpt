<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Number;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitNumber extends TestCase
{
    public function testNumber() : void
    {
        $number  = new class
        {
            use Number;

            public function set(int $number) : void
            {
                $this->number = $number;
            }
        };
        $_number = intval(Random::generate(5, '0-9'));
        $number->set($_number);

        Assert::same($number->getNumber(), $_number);
    }
}

// run tests
(new TraitNumber())->run();
