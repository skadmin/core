<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\PlaceOnTheMap;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitPlaceOnTheMap extends TestCase
{
    public function testPlaceOnTheMap() : void
    {
        $placeOnTheMap = new class
        {
            use PlaceOnTheMap;

            public function set(string $name, string $address, string $gpsLat, string $gpsLng) : void
            {
                $this->placeName    = $name;
                $this->placeAddress = $address;
                $this->placeGpsLat  = $gpsLat;
                $this->placeGpsLng  = $gpsLng;
            }
        };
        $name          = Random::generate();
        $address       = Random::generate();
        $gpsLat        = Random::generate();
        $gpsLng        = Random::generate();
        $placeOnTheMap->set($name, $address, $gpsLat, $gpsLng);

        Assert::same($placeOnTheMap->getPlaceName(), $name);
        Assert::same($placeOnTheMap->getPlaceAddress(), $address);
        Assert::same($placeOnTheMap->getPlaceGpsLat(), $gpsLat);
        Assert::same($placeOnTheMap->getPlaceGpsLng(), $gpsLng);
    }
}

// run tests
(new TraitPlaceOnTheMap())->run();
