<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Name;
use App\Model\Doctrine\Traits\Title;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitTitle extends TestCase
{
    public function testName() : void
    {
        $title  = new class
        {
            use Title;

            public function set(string $title) : void
            {
                $this->title = $title;
            }
        };
        $_title = Random::generate();
        $title->set($_title);

        Assert::same($title->getTitle(), $_title);
    }
}

// run tests
(new TraitTitle())->run();
