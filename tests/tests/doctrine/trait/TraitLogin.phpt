<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Login;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitLogin extends TestCase
{
    public function testLogin() : void
    {
        $login        = new class
        {
            use Login;

            public function set(string $userName, string $userPassword) : void
            {
                $this->userName     = $userName;
                $this->userPassword = $userPassword;
            }
        };
        $userName     = Random::generate();
        $userPassword = Random::generate();
        $login->set($userName, $userPassword);

        Assert::same($login->getUserName(), $userName);
        Assert::same($login->getUserPassword(), $userPassword);
    }
}

// run tests
(new TraitLogin())->run();
