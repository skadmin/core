<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Status;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitStatus extends TestCase
{
    public function testStatus() : void
    {
        $status = new class
        {
            use Status;

            public function set(int $status) : void
            {
                $this->status = $status;
            }
        };

        for ($i = 0; $i < 100; $i++) {
            $_status = intval(Random::generate(2, '0-9'));
            $status->set($_status);

            Assert::same($status->getStatus(), $_status);
        }
    }
}

// run tests
(new TraitStatus())->run();
