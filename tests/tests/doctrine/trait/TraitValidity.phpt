<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Validity;
use Nette\Utils\DateTime;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitValidity extends TestCase
{
    public function testValidity() : void
    {
        $validity = new class
        {
            use Validity;

            public function set(DateTime $validityFrom, ?DateTime $validityTo = null) : void
            {
                $this->validityFrom = $validityFrom;
                $this->validityTo   = $validityTo;
            }
        };

        $validityFrom = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', mt_rand(1262055681, 1262055681)));
        $validityTo   = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', mt_rand(1262055681, 1262055681)));

        $validity->set($validityFrom, $validityTo);

        Assert::true($validity->getValidityFrom() instanceof \DateTimeInterface);
        Assert::same($validity->getValidityFrom()->format('dmYHis'), $validityFrom->format('dmYHis'));
        Assert::true($validity->getValidityTo() instanceof \DateTimeInterface);
        Assert::same($validity->getValidityTo()->format('dmYHis'), $validityTo->format('dmYHis'));

        $validity->set($validityFrom, null);

        Assert::true($validity->getValidityTo() instanceof \DateTimeInterface);
        Assert::same($validity->getValidityTo()->format('Ymd'), (new DateTime())->modify('+1 year')->format('Ymd'));
        Assert::same($validity->getValidityTo(true), null);
    }
}

// run tests
(new TraitValidity())->run();
