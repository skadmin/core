<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\FinancialPremium;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitFinancialPremium extends TestCase
{
    public function testFinancialPremium() : void
    {
        $financialPremium  = new class
        {
            use FinancialPremium;

            public function set(string $financialPremium) : void
            {
                $this->financialPremium = $financialPremium;
            }
        };
        $_financialPremium = Random::generate(10);
        $financialPremium->set($_financialPremium);

        Assert::same($financialPremium->getFinancialPremium(), $_financialPremium);
    }
}

// run tests
(new TraitFinancialPremium())->run();
