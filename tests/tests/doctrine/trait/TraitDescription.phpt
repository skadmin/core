<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Description;
use App\Model\Doctrine\Traits\Name;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitDescription extends TestCase
{
    public function testName() : void
    {
        $description  = new class
        {
            use Description;

            public function set(string $description) : void
            {
                $this->description = $description;
            }
        };
        $_description = Random::generate();
        $description->set($_description);

        Assert::same($description->getDescription(), $_description);
    }
}

// run tests
(new TraitDescription())->run();
