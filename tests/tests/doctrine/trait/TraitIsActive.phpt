<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\IsActive;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitIsActive extends TestCase
{
    public function testIsActive() : void
    {
        $isActive = new class
        {
            use IsActive;
        };

        $_isActive = Random::generate(1, '0-1');
        $isActive->setIsActive($_isActive);

        Assert::same($isActive->isActive(), boolval($_isActive));

        $isActive->setIsActive(true);
        Assert::true($isActive->isActive());

        $isActive->setIsActive(false);
        Assert::false($isActive->isActive());

        $isActive->setIsActive(2);
        Assert::true($isActive->isActive());

        $isActive->setIsActive(0);
        Assert::false($isActive->isActive());
    }
}

// run tests
(new TraitIsActive())->run();
