<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\FinancialReward;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitFinancialReward extends TestCase
{
    public function testFinancialReward() : void
    {
        $financialReward  = new class
        {
            use FinancialReward;

            public function set(int $financialReward) : void
            {
                $this->financialReward = $financialReward;
            }
        };
        $_financialReward = intval(Random::generate(5, '0-9'));
        $financialReward->set($_financialReward);

        Assert::same($financialReward->getFinancialReward(), $_financialReward);
    }
}

// run tests
(new TraitFinancialReward())->run();
