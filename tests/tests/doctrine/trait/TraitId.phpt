<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Id;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitId extends TestCase
{
    public function testId() : void
    {
        $id  = new class
        {
            use Id;

            public function set(int $id) : void
            {
                $this->id = $id;
            }
        };
        $_id = intval(Random::generate(2, '0-9'));
        $id->set($_id);

        Assert::same($id->getId(), $_id);

        $id2 = clone  $id;
        Assert::same($id2->getId(), null);
    }
}

// run tests
(new TraitId())->run();
