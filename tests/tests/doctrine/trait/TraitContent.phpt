<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Content;
use Nette\Utils\Random;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitContent extends TestCase
{
    public function testName() : void
    {
        $content  = new class
        {
            use Content;

            public function set(string $content) : void
            {
                $this->content = $content;
            }
        };
        $_content = Random::generate();
        $content->set($_content);

        Assert::same($content->getContent(), $_content);
    }
}

// run tests
(new TraitContent())->run();
