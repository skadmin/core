<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Traits\Term;
use Nette\Utils\DateTime;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../Bootstrap2.php';

class TraitTerm extends TestCase
{
    public function testTerm() : void
    {
        $term = new class
        {
            use Term;

            public function set(DateTime $termFrom, ?DateTime $termTo = null) : void
            {
                $this->termFrom = $termFrom;
                $this->termTo   = $termTo;
            }
        };

        $termFrom = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', mt_rand(1262055681, 1262055681)));
        $termTo   = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', mt_rand(1262055681, 1262055681)));

        $term->set($termFrom, $termTo);


        Assert::true($term->getTermFrom() instanceof \DateTimeInterface);
        Assert::same($term->getTermFrom()->format('dmYHis'), $termFrom->format('dmYHis'));
        Assert::true($term->getTermTo() instanceof \DateTimeInterface);
        Assert::same($term->getTermTo()->format('dmYHis'), $termTo->format('dmYHis'));

        $term->set($termFrom, null);

        Assert::same($term->getTermTo(), null);
    }
}

// run tests
(new TraitTerm())->run();
