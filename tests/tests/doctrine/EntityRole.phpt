<?php

declare(strict_types=1);

namespace Tests\Doctrine;

use App\Model\Doctrine\Role\RoleFacade;
use Nette\DI\Container;
use Tester\Assert;
use Tester\TestCase;

$container = require __DIR__ . '/../Bootstrap.php';

class EntityRole extends TestCase
{
    /** @var Container */
    private $container;

    /** @var RoleFacade */
    private $facadeRole;

    public function __construct(Container $container)
    {
        $this->container  = $container;
        $this->facadeRole = $this->container->getByType('\App\Model\Doctrine\Role\RoleFacade');
    }

    public function testRole() : void
    {
        Assert::same($this->facadeRole->get()->getId(), null);
    }
}

// run tests
(new EntityRole($container))->run();
