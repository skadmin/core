<?php

declare(strict_types=1);

use Nette\Configurator;
use Nette\Utils\Finder;
use Tester\Environment;

require __DIR__ . '/../../vendor/autoload.php';

Environment::setup();

$configurator = new Configurator();
$configurator->addParameters(['appDir' => __DIR__ . '/../../app']);

$configurator->setDebugMode(false);
$configurator->setTimeZone('Europe/Prague');

$configurator->setTempDirectory(__DIR__ . '/../_temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__ . '/../../app')
    ->register();

$configurator->addConfig(__DIR__ . '/../../app/config/parameters/tester2.neon');
$configurator->addConfig(__DIR__ . '/../../app/config/common.neon');
$configurator->addConfig(__DIR__ . '/../../app/config/local.neon');

// add config from package
$masks = '/*/config/*.neon';
$from  = __DIR__ . '/../../vendor/skadmin/';
foreach (Finder::findFiles($masks)->from($from) as $key => $file) {
    $configurator->addConfig($key);
}

return $configurator->createContainer();
