<?php

declare(strict_types=1);

namespace App\Components;

use Exception;
use Nette\Bridges\ApplicationLatte\Template;

class Control extends \Nette\Application\UI\Control
{
    public function getComponentTemplate() : Template
    {
        $template = $this->template;

        if (! $template instanceof Template) {
            throw new Exception('Template must be instanceof "Nette\Bridges\ApplicationLatte\Template"');
        }

        return $template;
    }
}
