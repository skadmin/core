<?php

declare(strict_types=1);

namespace App\Components\Form;

/**
 * Interface IFormSignFactory
 */
interface IFormLogInFactory
{
    public function create() : FormLogIn;
}
