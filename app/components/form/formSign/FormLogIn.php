<?php

declare(strict_types=1);

namespace App\Components\Form;

use Nette\Utils\ArrayHash;
use SkadminUtils\FormControls\UI\Form as SkadminForm;

class FormLogIn extends FormControl
{
    /** @var callable[] */
    public $onForgotPassword = [];

    /** @var callable[] */
    public $onRegistration = [];

    public function processOnSuccess(SkadminForm $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formLogIn.latte'));
        $template->render();
    }

    public function processOnForgotPassword() : void
    {
        $this->onForgotPassword();
    }

    public function processOnRegistration() : void
    {
        $this->onRegistration();
    }

    protected function createComponentForm() : SkadminForm
    {
        $form = new SkadminForm();
        $form->setTranslator($this->translator);

        $form->addEmail('username', 'form.log-in.username')
            ->setRequired('form.log-in.username.req')
            ->setHtmlAttribute('placeholder', 'form.log-in.username.placeholder');
        $form->addPassword('password', 'form.log-in.password')
            ->setRequired('form.log-in.password.req')
            ->setHtmlAttribute('placeholder', 'form.log-in.password.placeholder');

        $form->addSubmit('send', 'form.log-in.send');
        $form->addSubmit('forgot_password', 'form.log-in.forgot-password')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnForgotPassword'];
        $form->addSubmit('registration', 'form.log-in.registration')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnRegistration'];

        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
