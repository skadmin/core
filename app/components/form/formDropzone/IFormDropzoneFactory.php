<?php

declare(strict_types=1);

namespace App\Components\Form;

/**
 * Interface IFormDropzoneFactory
 */
interface IFormDropzoneFactory
{
    public function create(string $prefix = 'default') : FormDropzone;
}
