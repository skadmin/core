<?php

declare(strict_types=1);

namespace App\Components\Form;

use App\Model\System\APackageControl;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form as SkadminForm;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class FormDropzone extends FormControl
{
    use APackageControl;

    /** @var callable[]&(callable(Nette\Http\FileUpload\FileUpload): void)[]; */
    public $onFileUpload;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var string */
    private $prefix = '';

    public function __construct(Translator $translator, LoaderFactory $webLoader, string $prefix = 'default')
    {
        parent::__construct($translator);
        $this->webLoader = $webLoader;

        $this->prefix = $prefix;
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [$this->webLoader->createCssLoader('dropzone')];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [$this->webLoader->createJavaScriptLoader('dropzone')];
    }

    public function processOnSuccess(SkadminForm $form, ArrayHash $values) : void
    {
        /** @var FileUpload $file */
        $file = $values->file;

        if ($file->isOk()) {
            $this->onFileUpload($file);
        } else {
            $this->onError($form);
        }
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formDropzone.latte');

        $template->prefix     = $this->prefix;
        $template->dropzoneId = Random::generate();

        $template->redrawLink = $this->link('redraw!');

        $template->render();
    }

    protected function createComponentForm() : SkadminForm
    {
        $form = new SkadminForm();
        $form->setTranslator($this->translator);

        $form->addUpload('file');

        $form->addSubmit('send', 'form.dropzone.send');

        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function handleRedraw() : void
    {
        $this->onRedraw();
    }
}
