<?php

declare(strict_types=1);

namespace App\Components\Form;

use Nette\Utils\ArrayHash;
use SkadminUtils\FormControls\UI\Form as SkadminForm;

class FormRecoveryPassword extends FormControl
{
    public function processOnSuccess(SkadminForm $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formRecoveryPassword.latte'));
        $template->render();
    }

    protected function createComponentForm() : SkadminForm
    {
        $form = new SkadminForm();
        $form->setTranslator($this->translator);

        $form->addPasswordMedium('password', 'form.recovery-password.password')
            ->setRequired('form.recovery-password.password.req')
            ->setHtmlAttribute('placeholder', 'form.recovery-password.password.placeholder');
        $form->addPasswordMedium('re_password', 'form.recovery-password.re-password')
            ->setRequired('form.recovery-password.re-password.req')
            ->addRule(Form::EQUAL, 'form.recovery-password.re-password.equal.password', $form['password'])
            ->setHtmlAttribute('placeholder', 'form.recovery-password.re-password.placeholder');

        $form->addSubmit('send', 'form.recovery-password.send');

        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
