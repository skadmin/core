<?php

declare(strict_types=1);

namespace App\Components\Form;

/**
 * Interface IFormSignFactory
 */
interface IFormRecoveryPasswordFactory
{
    public function create() : FormRecoveryPassword;
}
