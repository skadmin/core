<?php

declare(strict_types=1);

namespace App\Components\Form;

use Nette\Utils\ArrayHash;
use SkadminUtils\FormControls\UI\Form as SkadminForm;

class FormForgotPassword extends FormControl
{
    /** @var callable[] */
    public $onLogIn;

    public function processOnSuccess(SkadminForm $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formForgotPassword.latte'));
        $template->render();
    }

    public function processOnLogIn() : void
    {
        $this->onLogIn();
    }

    protected function createComponentForm() : SkadminForm
    {
        $form = new SkadminForm();
        $form->setTranslator($this->translator);

        $form->addEmail('username', 'form.forgot-password.username')
            ->setRequired('form.forgot-password.username.req')
            ->setHtmlAttribute('placeholder', 'form.forgot-password.username.placeholder');

        $form->addSubmit('send', 'form.forgot-password.send');
        $form->addSubmit('log_in', 'form.forgot-password.log-in')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnLogIn'];

        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
