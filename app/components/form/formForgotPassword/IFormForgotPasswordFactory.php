<?php

declare(strict_types=1);

namespace App\Components\Form;

/**
 * Interface IFormForgotPasswordFactory
 */
interface IFormForgotPasswordFactory
{
    public function create() : FormForgotPassword;
}
