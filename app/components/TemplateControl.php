<?php

declare(strict_types=1);

namespace App\Components\Grid;

use App\Components\Control;
use App\CoreModule\Presenters\CorePresenter;
use Nette\SmartObject;
use Skadmin\Translator\Translator;

/**
 * Class TemplateControl
 */
class TemplateControl extends Control
{
    use SmartObject;

    /** @var callable[] */
    public $onFlashmessage;

    /** @var Translator */
    protected $translator;

    /** @var CorePresenter */
    protected $presenter;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }
}
