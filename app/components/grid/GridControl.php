<?php

declare(strict_types=1);

namespace App\Components\Grid;

use App\Components\Control;
use App\CoreModule\Presenters\CorePresenter;
use Nette\Security\User;
use Nette\SmartObject;
use Skadmin\Translator\Translator;

/**
 * Class GridControl
 */
class GridControl extends Control
{
    use SmartObject;

    /** @var callable[] */
    public $onFlashmessage;

    /** @var Translator */
    protected $translator;

    /** @var User */
    protected $loggedUser;

    /** @var CorePresenter */
    protected $presenter;

    public function __construct(Translator $translator, User $user)
    {
        $this->translator = $translator;
        $this->loggedUser = $user;
    }

    protected function isAllowed(string $resource, string $privilege) : bool
    {
        return $this->loggedUser->isAllowed($resource, $privilege);
    }
}
