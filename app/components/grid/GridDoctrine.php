<?php

declare(strict_types=1);

namespace App\Components\Grid;

use App\CoreModule\Presenters\CorePresenter;
use DateTime;
use InvalidArgumentException;
use Nette\Forms\Container;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\IControl;
use Skadmin\Seo\Utils\UtilsSeoRenderSeoHref;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Ublaboo\DataGrid\Components\DataGridPaginator\DataGridPaginator;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Filter\FilterSelect;
use Ublaboo\DataGrid\Filter\FilterText;
use Ublaboo\DataGrid\Filter\IFilterDate;
use Ublaboo\DataGrid\InlineEdit\InlineAdd;
use Ublaboo\DataGrid\InlineEdit\InlineEdit;
use UnexpectedValueException;

class GridDoctrine extends DataGrid
{
    /** @var CorePresenter */
    protected $presenter;

    /** @var Translator */
    protected $translator;

    public function __construct(CorePresenter $presenter)
    {
        $this->presenter  = $presenter;
        $this->translator = $presenter->translator;

        parent::__construct();
        $this->setTemplateFile(__DIR__ . '/ublaboo-datagrid.latte');
        $this->setItemsPerPageList([10, 15, 20, 30, 40, 50]);

        $this::$iconPrefix = 'fas fa-';
    }

    /**
     * @param string $treeViewHasChildrenColumn
     */
    public function setTreeView(callable $getChildrenCallback, $treeViewHasChildrenColumn = 'has_children') : DataGrid
    {
        parent::setTreeView($getChildrenCallback, $treeViewHasChildrenColumn);
        $this->setTemplateFile(__DIR__ . '/ublaboo-datagrid-tree.latte');
        return $this;
    }

    /**
     * @param string[]|array|string $columns
     */
    public function addFilterText(string $key, string $name, $columns = null) : FilterText
    {
        $filter = parent::addFilterText($key, $name, $columns);
        $filter->setTemplate(__DIR__ . '/ublaboo-datagrid-filter-text-select.latte');
        return $filter;
    }

    /**
     * @param string[] $options
     */
    public function addFilterSelect(string $key, string $name, array $options, ?string $column = null) : FilterSelect
    {
        $filter = parent::addFilterSelect($key, $name, $options, $column);
        $filter->setTemplate(__DIR__ . '/ublaboo-datagrid-filter-text-select.latte');
        return $filter;
    }

    public function render() : void
    {
        if ($this->getInlineAdd() !== null) {
            $this->getInlineAdd()->setClass('btn btn-xs btn-primary ajax');
        }

        if ($this->getInlineEdit() !== null) {
            $this->getInlineEdit()->setClass('btn btn-xs btn-primary ajax');
        }

        if ($this->getItemsDetail() !== null) {
            $this->getItemsDetail()->setClass('btn btn-xs btn-outline-primary ajax');
        }

        if ($this->getPaginator() instanceof DataGridPaginator) {
            $this->getPaginator()->setTemplateFile(__DIR__ . '/ublaboo-datagrid-paginator.latte');
        }

        parent::render();
    }

    /**
     * @param mixed[] $values
     */
    public function setFilterContainerDefaults(Container $container, array $values, ?string $parentKey = null) : void
    {
        foreach ($container->getComponents() as $key => $control) {
            if (! isset($values[$key])) {
                continue;
            }

            if ($control instanceof Container) {
                $this->setFilterContainerDefaults($control, (array) $values[$key], (string) $key);

                continue;
            }

            $value = $values[$key];

            if ($value instanceof DateTime) {
                if ($parentKey !== null) {
                    $filter = $this->getFilter($parentKey);
                } else {
                    $filter = $this->getFilter((string) $key);
                }

                if ($filter instanceof IFilterDate) {
                    $value = $value->format($filter->getPhpFormat());
                }
            }

            try {
                if (! $control instanceof IControl) {
                    throw new UnexpectedValueException();
                }

                if ($control instanceof SelectBox) {
                    if (isset($control->getItems()[$value])) {
                        $control->setValue($value);
                    }
                } else {
                    $control->setValue($value);
                }
            } catch (InvalidArgumentException $e) {
                if ($this->strictSessionFilterValues) {
                    throw $e;
                }
            }
        }
    }

    public function getInlineAddPure() : InlineAdd
    {
        /** @var InlineAdd $inlineAdd */
        $inlineAdd = parent::getInlineAdd();
        return $inlineAdd;
    }

    public function getInlineEditPure() : InlineEdit
    {
        /** @var InlineEdit $inlineEdit */
        $inlineEdit = parent::getInlineEdit();
        return $inlineEdit;
    }

    /**
     * @param SimpleTranslation|string $name
     */
    public function addToolbarSeo(string $type, $name) : void
    {
        $this->toolbarButtons['toolbar-seo'] = UtilsSeoRenderSeoHref::createDataGridSeoToolbar($this, $type, $this->translator->translate($name));
    }
}
