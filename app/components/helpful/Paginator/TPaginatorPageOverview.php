<?php

declare(strict_types=1);

namespace App\Components\Helpful;

trait TPaginatorPageOverview
{
    /** @var IPaginatorFactory @inject */
    public $iPaginatorFactory;

    /** @var Paginator */
    private $paginator;

    /** @var int @persistent */
    public $page = 1;

    protected function createComponentPaginator() : Paginator
    {
        $this->paginator->onChangePage[] = [$this, 'paginatorOnChangePage'];
        return $this->paginator;
    }

    public function paginatorOnChangePage(int $page) : void
    {
        $this->page = $page;
        $this->paginator->setPage($page);
        $this->payload->url = $this->link('this');
        $this->redrawControl('snipOverview');
    }
}
