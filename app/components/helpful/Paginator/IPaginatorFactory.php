<?php

declare(strict_types=1);

namespace App\Components\Helpful;

/**
 * Interface IPaginatorFactory
 */
interface IPaginatorFactory
{
    public function create() : Paginator;
}
