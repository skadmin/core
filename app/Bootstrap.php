<?php

declare(strict_types=1);

namespace App;

use Nette\Configurator;
use Nette\Utils\Finder;
use Nette\Utils\Strings;
use function in_array;

class Bootstrap
{
    public const LOCAL_CONFIG = [
        '127.0.0.1',
        '::1',
    ];

    public static function boot() : Configurator
    {
        $configurator = new Configurator();

        if (Strings::endsWith($_SERVER['HTTP_HOST'] ?? $_SERVER['VIRTUAL_HOST'] ?? '', '.loc')
            ||
            (isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], self::LOCAL_CONFIG, true))
            ||
            (isset($_SERVER['PHP_SELF']) && $_SERVER['PHP_SELF'] === 'bin/console.php')) {
            $localServer = true;
            $configurator->setDebugMode(true);
        } else {
            $localServer = false;
            $configurator->setDebugMode('91.228.45.18');
        }
        //$configurator->setDebugMode(false);

        $configurator->enableTracy(__DIR__ . '/../log');

        $configurator->setTimeZone('Europe/Prague');
        $configurator->setTempDirectory(__DIR__ . '/../temp');

        $configurator->createRobotLoader()
            ->addDirectory(__DIR__)
            ->register();

        $configurator->addConfig(__DIR__ . '/config/parameters/website.neon');
        $configurator->addConfig(__DIR__ . '/config/common.neon');

        if ($localServer) {
            $configurator->addConfig(__DIR__ . '/config/local.neon');
        } else {
            $configurator->addConfig(__DIR__ . '/config/production.neon');
        }

        // add config from skadmin in vendor
        $masks = '/*/config/*.neon';
        $from  = [
            __DIR__ . '/../vendor/skadmin/',
            __DIR__ . '/../vendor/skadmin-utils/',
        ];
        foreach (Finder::findFiles($masks)->from($from) as $key => $file) {
            $configurator->addConfig($key);
        }

        return $configurator;
    }
}
