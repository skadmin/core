<?php

declare(strict_types=1);

namespace App\Router;

use App\Model\System\SystemDir;
use Nette\Application\Routers\RouteList;
use Nette\DI\Container;
use Nette\Utils\Finder;
use Nette\Utils\Strings;
use function count;
use function explode;
use function get_class;
use function ksort;
use function sprintf;

final class RouterFactory
{
    /** @var ARouterFactory[] */
    private static $routers = [];

    public function __construct(Container $container, SystemDir $systemDir)
    {
        // maybe better way?
        $path = $systemDir->getPathApp(['modules']);
        foreach (Finder::findFiles('*Module/routers/*RouterFactory.php')->from($path) as $key => $file) {
            $prefix    = Strings::match($key, '/\/(.*)\/(.*)Module\//');
            $className = explode('/', $key);
            $className = Strings::substring($className[count($className) - 1], 0, -4);

            if (! isset($prefix[2])) {
                continue;
            }

            $class           = sprintf('App\%sModule\Presenters\%s', Strings::firstUpper($prefix[2]), $className);
            self::$routers[] = $container->getByType($class);
        }
    }

    public function createRouter() : RouteList
    {
        $routerList = new RouteList();

        /** @var ARouterFactory[] $routerOrder */
        $routerOrder = [];
        foreach (self::$routers as $router) {
            if (isset($routerOrder[$router->getSequence()])) {
                throw new ExceptionSameSequence(sprintf('%s|%d', get_class($router), $router->getSequence()));
            }

            $routerOrder[$router->getSequence()] = $router;
        }

        ksort($routerOrder);

        foreach ($routerOrder as $router) {
            $router->createRouter($routerList);
        }

        return $routerList;
    }
}
