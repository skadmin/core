<?php

declare(strict_types=1);

namespace App\Router;

use Nette\Application\Routers\RouteList;

abstract class ARouterFactory
{
    abstract public function createRouter(RouteList &$routerList) : void;

    public function getSequence() : int
    {
        return 99;
    }
}
