<?php

declare(strict_types=1);

namespace App\Router;

use Exception;
use Throwable;
use function explode;
use function sprintf;

class ExceptionSameSequence extends Exception
{
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        $eMessage = explode('|', $message);
        parent::__construct(sprintf('Router "%s" with sequence [%s] is not free.', $eMessage[0], $eMessage[1] ?? '--'), $code, $previous);
    }
}
