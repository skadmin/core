<?php

declare(strict_types=1);

namespace App\CoreModule\Presenters;

use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

trait Webloader
{
    /** @var LoaderFactory  @inject */
    public $webLoader;

    protected function createComponentDaterangePicker() : CssLoader
    {
        return $this->webLoader->createCssLoader('daterangePicker');
    }

    protected function createComponentCssCustomFileInput() : CssLoader
    {
        return $this->webLoader->createCssLoader('customFileInput');
    }

    protected function createComponentCssDropzone() : CssLoader
    {
        return $this->webLoader->createCssLoader('dropzone');
    }

    protected function createComponentCssJQueryUi() : CssLoader
    {
        return $this->webLoader->createCssLoader('jQueryUi');
    }

    protected function createComponentCssFancyBox() : CssLoader
    {
        return $this->webLoader->createCssLoader('fancyBox');
    }

    protected function createComponentJsMoment() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('moment');
    }

    protected function createComponentJsDaterangePicker() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('daterangePicker');
    }

    protected function createComponentJsCustomFileInput() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('customFileInput');
    }

    protected function createComponentJsDropzone() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('dropzone');
    }

    protected function createComponentJsChoosen() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('choosen');
    }

    protected function createComponentJsReCaptcha() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('reCaptcha');
    }

    protected function createComponentJsReCaptchaInvisible() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('reCaptchaInvisible');
    }

    protected function createComponentJsJQueryUi() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('jQueryUi');
    }

    protected function createComponentJsFancyBox() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('fancyBox');
    }
}
