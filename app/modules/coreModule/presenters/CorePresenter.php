<?php

declare(strict_types=1);

namespace App\CoreModule\Presenters;

use App\Model\Doctrine\Package\PackageFacade;
use Nette\Application\UI\Presenter;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

class CorePresenter extends Presenter
{
    use Webloader;

    /** @var Translator @inject */
    public $translator;

    /** @var PackageFacade @inject */
    public $facadePackage;

    /**
     * @param SimpleTranslation|string $message
     */
    public function onFlashMessage($message, string $type) : void
    {
        $this->flashMessage($message, $type);
    }

    protected function beforeRender() : void
    {
        $this->template->setTranslator($this->translator);
        parent::beforeRender();
    }

    protected function afterRender() : void
    {
        if ($this->hasFlashSession()) {
            $this->redrawControl('snipFlash');
        }

        parent::afterRender();
    }
}
