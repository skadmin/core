<?php

declare(strict_types=1);

namespace App\ServiceModule\Presenters;

use App\Router\ARouterFactory;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

final class RouterFactory extends ARouterFactory
{
    public function getSequence() : int
    {
        return 0;
    }

    public function createRouter(RouteList &$routerList) : void
    {
        $router       = new RouteList('Service');
        $router[]     = new Route('service/<presenter>/<action>[/<id>]');
        $routerList[] = $router;
    }
}
