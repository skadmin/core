<?php

declare(strict_types=1);

namespace App\ServiceModule\Presenters;

use App\Bootstrap;
use App\Model\System\SystemDir;
use Contributte\Console\Application;
use Exception;
use Nette\Application\UI\Presenter;
use Nette\Utils\FileSystem;
use Nette\Utils\Random;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Tracy\Debugger;
use const DIRECTORY_SEPARATOR;
use function count;
use function is_dir;
use function is_link;
use function rmdir;
use function scandir;
use function sprintf;
use function unlink;

final class MigrationPresenter extends Presenter
{
    private const MIGRATION_ARRAY_INPUT = [
        'command'          => 'migration:migrate',
        '--no-interaction' => true,
    ];
    private const MIGRATIONS_CONFIG     = [
        null,
        ['config', 'includes', 'nettrine', 'nettrine.migrations.skadmin.neon'],
        ['config', 'includes', 'nettrine', 'nettrine.migrations.skadmin-utils.neon'],
        ['config', 'includes', 'nettrine', 'nettrine.migrations.after.neon'],
    ];

    /** @var SystemDir @inject */
    public $systemDir;

    public function actionCleareCache() : void
    {
        if ($this->getParameter('griston') !== 'skadmin-1991-vymazpamet') {
            return;
        }

        $this->clearCache();
    }

    public function actionMigrate() : void
    {
        if ($this->getParameter('griston') === 'skadmin-1991-spustmigrace') {
            $this->systemDir->createDir($this->systemDir->getPathWww(['storage', 'rf', 't']), 0750);
            $this->systemDir->createDir($this->systemDir->getPathWww(['storage', 'rf', 's']), 0750);

            $input = new ArrayInput(self::MIGRATION_ARRAY_INPUT);

            $this->clearCache();

            foreach (self::MIGRATIONS_CONFIG as $config) {
                $this->runMigration($input, $config);
            }

            $this->clearCache();
        }

        $this->terminate();
    }

    /**
     * @param string[]|array|null $config
     *
     * @throws Exception
     */
    private function runMigration(ArrayInput $input, ?array $config) : void
    {
        $output = new BufferedOutput();

        $configurator = Bootstrap::boot();

        if ($config !== null) {
            $configurator->addConfig($this->systemDir->getPathApp($config));
        }

        /** @var Application $application */
        $application = $configurator
            ->addParameters(['consoleMode' => true])
            ->createContainer()
            ->getByType(Application::class);

        $application->setAutoExit(false);
        $application->run($input, $output);

        $content = $output->fetch();
        Debugger::log($content, 'migration');
    }

    private function clearCache() : void
    {
        $pathCache   = $this->systemDir->getPathApp(['..', 'temp', 'cache']);
        $pathProxies = $this->systemDir->getPathApp(['..', 'temp', 'proxies']);

        $randomSufix = sprintf('del-%s', Random::generate());

        $_pathCache   = $this->systemDir->getPathApp(['..', 'temp', sprintf('cache-%s', $randomSufix)]);
        $_pathProxies = $this->systemDir->getPathApp(['..', 'temp', sprintf('proxies-%s', $randomSufix)]);

        $paths = [];
        if (is_dir($pathCache)) {
            FileSystem::rename($pathCache, $_pathCache);
            $paths[] = $_pathCache;
        }

        if (is_dir($pathProxies)) {
            FileSystem::rename($pathProxies, $_pathProxies);
            $paths[] = $_pathProxies;
        }

        if (count($paths) === 0) {
            return;
        }

        foreach ($paths as $path) {
            $this->recursiveRemoveDirAndContent($path);
        }
    }

    private function recursiveRemoveDirAndContent(string $path) : void
    {
        if (! is_dir($path)) {
            return;
        }

        $objects = scandir($path);
        if ($objects !== false) {
            foreach ($objects as $object) {
                if ($object === '.' || $object === '..') {
                    continue;
                }

                if (is_dir($path . DIRECTORY_SEPARATOR . $object) && ! is_link($path . '/' . $object)) {
                    $this->recursiveRemoveDirAndContent($path . DIRECTORY_SEPARATOR . $object);
                } else {
                    unlink($path . DIRECTORY_SEPARATOR . $object);
                }
            }
        }
        rmdir($path);
    }
}
