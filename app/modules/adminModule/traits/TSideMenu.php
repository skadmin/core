<?php

declare(strict_types=1);

namespace App\AdminModule\Traits;

use App\Model\Doctrine\Package\Package;
use App\Model\Doctrine\Role\Privilege;
use App\Model\System\ABaseControl;
use Nette\Application\UI\InvalidLinkException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use function count;
use function sprintf;

trait TSideMenu
{
    /** @var ABaseControl|null */
    private $currentPackage = null;

    private function createSideMenu() : void
    {
        $menu     = [];
        $_package = [];
        foreach ($this->facadePackage->getAll() as $package) {
            $control                           = $package->getBaseControl();
            $_package[$package->getWebalize()] = $control;

            if ($control === null || ! $this->userIsAllowed($control->getResource(), Privilege::READ)) {
                continue;
            }

            $controlMenu = $control->getMenu();

            $items = $controlMenu->items;
            if (count($items) === 0) {
                continue;
            } elseif (count($items) === 1) {
                $_menu = $this->createSideMenuLi($package, $controlMenu->control, $items[0], $controlMenu->icon);
            } else {
                $_menu = [
                    'name'          => $this->translator->translate(sprintf('%s.%s', $package->getWebalize(), 'title')),
                    'icon'          => $controlMenu->icon,
                    'items'         => [],
                    'isCurrentLink' => false,
                ];

                foreach ($items as $render) {
                    $_menu['items'][] = $this->createSideMenuA($package, $controlMenu->control, $render);

                    if (! $this->isCurrentLinkActive($controlMenu->control)) {
                        continue;
                    }

                    $_menu['isCurrentLink'] = true;
                }

                $_menu = ArrayHash::from($_menu);
            }

            $menu[] = $_menu;
        }

        $this->template->sideMenu = $menu;
        $this->template->package  = $_package;
    }

    private function createSideMenuLi(Package $package, ABaseControl $packageControl, string $render, Html $icon) : Html
    {
        $isCurrentLink = $this->isCurrentLinkActive($packageControl);

        $li = Html::el('li', ['class' => sprintf('nav-item %s', $isCurrentLink ? 'active' : '')]);

        $li->addHtml($this->createSideMenuA($package, $packageControl, $render, $icon));

        return $li;
    }

    private function isCurrentLinkActive(ABaseControl $packageControl) : bool
    {
        return $this->currentPackage !== null && $this->currentPackage->getResource() === $packageControl->getResource();
    }

    /**
     * @throws InvalidLinkException
     */
    private function createSideMenuA(Package $package, ABaseControl $packageControl, string $render, ?Html $icon = null) : Html
    {
        $class = $this->isCurrentLinkActive($packageControl) ? 'active' : '';

        $link = $this->link('Component:default', [
            'package' => $packageControl,
            'render'  => $render,
        ]);

        $span = Html::el('span')
            ->setText($this->translator->translate(sprintf('%s.%s', $package->getWebalize(), $render)));

        $a = Html::el('a', [
            'class' => sprintf('nav-link %s', $class),
            'href'  => $link,
        ]);

        if ($icon !== null) {
            $a->addHtml($icon)
                ->addText(' ');
        }

        $a->addHtml($span);

        return $a;
    }
}
