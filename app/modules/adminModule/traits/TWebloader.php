<?php

declare(strict_types=1);

namespace App\AdminModule\Traits;

use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

trait TWebloader
{
    protected function createComponentCss() : CssLoader
    {
        return $this->webLoader->createCssLoader('admin');
    }

    protected function createComponentJsHead() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('adminHead');
    }

    protected function createComponentJsFoot() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('adminFoot');
    }

    protected function createComponentJsAdminTinyMce() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('adminTinyMce');
    }
}
