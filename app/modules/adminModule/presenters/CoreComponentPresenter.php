<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\System\ABaseControl;
use App\Model\System\Strings as SystemStrings;
use Nette\Application\UI\Control;
use Nette\DI\Container;
use Nette\DI\MissingServiceException;
use Nette\Utils\Strings;
use Tracy\Debugger;
use Tracy\ILogger;
use function property_exists;
use function sprintf;
use function str_replace;

class CoreComponentPresenter extends CoreAdminAuthorizedPresenter
{
    /** @var Control */
    private $control;

    /** @var Container @inject */
    public $container;

    public function renderDefault() : void
    {
        $this->template->title   = $this->control->getTitle();
        $this->template->jss     = $this->control->getJs();
        $this->template->csss    = $this->control->getCss();
        $this->template->control = $this->control;
    }

    protected function prepareControl(?ABaseControl $package, ?string $_render, ?int $id, string $module = ABaseControl::MODULE_ADMIN) : void
    {
        $render       = Strings::firstLower(SystemStrings::camelize($_render));
        $controlClass = $package->getControlClass($module, $render);

        $this->template->currentPackage = $package;

        try {
            if (! ($package instanceof ABaseControl)) {
                throw new MissingServiceException('Package is not set');
            }
            $controlClass = $package->getControlClass($module, $render);

            $control = $this->container->getByType($controlClass);

            if ($id === null) {
                $this->control = $control->create();
            } else {
                $this->control = $control->create($id);
            }

            if (property_exists($this->control, 'onFlashmessage')) {
                $this->control->onFlashmessage[] = [$this, 'onFlashMessage'];
            }

            $componentName = Strings::lower(sprintf('%s_%s_%s', str_replace('-', '', (string) $package->getResource()), $module, $render));
            $this->addComponent($this->control, $componentName);

            $this->template->isModal = false;

            if ($this->control->isModal() && $this->isAjax()) {
                $this->control->setDrawBox(false);
                $this->payload->isModal = true;
                $this->redrawControl('snipModal');
            }
        } catch (MissingServiceException $e) {
            Debugger::log($e->getMessage(), ILogger::ERROR);
            $this->template->class = $controlClass ?? 'control-not-set';
            $this->setView('packageNotFound');
        }
    }
}
