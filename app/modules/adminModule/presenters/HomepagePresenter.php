<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

final class HomepagePresenter extends CoreAdminAuthorizedPresenter
{
    public function actionDefault() : void
    {
    }

    public function renderDefault() : void
    {
        $this->template->title = 'homepage.default.title';
    }
}
