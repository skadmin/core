<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\Form\FormAccount;
use App\AdminModule\Components\Form\IFormAccountFactory;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use SkadminUtils\FormControls\UI\Form;
use function method_exists;

final class AccountPresenter extends CoreAdminAuthorizedPresenter
{
    /** @var UserFacade @inject */
    public $facade;

    /** @var IFormAccountFactory @inject */
    public $iFormAccountFactory;

    /** @var User */
    private $eUser;

    public function actionDefault() : void
    {
        $this->eUser = $this->facade->get($this->user->id);
    }

    public function renderDefault() : void
    {
        $this->template->title = 'account.default.title';
    }

    public function formAccountOnSubmit(Form $form, User $user, string $submitted) : void
    {
        $identity = $this->getUser()->getIdentity();
        if ($identity === null) {
            return;
        }

        if (method_exists($identity, 'updateAccount')) {
            $identity->updateAccount(
                $user->getName(),
                $user->getSurname(),
                $user->getEmail(),
                $user->getPhone(),
                $user->getFacebook(),
                $user->getAddress(),
                $user->getBirthdate(),
                $user->getNote(),
                $user->getExperience(),
                $user->getImagePreview()
            );
        }

        $this->redrawControl('snipAreaTopbar');
        $this->redrawControl('snipAccount1');
        $this->redrawControl('snipAccount2');
    }

    protected function startup() : void
    {
        parent::startup();
        $this->isAllowed('account', 'read');
    }

    protected function createComponentFormAccount() : FormAccount
    {
        $control                   = $this->iFormAccountFactory->create($this->eUser);
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        $control->onSubmit[]       = [$this, 'formAccountOnSubmit'];
        return $control;
    }
}
