<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Traits\TSideMenu;
use App\AdminModule\Traits\TWebloader;
use App\CoreModule\Presenters\CorePresenter;
use App\Model\Security\AuthenticatorAdmin;
use App\Model\Security\AuthorizatorAdmin;
use Contributte\ImageStorage\ImageStoragePresenterTrait;
use function sprintf;

class CoreAdminPresenter extends CorePresenter
{
    use ImageStoragePresenterTrait;
    use TWebloader;
    use TSideMenu;

    /** @var AuthorizatorAdmin @inject */
    public $authorizatorAdmin;

    /** @var AuthenticatorAdmin @inject */
    public $authenticatorAdmin;

    public function renderAccessDenied() : void
    {
        $this->template->title = 'core-admin.access-denied.title';
        $this->template->setFile(__DIR__ . '/../templates/_errors/accessDenided.latte');
    }

    public function renderFileNotFound(string $hash) : void
    {
        $this->template->title = 'core-admin.file-not-found.title';
        $this->template->setFile(__DIR__ . '/../templates/_errors/fileNotFound.latte');
        $this->template->hash = $hash;
    }

    public function renderPackageNotFound() : void
    {
        $this->template->title = 'core-admin.package-not-found.title';
        $this->template->setFile(__DIR__ . '/../templates/_errors/packageNotFound.latte');
    }

    protected function beforeRender() : void
    {
        parent::beforeRender();

        $this->currentPackage = $this->getParameter('package');
        $this->createSideMenu();

        $cssFront  = [];
        $cssLoader = $this->webLoader->createCssLoader('front');
        foreach ($cssLoader->getCompiler()->generate() as $file) {
            $cssFront[] = sprintf('%s/%s?%s', $cssLoader->getTempPath(), $file->getFile(), $file->getLastModified());
        }
        $this->template->cssFront = $cssFront;
    }

    protected function startup() : void
    {
        parent::startup();
        $this->translator->setModule('admin');

        $this->user->getStorage()->setNamespace('admin');
        $this->user->setAuthorizator($this->authorizatorAdmin);
        $this->user->setAuthenticator($this->authenticatorAdmin);
    }

    protected function isAllowed(?string $resource, ?string $privilege) : void
    {
        if ($this->userIsAllowed($resource, $privilege)) {
            return;
        }

        $this->setView('accessDenied');
    }

    protected function userIsAllowed(?string $resource, ?string $privilege) : bool
    {
        return $this->getUser()->isAllowed($resource, $privilege);
    }
}
