<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\Form\FormUserChangePassword;
use App\AdminModule\Components\Form\FormUserEdit;
use App\AdminModule\Components\Form\IFormUserChangePasswordFactory;
use App\AdminModule\Components\Form\IFormUserEditFactory;
use App\AdminModule\Components\Grid\GridUser;
use App\Components\Grid\GridDoctrine;
use App\Model\Doctrine\Role\RoleFacade;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use Skadmin\Mailing\Model\MailService;
use Nette\Security\IIdentity;
use Skadmin\Translator\SimpleTranslation;
use SkadminUtils\FormControls\UI\Form;
use function method_exists;

final class UserPresenter extends CoreAdminAuthorizedPresenter
{
    /** @var MailService @inject */
    public $mailService;

    /** @var IFormUserEditFactory @inject */
    public $iFormUserEditFactory;

    /** @var IFormUserChangePasswordFactory @inject */
    public $iFormUserChangePasswordFactory;

    /** @var UserFacade @inject */
    public $facadeUser;

    /** @var RoleFacade @inject */
    public $facadeRole;

    /** @var User */
    private $eUser;

    public function actionDefault() : void
    {
        $this->isAllowed('user', 'read');
    }

    public function renderDefault() : void
    {
        $this->template->title = 'user.default.title';
    }

    public function actionNew() : void
    {
        $this->isAllowed('user', 'write');
        $this->setView('edit');
        $this->actionEdit(null);
    }

    public function actionEdit(?int $id = null) : void
    {
        $this->isAllowed('user', 'write');
        $this->eUser = $this->facadeUser->get($id);

        if ($this->eUser->isLoaded()) {
            $this->template->title = new SimpleTranslation('user.edit.title %s', [$this->eUser->getFullName()]);
        } else {
            $this->template->title = 'user.edit.title';
        }

        if (! $this->isAjax()) {
            return;
        }

        $this->payload->isModal = true;
        $this->redrawControl('snipModal');
    }

    public function actionChangePassword(int $id) : void
    {
        $this->isAllowed('user', 'write');
        $this->eUser = $this->facadeUser->get($id);

        $this->template->title = new SimpleTranslation('user.change-password.title %s', [$this->eUser->getFullName()]);

        if (! $this->isAjax()) {
            return;
        }

        $this->payload->isModal = true;
        $this->redrawControl('snipModal');
    }

    public function formUserEditOnSubmit(Form $form, ?User $user, string $submitted) : Form
    {
        if ($form->hasErrors() || $user === null) {
            return $form;
        }

        if ($user->getId() === $this->getUser()->getId()
            && $this->getUser()->getIdentity() instanceof IIdentity
            && method_exists($this->getUser()->getIdentity(), 'updateAccount')) {
            $this->getUser()->getIdentity()->updateAccount(
                $user->getName(),
                $user->getSurname(),
                $user->getEmail(),
                $user->getPhone()
            );
        }

        if ($submitted === 'send_back' || $this->isAjax()) {
            $this->formOnBack();
        } else {
            $this->redirect('edit', ['id' => $user->getId()]);
        }

        return $form;
    }

    public function formOnBack() : void
    {
        $this->redirect('default');
    }

    public function formUserChangePasswordOnSubmit(Form $form, ?User $user, string $submitted) : Form
    {
        if ($form->hasErrors() || $user === null) {
            return $form;
        }

        $this->formOnBack();
        return $form;
    }

    public function actionOverview() : void
    {
    }

    public function renderOverview() : void
    {
        $this->template->title = 'user.overview.title';
        $this->template->users = $this->facadeUser->getActiveUsers();
    }

    protected function startup() : void
    {
        parent::startup();
    }

    protected function createComponentGridUser() : GridDoctrine
    {
        $control = new GridUser($this, $this->facadeUser, $this->facadeRole, $this->imageStorage, $this->mailService);
        return $control->create();
    }

    protected function createComponentFormUserEdit() : FormUserEdit
    {
        $control                   = $this->iFormUserEditFactory->create($this->eUser);
        $control->onBack[]         = [$this, 'formOnBack'];
        $control->onSubmit[]       = [$this, 'formUserEditOnSubmit'];
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        return $control;
    }

    protected function createComponentFormUserChangePassword() : FormUserChangePassword
    {
        $control                   = $this->iFormUserChangePasswordFactory->create($this->eUser);
        $control->onBack[]         = [$this, 'formOnBack'];
        $control->onSubmit[]       = [$this, 'formUserChangePasswordOnSubmit'];
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        return $control;
    }
}
