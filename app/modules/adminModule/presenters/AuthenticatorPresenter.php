<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\Form\FormRegistration;
use App\AdminModule\Components\Form\IFormRegistrationFactory;
use App\Components\Form\FormForgotPassword;
use App\Components\Form\FormLogIn;
use App\Components\Form\FormRecoveryPassword;
use App\Components\Form\IFormForgotPasswordFactory;
use App\Components\Form\IFormLogInFactory;
use App\Components\Form\IFormRecoveryPasswordFactory;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\Mail\Type\CMailRecoveryPassword;
use App\Model\Mail\Type\CMailRegistration;
use Skadmin\Mailing\Model\MailService;
use App\Model\System\Flash;
use DateTime;
use DateTimeInterface;
use Haltuf\Genderer\Genderer;
use Nette\Application\AbortException;
use Nette\Security\AuthenticationException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Translator\SimpleTranslation;
use SkadminUtils\FormControls\UI\Form;
use function strlen;

final class AuthenticatorPresenter extends CoreAdminPresenter
{
    /** @var MailService @inject */
    public $mailService;

    /** @var UserFacade @inject */
    public $facade;

    /** @var IFormLogInFactory @inject */
    public $iFormLogInFactory;

    /** @var IFormForgotPasswordFactory @inject */
    public $iFormForgotPasswordFactory;

    /** @var IFormRecoveryPasswordFactory @inject */
    public $iFormRecoveryPasswordFactory;

    /** @var IFormRegistrationFactory @inject */
    public $iFormRegistrationFactory;

    /** @var string @persistent */
    public $bl = null;

    /** @var User */
    private $eUser;

    public function actionDefault() : void
    {
        $this->initLoginLayout();
    }

    private function initLoginLayout() : void
    {
        if ($this->user->isLoggedIn()) {
            $this->redirectHomepage();
        }

        $this->setLayout('login');
    }

    private function redirectHomepage() : void
    {
        if ($this->bl !== null) {
            $this->restoreRequest($this->bl);
        }
        $this->redirect('Homepage:', ['bl' => null]);
    }

    public function actionForgotPassword() : void
    {
        $this->initLoginLayout();
    }

    public function actionRegistration() : void
    {
        $this->initLoginLayout();
    }

    public function actionLogOut() : void
    {
        $this->flashMessage('authenticator.flash.log-out', Flash::SUCCESS);
        $this->user->logout(true);
        $this->redirect('default');
    }

    public function renderDefault() : void
    {
        $this->template->title = 'authenticator.default.title';
    }

    public function renderForgotPassword() : void
    {
        $this->template->title = 'authenticator.forgot-password.title';
    }

    public function renderRegistration() : void
    {
        $this->template->title = 'authenticator.registration.title';
    }

    /**
     * @param ArrayHash<string> $values
     */
    public function formLogInOnSuccess(Form $form, ArrayHash $values) : Form
    {
        try {
            $this->user->logout(true);
            $this->user->login($values->username, $values->password);
            $this->user->setExpiration('1 hours');

            $this->flashMessage('authenticator.flash.log-in.success', Flash::SUCCESS);
            $this->redirectHomepage();
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
        }

        return $form;
    }

    public function formLogInOnForgotPassword() : void
    {
        $this->redirect('forgotPassword');
    }

    public function formLogInOnRegistration() : void
    {
        $this->redirect('registration');
    }

    public function formForgotPasswordOnLogIn() : void
    {
        $this->redirect('default');
    }

    public function formRegistrationOnLogIn() : void
    {
        $this->redirect('default');
    }

    /**
     * @param ArrayHash<string> $values
     *
     * @throws AbortException
     */
    public function formForgotPasswordOnSuccess(Form $form, ArrayHash $values) : Form
    {
        $user = $this->facade->findByUsername($values->username);

        if ($user === null) {
            $this->flashMessage('authenticator.flash.forgot-password.danger', Flash::DANGER);
            return $form;
        }

        $user = $this->facade->createForgotPasswordByUser($user);

        if ($user->getForgotPasswordDate() === null) {
            $message = new SimpleTranslation('authenticator.flash.forgot-password.danger %s', $user->getEmail());
            $this->flashMessage($message, Flash::DANGER);
        } else {
            $this->formForgotPasswordSendEmail($user);
        }

        $this->redirect('default');
    }

    private function formForgotPasswordSendEmail(User $user) : void
    {
        $href = $this->link('//recoveryPassword', [
            'id' => $user->getForgotPasswordHash(),
        ]);
        $link = Html::el('a', [
            'href'   => $href,
            'target' => '_blank',
        ])->setText($this->translator->translate('mail-recovery-password.link'));

        /** @var DateTimeInterface $validTo */
        $validTo               = $user->getForgotPasswordDate();
        $cMailRecoveryPassword = new CMailRecoveryPassword(
            (string) $user->getForgotPasswordHash(),
            $href,
            (string) $link,
            $validTo
        );

        $mailQueue = $this->mailService->addByTemplateType(
            CMailRecoveryPassword::TYPE,
            $cMailRecoveryPassword,
            [$user->getEmail()],
            true
        );

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('authenticator.flash.forgot-password.success %s', $user->getEmail());
            $this->flashMessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('authenticator.flash.forgot-password.danger %s', $user->getEmail());
            $this->flashMessage($message, Flash::DANGER);
        }
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function formRegistrationOnSuccess(Form $form, ArrayHash $values) : Form
    {
        $birthdate = null;
        if ($values->birthdate) {
            $birthdate = DateTime::createFromFormat('d.m.Y', $values->birthdate);
            $birthdate = $birthdate instanceof DateTimeInterface ? $birthdate : null;
        }

        if ($values->password !== $values->re_password) {
            $this->flashMessage('authenticator.flash.registration.danger.password-is-not-same', Flash::DANGER);
            return $form;
        }

        if (! $values->accept_the_terms) {
            $this->flashMessage('authenticator.flash.registration.danger.must_accept_the_terms', Flash::DANGER);
            return $form;
        }

        $user = $this->facade->create(
            $values->email,
            $values->password,
            $values->name,
            $values->surname,
            $values->phone,
            $values->email,
            $values->facebook,
            $values->address,
            $birthdate,
            '',
            '',
            false,
            [],
            true
        );

        if (! ($user instanceof User)) {
            $this->flashMessage('authenticator.flash.registration.danger.username-is-not-available', Flash::DANGER);
            return $form;
        }

        $this->formRegistrationSendEmail($user);
        $this->redirect('default');
    }

    private function formRegistrationSendEmail(User $user) : void
    {
        $href = $this->presenter->link('//Authenticator:default', ['bl' => null]);
        $link = Html::el('a', [
            'href'   => $href,
            'target' => '_blank',
        ])->setText($this->translator->translate('mail-user-authorize.link'));

        $genderer          = new Genderer();
        $cMailRegistration = new CMailRegistration($genderer->getVocative($user->getFullName()), $user->getEmail(), $href, (string) $link);

        $mailQueue = $this->mailService->addByTemplateType(
            CMailRegistration::TYPE,
            $cMailRegistration,
            [$user->getEmail()],
            true
        );

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('authenticator.flash.registration.success %s', $user->getEmail());
            $this->flashMessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('authenticator.flash.registration.danger %s', $user->getEmail());
            $this->flashMessage($message, Flash::DANGER);
        }
    }

    public function actionRecoveryPassword(?string $id = null) : void
    {
        $this->initLoginLayout();

        if ($id === null || strlen($id) !== 32) {
            $this->flashMessage('authenticator.flash.recovery-password.danger.wrong-length', Flash::DANGER);
            $this->redirect('default');
        }

        $eUser = $this->facade->findByForgotPasswordHash($id);
        if ($eUser === null) {
            $this->flashMessage('authenticator.flash.recovery-password.danger.invalid-hash', Flash::DANGER);
            $this->redirect('default');
        }
        $this->eUser = $eUser;

        /** @var DateTime $forgotPasswordDate */
        $forgotPasswordDate = $this->eUser->getForgotPasswordDate();
        if ($forgotPasswordDate >= new DateTime()) {
            return;
        }

        $this->flashMessage('authenticator.flash.recovery-password.danger.expiration-validity', Flash::DANGER);
        $this->redirect('forgotPassword');
    }

    public function renderRecoveryPassword() : void
    {
        $this->template->title = 'authenticator.recovery-password.title';
    }

    /**
     * @param ArrayHash<string> $values
     *
     * @throws AbortException
     */
    public function formRecoveryPasswordOnSuccess(Form $form, ArrayHash $values) : void
    {
        $this->facade->updatePasswordByUser($this->eUser, $values->password);
        $this->flashMessage('authenticator.flash.recovery-password.success', Flash::SUCCESS);
        $this->redirect('default');
    }

    protected function startup() : void
    {
        parent::startup();
    }

    protected function createComponentFormLogIn() : FormLogIn
    {
        $control                     = $this->iFormLogInFactory->create();
        $control->onSuccess[]        = [$this, 'formLogInOnSuccess'];
        $control->onFlashmessage[]   = [$this, 'onFlashMessage'];
        $control->onForgotPassword[] = [$this, 'formLogInOnForgotPassword'];
        $control->onRegistration[]   = [$this, 'formLogInOnRegistration'];
        return $control;
    }

    protected function createComponentFormForgotPassword() : FormForgotPassword
    {
        $control                   = $this->iFormForgotPasswordFactory->create();
        $control->onSuccess[]      = [$this, 'formForgotPasswordOnSuccess'];
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        $control->onLogIn[]        = [$this, 'formForgotPasswordOnLogIn'];
        return $control;
    }

    protected function createComponentFormRegistration() : FormRegistration
    {
        $control                   = $this->iFormRegistrationFactory->create();
        $control->onSuccess[]      = [$this, 'formRegistrationOnSuccess'];
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        $control->onLogIn[]        = [$this, 'formRegistrationOnLogIn'];
        return $control;
    }

    protected function createComponentFormRecoveryPassword() : FormRecoveryPassword
    {
        $control                   = $this->iFormRecoveryPasswordFactory->create();
        $control->onSuccess[]      = [$this, 'formRecoveryPasswordOnSuccess'];
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        return $control;
    }
}
