<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\Form\FormSetting;
use App\AdminModule\Components\Form\IFormSettingFactory;

final class SettingPresenter extends CoreAdminAuthorizedPresenter
{
    /** @var IFormSettingFactory @inject */
    public $iFormSettingFactory;

    public function actionDefault() : void
    {
    }

    public function renderDefault() : void
    {
        $this->template->title = 'setting.default.title';
    }

    protected function startup() : void
    {
        parent::startup();
        $this->isAllowed('setting', 'read');
    }


    protected function createComponentFormSetting() : FormSetting
    {
        $control                   = $this->iFormSettingFactory->create();
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        return $control;
    }
}
