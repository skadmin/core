<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\Doctrine\User\User;
use App\Model\System\Flash;
use Nette\Security\IUserStorage;
use function implode;

class CoreAdminAuthorizedPresenter extends CoreAdminPresenter
{
    /** @var User */
    protected $identity;

    protected function startup() : void
    {
        parent::startup();

        if ($this->user->isLoggedIn()) {
            /** @var User $identity */
            $identity                 = $this->getUser()->getIdentity();
            $this->identity           = $identity;
            $this->template->identity = $identity;
            return;
        }

        if ($this->user->getLogoutReason() === IUserStorage::INACTIVITY) {
            $this->flashMessage('core-admin-authorized.startup.user-inactivity', Flash::INFO);
        }

        $this->redirect('Authenticator:', [
            'bl' => $this->storeRequest(),
        ]);
    }

    protected function beforeRender() : void
    {
        parent::beforeRender();

        $userRoles = [];
        foreach ($this->identity->getRolesObject() as $role) {
            $userRoles[] = $role->getName();
        }
        $this->template->userRoles = implode(', ', $userRoles);
    }
}
