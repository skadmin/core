<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\System\ABaseControl;

final class ComponentFrontPresenter extends CoreComponentPresenter
{
    public function actionDefault(?ABaseControl $package = null, ?string $render = null, ?int $id = null) : void
    {
        $this->prepareControl($package, $render, $id, ABaseControl::MODULE_FRONT);
    }
}
