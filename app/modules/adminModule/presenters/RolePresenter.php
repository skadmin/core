<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\Form\FormPermissionsForRole;
use App\AdminModule\Components\Form\IFormPermissionsForRoleFactory;
use App\AdminModule\Components\Grid\GridRole;
use App\Components\Grid\GridDoctrine;
use App\Model\Doctrine\Role\Role;
use App\Model\Doctrine\Role\RoleFacade;
use Nette\Security\Permission;
use Skadmin\Translator\SimpleTranslation;
use SkadminUtils\FormControls\UI\Form;

final class RolePresenter extends CoreAdminAuthorizedPresenter
{
    /** @var RoleFacade @inject */
    public $facadeRole;

    /** @var IFormPermissionsForRoleFactory @inject */
    public $iFormPermissionsForRoleFactory;

    /** @var Role */
    private $role;

    public function actionDefault() : void
    {
    }

    public function renderDefault() : void
    {
        $this->template->title = 'role.default.title';
    }

    public function actionPermission(?int $id = null) : void
    {
        $this->isAllowed('role', 'write');
        $this->role = $this->facadeRole->get($id);
    }

    public function renderPermission() : void
    {
        $this->template->title = new SimpleTranslation('role.permission.title %s', [$this->role->getName()]);
    }

    public function formPermissionsForRoleOnSubmit(Form $form, ?Permission $permission, string $submitted) : void
    {
        if ($submitted === 'send_back') {
            $this->formPermissionsForRoleOnBack();
        }
        // component call redraw via ajax
    }

    public function formPermissionsForRoleOnBack() : void
    {
        $this->redirect('default');
    }

    protected function startup() : void
    {
        parent::startup();
        $this->isAllowed('role', 'read');
    }

    protected function createComponentGridRole() : GridDoctrine
    {
        $control = new GridRole($this, $this->facadeRole);
        return $control->create();
    }

    protected function createComponentFormPermissionsForRole() : FormPermissionsForRole
    {
        $control                   = $this->iFormPermissionsForRoleFactory->create($this->role);
        $control->onBack[]         = [$this, 'formPermissionsForRoleOnBack'];
        $control->onSubmit[]       = [$this, 'formPermissionsForRoleOnSubmit'];
        $control->onFlashmessage[] = [$this, 'onFlashMessage'];
        return $control;
    }
}
