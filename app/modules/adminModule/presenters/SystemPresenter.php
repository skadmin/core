<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use Skadmin\FileStorage\FileStorage;

final class SystemPresenter extends CoreAdminAuthorizedPresenter
{
    /** @var FileStorage @inject */
    public $fileSystem;

    public function actionDownloadI(string $identifier) : void
    {
        $this->fileSystem->download($identifier);
    }
}
