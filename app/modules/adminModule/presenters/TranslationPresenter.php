<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Components\Grid\GridDoctrine;
use App\Model\System\Flash;
use Skadmin\Translator\Components\Grid\GridTranslation;
use Skadmin\Translator\Components\Grid\GridTranslationWithoutTranslate;
use Skadmin\Translator\Doctrine\User\LanguageFacade;
use Skadmin\Translator\SimpleTranslation;

final class TranslationPresenter extends CoreAdminAuthorizedPresenter
{
    /** @var LanguageFacade @inject */
    public $facadeLanguage;

    public function actionDefault() : void
    {
    }

    public function renderDefault() : void
    {
        $this->template->title = 'translation.default.title';
    }

    public function actionWithoutTranslate() : void
    {
    }

    public function renderWithoutTranslate() : void
    {
        $this->template->title = 'translation.without-translate.title';
    }

    public function handleDeleteNoUse() : void
    {
        $countDeleted      = $this->facadeLanguage->deleteNoUseTranslation();
        $simpleTranslation = new SimpleTranslation('translation.flash.delete-no-use.success %d', [$countDeleted]);

        $this->flashMessage($simpleTranslation, Flash::SUCCESS);
        $this->redrawControl('snipGrid');
    }

    protected function startup() : void
    {
        parent::startup();
        $this->isAllowed('translation', 'read');
    }

    protected function createComponentGridTranslation() : GridDoctrine
    {
        $control = new GridTranslation($this, $this->facadeLanguage);
        return $control->create();
    }

    protected function createComponentGridTranslationWithoutTranslate() : GridDoctrine
    {
        $control = new GridTranslationWithoutTranslate($this, $this->facadeLanguage);
        return $control->create();
    }
}
