<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\Doctrine\Role\Privilege;
use App\Model\System\ABaseControl;

final class ComponentPresenter extends CoreComponentPresenter
{
    public function actionDefault(?ABaseControl $package = null, ?string $render = null, ?int $id = null) : void
    {
        $this->isAllowed($package instanceof ABaseControl ? $package->getResource() : null, Privilege::READ);
        $this->prepareControl($package, $render, $id);
    }
}
