<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

/**
 * Interface IFormRegistrationFactory
 */
interface IFormRegistrationFactory
{
    public function create() : FormRegistration;
}
