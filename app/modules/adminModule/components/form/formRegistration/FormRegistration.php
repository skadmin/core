<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Components\Form\FormControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use SkadminUtils\FormControls\UI\Form;

class FormRegistration extends FormControl
{
    /** @var callable[] */
    public $onLogIn;

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formRegistration.latte');
        $template->render();
    }

    public function processOnLogIn() : void
    {
        $this->onLogIn();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('name', 'form.registration.name')
            ->setRequired('form.registration.name.req')
            ->setHtmlAttribute('placeholder', 'form.registration.name.placeholder');
        $form->addText('surname', 'form.registration.surname')
            ->setRequired('form.registration.surname.req')
            ->setHtmlAttribute('placeholder', 'form.registration.surname.placeholder');
        $form->addEmail('email', 'form.registration.email')
            ->setRequired('form.registration.email.req')
            ->setHtmlAttribute('placeholder', 'form.registration.email.placeholder');
        $form->addText('phone', 'form.registration.phone')
            ->setRequired('form.registration.phone.req')
            ->setHtmlAttribute('placeholder', 'form.registration.phone.placeholder');

        $form->addText('facebook', 'form.registration.facebook')
            ->setHtmlAttribute('placeholder', 'form.registration.facebook.placeholder');
        $form->addText('address', 'form.registration.address')
            ->setRequired('form.registration.address.req')
            ->setHtmlAttribute('placeholder', 'form.registration.address.placeholder');
        $form->addText('birthdate', 'form.registration.birthdate')
            ->setHtmlAttribute('data-date')
            ->setHtmlAttribute('data-date-noicon')
            ->setHtmlAttribute('data-date-clear')
            ->setHtmlAttribute('data-date-drops', 'up')
            ->setRequired('form.registration.birthdate.req')
            ->setHtmlAttribute('placeholder', 'form.registration.birthdate.placeholder');

        // PASS
        $form->addPassword('password', 'form.registration.password')
            ->setRequired('form.registration.password.req')
            ->setHtmlAttribute('placeholder', 'form.registration.password.placeholder');
        $form->addPassword('re_password', 'form.registration.re-password')
            ->setRequired('form.registration.re-password.req')
            ->addRule(Form::EQUAL, 'form.registration.re-password.equal.password', $form['password'])
            ->setHtmlAttribute('placeholder', 'form.registration.re-password.placeholder');

        // ACCEPT THE TERMO
        $link = Html::el('a', [
            'href'   => 'https://www.google.com',
            'target' => '_blank',
        ])->setText('odkaz');

        $caption = new Html();
        $caption->setText($this->translator->translate('form.registration.accept-the-terms'))
            ->addText(' ')
            ->addHtml($link);

        $form->addCheckbox('accept_the_terms', $caption)
            ->setRequired('form.registration.accept-the-terms.req');

        // CAPTCHA
        $form->addInvisibleReCaptchaInput();

        // BTN
        $form->addSubmit('send', 'form.registration.send');
        $form->addSubmit('log_in', 'form.registration.log-in')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnLogIn'];

        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
