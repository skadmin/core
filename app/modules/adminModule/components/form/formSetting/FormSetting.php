<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Components\Form\FormControl;
use App\Model\Doctrine\Setting\SettingFacade;
use App\Model\System\Flash;
use Nette\Forms\Container;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use function intval;

class FormSetting extends FormControl
{
    /** @var SettingFacade */
    private $facadeSetting;

    /** @var LoggedUser */
    private $loggedUser;

    public function __construct(SettingFacade $facadeSetting, LoggedUser $loggedUser, Translator $translator)
    {
        parent::__construct($translator);
        $this->facadeSetting = $facadeSetting;
        $this->loggedUser    = $loggedUser;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);

        foreach ($values->section as $section) {
            foreach ($section as $settingId => $settingValue) {
                $this->facadeSetting->update(intval($settingId), (string) $settingValue);
            }
        }

        $this->onFlashmessage('form.setting.flash.success', Flash::SUCCESS);
        $this->onSubmit($form, null, $form->isSubmitted()->name);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formSetting.latte');
        $template->sections = $this->facadeSetting->getAllSections();

        $template->enableEdit = $this->loggedUser->isAllowed('setting', 'write');
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $referencForm = [];
        $formSection  = $form->addContainer('section');
        foreach ($this->facadeSetting->getAll() as $setting) {

            /** @var int $sectionId */
            $sectionId = $setting->getSection()->getId();
            if (! isset($referencForm[$sectionId])) {
                $referencForm[$sectionId] = $formSection->addContainer($sectionId);
            }

            /** @var Container $_container */
            $_container = &$referencForm[$sectionId];
            $name       = new Html();
            $name->setText($setting->getName())
                ->addText(' ');

            $title   = new SimpleTranslation('code: %s', [$setting->getCode()]);
            $popover = Html::el('sup', [
                'class'          => 'fas fa-question-circle text-primary',
                'title'          => $this->translator->translate($title),
                'data-content'   => $this->translator->translate($setting->getDescription()),
                'data-trigger'   => 'hover',
                'data-toggle'    => 'popover',
                'data-placement' => 'right',
                'data-html'      => 'true',
            ]);
            $name->addHtml($popover);

            $_container->addText((string) $setting->getId(), $name)
                ->setDefaultValue($setting->getValue())
                ->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.setting.send');

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        return [];
    }
}
