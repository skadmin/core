<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

/**
 * Interface IFormSignFactory
 */
interface IFormSettingFactory
{
    public function create() : FormSetting;
}
