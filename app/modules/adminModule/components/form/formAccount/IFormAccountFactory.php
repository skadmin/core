<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Model\Doctrine\User\User;

/**
 * Interface IFormSignFactory
 */
interface IFormAccountFactory
{
    public function create(User $user) : FormAccount;
}
