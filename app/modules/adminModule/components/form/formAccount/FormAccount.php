<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Components\Form\FormControl;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\System\Flash;
use Contributte\ImageStorage\ImageStorage;
use DateTimeInterface;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use function in_array;
use function intval;

class FormAccount extends FormControl
{
    /** @var IFormUserChangePasswordFactory */
    private $iFormUserChangePasswordFactory;

    /** @var UserFacade */
    private $facade;

    /** @var User */
    private $user;

    /** @var LoggedUser */
    private $loggedUser;

    /** @var bool */
    private $drawForm = false;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(User $user, UserFacade $facade, Translator $translator, ImageStorage $imageStorage, LoggedUser $loggedUser, IFormUserChangePasswordFactory $iFormUserChangePasswordFactory)
    {
        parent::__construct($translator);
        $this->user   = $user;
        $this->facade = $facade;

        $this->imageStorage                   = $imageStorage;
        $this->loggedUser                     = $loggedUser;
        $this->iFormUserChangePasswordFactory = $iFormUserChangePasswordFactory;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        if (in_array($form->isSubmitted()->name, ['back'], true)) {
            return;
        }

        $this->onSuccess($form, $values);

        $birthdate = null;
        if ($values->birthdate) {
            $birthdate = DateTime::createFromFormat('d.m.Y', $values->birthdate);
            $birthdate = $birthdate instanceof DateTimeInterface ? $birthdate : null;
        }

        $identifier = null;

        /** @var FileUpload $imagePreview */
        $imagePreview = $values->image_preview;
        if ($imagePreview->isOk() && $imagePreview->isImage()) {
            $identifier = $this->imageStorage->saveUpload($imagePreview, User::DIR_IMAGE)->identifier;
        }

        if ($identifier !== null && $this->user->getImagePreview() !== null) {
            $this->imageStorage->delete($this->user->getImagePreview());
        }

        $this->user = $this->facade->updateAccount(
            intval($this->user->getId()),
            $values->name,
            $values->surname,
            $values->phone,
            $values->email,
            $values->facebook,
            $values->address,
            $birthdate,
            $values->note,
            $values->experience,
            $identifier
        );

        $this->onFlashmessage('form.account.flash.success', Flash::SUCCESS);
        $this->onSubmit($form, $this->user, $form->isSubmitted()->name);
        $this->redrawControl('snipForm');
    }

    public function onFlashMessageDelegate(string $message, string $type) : void
    {
        $this->onFlashmessage($message, $type);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formAccount.latte');

        $template->user         = $this->user;
        $template->drawForm     = $this->drawForm;
        $template->allowWrite   = $this->loggedUser->isAllowed('account', 'write');
        $template->imageStorage = $this->imageStorage;

        $template->render();
    }

    public function handleEdit() : void
    {
        if (! $this->loggedUser->isAllowed('account', 'write')) {
            return;
        }

        $this->drawForm = true;
        $this->redrawControl('snipForm');
    }

    public function handleChangePassword() : void
    {
        if (! $this->loggedUser->isAllowed('account', 'write')) {
            return;
        }

        $this->drawForm = true;
        $this->redrawControl('snipFormChangePassword');
    }

    public function processOnBack() : void
    {
        $this->drawForm = false;
        $this->redrawControl('snipForm');
    }

    public function formUserChangePasswordOnBack() : void
    {
        $this->drawForm = false;
        $this->redrawControl('snipFormChangePassword');
    }

    public function formUserChangePasswordOnError(Form $form) : void
    {
        $this->drawForm = true;
        $this->redrawControl('snipFormChangePassword');
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.account.name')
            ->setRequired('form.account.name.req');
        $form->addText('surname', 'form.account.surname')
            ->setRequired('form.account.surname.req');
        $form->addEmail('email', 'form.account.email')
            ->setRequired('form.account.email.req');
        $form->addText('phone', 'form.account.phone');

        // IMAGE
        $form->addUpload('image_preview', 'form.account.image-preview')
            ->addRule(Form::IMAGE, 'form.account.image-preview.rule-image');

        $form->addText('facebook', 'form.account.facebook');
        $form->addText('address', 'form.user-edit.address');
        $form->addText('birthdate', 'form.user-edit.birthdate')
            ->setHtmlAttribute('data-date');

        $form->addTextArea('note', 'form.user-edit.note');
        $form->addTextArea('experience', 'form.user-edit.experience');

        // BUTTON
        $form->addSubmit('send', 'form.account.send');
        $form->addSubmit('back', 'form.account.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        return [
            'name'       => $this->user->getName(),
            'surname'    => $this->user->getSurname(),
            'email'      => $this->user->getEmail(),
            'phone'      => $this->user->getPhone(),
            'facebook'   => $this->user->getFacebook(),
            'address'    => $this->user->getAddress(),
            'birthdate'  => $this->user->getBirthdate('d.m.Y'),
            'note'       => $this->user->getNote(),
            'experience' => $this->user->getExperience(),
        ];
    }

    protected function createComponentFormUserChangePassword() : FormUserChangePassword
    {
        $this->loggedUser->isAllowed('account', 'write');
        $control                   = $this->iFormUserChangePasswordFactory->create($this->user);
        $control->onFlashmessage[] = [$this, 'onFlashMessageDelegate'];
        $control->onBack[]         = [$this, 'formUserChangePasswordOnBack'];
        $control->onSubmit[]       = [$this, 'formUserChangePasswordOnBack'];
        $control->onError[]        = [$this, 'formUserChangePasswordOnError'];
        return $control;
    }
}
