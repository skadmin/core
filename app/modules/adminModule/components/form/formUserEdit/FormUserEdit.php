<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Components\Form\FormControl;
use App\Model\Doctrine\Role\RoleFacade;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\System\Flash;
use DateTimeInterface;
use Nette\Application\UI\Presenter;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use function boolval;

class FormUserEdit extends FormControl
{
    /** @var UserFacade */
    private $facadeUser;

    /** @var RoleFacade */
    private $facadeRole;

    /** @var User */
    private $user;

    /** @var LoggedUser */
    private $loggedUser;

    public function __construct(User $user, UserFacade $facadeUser, RoleFacade $facadeRole, LoggedUser $loggedUser, Translator $translator)
    {
        parent::__construct($translator);

        $this->user       = $user;
        $this->facadeUser = $facadeUser;
        $this->facadeRole = $facadeRole;
        $this->loggedUser = $loggedUser;
    }

    public function processOnBack() : void
    {
        $this->onBack();
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);

        $roles = [];
        foreach ($values->roles as $roleId) {
            $roles[] = $this->facadeRole->get($roleId);
        }

        $birthdate = null;
        if ($values->birthdate) {
            $birthdate = DateTime::createFromFormat('d.m.Y', $values->birthdate);
            $birthdate = $birthdate instanceof DateTimeInterface ? $birthdate : null;
        }

        if ($this->user->getId() !== null) {
            $user = $this->facadeUser->update(
                $this->user->getId(),
                $values->name,
                $values->surname,
                $values->phone,
                $values->email,
                $values->facebook,
                $values->address,
                $birthdate,
                $values->note,
                $values->experience,
                boolval($values->is_active),
                $roles,
                $this->loggedUser
            );
        } else {
            if ($values->password !== $values->re_password) {
                $form->addError('form.user-edit.flash.not-equal-password');
                $user = null;
            } else {
                $user = $this->facadeUser->create(
                    $values->username,
                    $values->password,
                    $values->name,
                    $values->surname,
                    $values->phone,
                    $values->email,
                    $values->facebook,
                    $values->address,
                    $birthdate,
                    $values->note,
                    $values->experience,
                    boolval($values->is_active),
                    $roles
                );

                if ($user === null) {
                    $form->addError('form.user-edit.flash.username-not-unique');
                }
            }
        }

        if (! $form->hasErrors()) {
            $this->onFlashmessage('form.user-edit.flash.success', Flash::SUCCESS);
        }

        $this->onSubmit($form, $user, $form->isSubmitted()->name);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formUserEdit.latte');

        if ($this->hasPresenter()) {
            /** @var Presenter $presenter */
            $presenter        = $this->getPresenter();
            $template->isAjax = $presenter->isAjax();
        } else {
            $template->isAjax = false;
        }

        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $roles = [];
        foreach ($this->facadeRole->getAllRoles() as $role) {
            if ($role->isMasterAdmin()) {
                if ($this->loggedUser->isInRole($role->getWebalize())) {
                    $roles[$role->getId()] = $role->getName();
                }
            } else {
                $roles[$role->getId()] = $role->getName();
            }
        }

        // INPUT
        // NEW USER?
        if (! $this->user->isLoaded()) {
            $form->addEmail('username', 'form.user-edit.username')
                ->setRequired('form.user-edit.username.req');
            $form->addPassword('password', 'form.user-edit.password')
                ->setRequired('form.user-edit.password.req');
            $form->addPassword('re_password', 'form.user-edit.re-password')
                ->setRequired('form.user-edit.re-password.req')
                ->addRule(Form::EQUAL, 'form.user-edit.re-password.equal.password', $form['password']);
        }

        $form->addText('surname', 'form.user-edit.surname')
            ->setRequired('form.user-edit.surname.req');
        $form->addText('name', 'form.user-edit.name')
            ->setRequired('form.user-edit.name.req');
        $form->addEmail('email', 'form.user-edit.email')
            ->setRequired('form.user-edit.email.req');
        $form->addText('phone', 'form.user-edit.phone');
        $form->addCheckbox('is_active', 'form.user-edit.is-active');
        $form->addMultiSelect('roles', 'form.user-edit.roles', $roles)
            ->setTranslator(null);

        $form->addText('facebook', 'form.user-edit.facebook');
        $form->addText('address', 'form.user-edit.address');
        $form->addText('birthdate', 'form.user-edit.birthdate')
            ->setHtmlAttribute('data-date');

        $form->addTextArea('note', 'form.user-edit.note');
        $form->addTextArea('experience', 'form.user-edit.experience');

        // BUTTON
        $form->addSubmit('send', 'form.user-edit.send');
        $form->addSubmit('send_back', 'form.user-edit.send-back');
        $form->addSubmit('back', 'form.user-edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $defaults = $this->getDefaults();
        $_roles   = [];
        foreach ($defaults['roles'] as $roleId) {
            if (! isset($roles[$roleId])) {
                continue;
            }
            $_roles[] = $roleId;
        }
        $defaults['roles'] = $_roles;
        $form->setDefaults($defaults);

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        $roles = [];
        foreach ($this->user->getRolesObject() as $role) {
            $roles[] = $role->getId();
        }

        return [
            'name'       => $this->user->getName(),
            'surname'    => $this->user->getSurname(),
            'email'      => $this->user->getEmail(),
            'phone'      => $this->user->getPhone(),
            'is_active'  => $this->user->isActive(),
            'facebook'   => $this->user->getFacebook(),
            'address'    => $this->user->getAddress(),
            'birthdate'  => $this->user->getBirthdate('d.m.Y'),
            'note'       => $this->user->getNote(),
            'experience' => $this->user->getExperience(),
            'roles'      => $roles,
        ];
    }
}
