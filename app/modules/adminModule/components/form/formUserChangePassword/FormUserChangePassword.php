<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Components\Form\FormControl;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\System\Flash;
use Exception;
use Nette\Application\UI\Presenter;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Security\Passwords;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use function in_array;
use function intval;

class FormUserChangePassword extends FormControl
{
    /** @var UserFacade */
    private $facadeUser;

    /** @var User */
    private $user;

    /** @var LoggedUser */
    private $loggedUser;

    public function __construct(User $user, UserFacade $facadeUser, LoggedUser $loggedUser, Translator $translator)
    {
        parent::__construct($translator);

        $this->user       = $user;
        $this->facadeUser = $facadeUser;
        $this->loggedUser = $loggedUser;
    }

    public function processOnBack() : void
    {
        $this->onBack();
    }

    public function processOnError(Form $form) : void
    {
        $this->onError($form);
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        if (in_array($form->isSubmitted()->name, ['back'], true)) {
            return;
        }

        $this->onSuccess($form, $values);

        if ($values->password !== $values->re_password) {
            $form->addError('form.user-change-password.flash.not-equal-password');
            $user = null;
        } else {
            $user = $this->facadeUser->updatePassword(intval($this->user->getId()), $values->password);
        }

        if (! $form->hasErrors()) {
            $this->onFlashmessage('form.user-change-password.flash.success', Flash::SUCCESS);
        }

        $this->onSubmit($form, $user, $form->isSubmitted()->name);
    }

    public function render() : void
    {
        $template = $this->initRender();
        $template->setFile(__DIR__ . '/formUserChangePassword.latte');
        $template->render();
    }

    /**
     * @throws Exception
     */
    private function initRender() : Template
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);

        if ($this->hasPresenter()) {
            /** @var Presenter $presenter */
            $presenter        = $this->getPresenter();
            $template->isAjax = $presenter->isAjax();
        } else {
            $template->isAjax = false;
        }
        return $template;
    }

    public function renderMaxWidth() : void
    {
        $template = $this->initRender();
        $template->setFile(__DIR__ . '/formUserChangePasswordMaxWidth.latte');
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // VALIDACE
        /** @var User $user */
        $user                    = $this->loggedUser->getIdentity();
        $validateCurrentPassword = static function ($field, $args) use ($user) : bool {
            $password = new Passwords();
            return $password->verify($field->getValue(), $user->getUserPassword());
        };

        // INPUT
        $form->addPassword('current_password', 'form.user-change-password.current-password')
            ->setRequired('form.user-change-password.current-password.req')
            ->addRule($validateCurrentPassword, 'form.user-change-password.current-password.equal.password');
        $form->addPassword('password', 'form.user-change-password.password')
            ->setRequired('form.user-change-password.password.req');
        $form->addPassword('re_password', 'form.user-change-password.re-password')
            ->setRequired('form.user-change-password.re-password.req')
            ->addRule(Form::EQUAL, 'form.user-change-password.re-password.equal.password', $form['password']);

        // BUTTON
        $form->addSubmit('send', 'form.user-change-password.send');
        $form->addSubmit('back', 'form.user-change-password.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];
        $form->onError[]   = [$this, 'processOnError'];

        return $form;
    }
}
