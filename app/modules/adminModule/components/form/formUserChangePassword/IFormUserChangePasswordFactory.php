<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Model\Doctrine\User\User;

/**
 * Interface IFormSignFactory
 */
interface IFormUserChangePasswordFactory
{
    public function create(User $user) : FormUserChangePassword;
}
