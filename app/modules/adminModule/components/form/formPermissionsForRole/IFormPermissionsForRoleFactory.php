<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Model\Doctrine\Role\Role;

/**
 * Interface IFormSignFactory
 */
interface IFormPermissionsForRoleFactory
{
    public function create(Role $role) : FormPermissionsForRole;
}
