<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Form;

use App\Components\Form\FormControl;
use App\Model\Doctrine\Role\Privilege;
use App\Model\Doctrine\Role\Role;
use App\Model\Doctrine\Role\RoleFacade;
use App\Model\System\Flash;
use Nette\Utils\ArrayHash;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use function array_combine;
use function array_search;
use function in_array;
use function sprintf;

class FormPermissionsForRole extends FormControl
{
    /** @var RoleFacade */
    private $facadeRole;

    /** @var Role */
    private $role;

    public function __construct(Role $role, RoleFacade $facadeRole, Translator $translator)
    {
        parent::__construct($translator);

        $this->role       = $role;
        $this->facadeRole = $facadeRole;
    }

    public function processOnBack() : void
    {
        $this->onBack();
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $this->onSuccess($form, $values);

        foreach ($values->privilege as $resource => $privilege) {
            $read   = in_array(Privilege::READ, $privilege, true);
            $write  = in_array(Privilege::WRITE, $privilege, true);
            $delete = in_array(Privilege::DELETE, $privilege, true);

            // remove default privilege
            foreach (Privilege::PRIVILEGES as $val) {
                $key = array_search($val, $privilege, true);
                if ($key === false) {
                    continue;
                }

                unset($privilege[$key]);
            }

            $this->facadeRole->createPrivilege(
                $this->role,
                $resource,
                $read,
                $write,
                $delete,
                $privilege
            );
        }

        $this->onFlashmessage('form.permissions-for-role.flash.success', Flash::SUCCESS);
        $this->onSubmit($form, null, $form->isSubmitted()->name);
        $this->redrawControl('snipForm');
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/formPermissionsForRole.latte');
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataPrivilege = array_combine(Privilege::PRIVILEGES, Privilege::PRIVILEGES);

        // INPUT
        $formPrivilege = $form->addContainer('privilege');
        foreach ($this->facadeRole->getAllResources() as $resource) {
            $addPrivileges = [];
            foreach ($resource->getAdditionalPrivilege() as $addPrivilege) {
                $addPrivileges[$addPrivilege] = sprintf('privilege.%s', $addPrivilege);
            }
            $formPrivilege->addCheckboxList((string) $resource->getId(), $resource->getTitle(), $dataPrivilege + $addPrivileges)
                ->setHtmlAttribute('data-description', $resource->getDescription());
        }

        // BUTTON
        $form->addSubmit('send', 'form.permissions-for-role.send');
        $form->addSubmit('send_back', 'form.permissions-for-role.send-back');
        $form->addSubmit('back', 'form.permissions-for-role.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        $defaults = [
            'privilege' => [],
        ];

        foreach ($this->role->getPrivileges() as $privilege) {
            $defaults['privilege'][$privilege->getResource()->getId()] = $privilege->getAllowPrivilege();
        }

        return $defaults;
    }
}
