<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Grid;

use App\Components\Grid\GridDoctrine;
use App\CoreModule\Presenters\CorePresenter;
use App\Model\Doctrine\Role\Privilege;
use App\Model\Doctrine\Role\Role;
use App\Model\Doctrine\Role\RoleFacade;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\Forms\Container;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Translator\SimpleTranslation;
use function boolval;
use function count;
use function implode;
use function intval;

class GridRole extends GridDoctrine
{
    /** @var RoleFacade */
    private $facade;


    public function __construct(CorePresenter $presenter, RoleFacade $facade)
    {
        parent::__construct($presenter);
        $this->facade = $facade;
    }

    public function create() : GridDoctrine
    {
        // DEFAULT
        $this->setPrimaryKey('id');
        $dataSource = $this->facade->getModel();
        $this->setDataSource($dataSource);

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $this->addColumnText('name', 'grid.roles.name');
        $this->addColumnText('isActive', 'grid.roles.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);
        $this->addColumnText('permission', 'grid.roles.permission')
            ->setRenderer(static function (Role $role) use ($translator) : Html {
                if ($role->isMasterAdmin()) {
                    return Html::el('span', ['class' => 'badge badge-primary mb-1 mt-1'])->setText($translator->translate('resource.all'));
                }

                $htmlPrivileges = new Html();
                foreach ($role->getPrivileges() as $privilege) {
                    $privileges = $privilege->getAllowPrivilege(true);

                    if (count($privileges) <= 0) {
                        continue;
                    }

                    $htmlPrivilege = Html::el('span', ['class' => 'badge badge-primary mr-2 mb-1 mt-1'])
                        ->addText($translator->translate($privilege->getResource()->getTitle()));

                    $countAdditionalPrivilege = count($privilege->getResource()->getAdditionalPrivilege());
                    if (count($privileges) !== ($countAdditionalPrivilege + count(Privilege::PRIVILEGES))) {
                        $_privileges = Arrays::map($privileges, static function ($privilege) use ($translator) : string {
                            return $translator->translate($privilege);
                        });
                        $htmlPrivilege->addText(': ')
                            ->addText(implode(', ', $_privileges));
                    }

                    $htmlPrivileges->addHtml($htmlPrivilege);
                }
                return $htmlPrivileges;
            });

        // FILTER
        $this->addFilterText('name', 'grid.users.name');
        $this->addFilterSelect('isActive', 'grid.users.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();

        // IF ROLE ALLOWED WRITE
        if ($this->presenter->getUser()->isAllowed('role', 'write')) {
            // INLINE ADD
            $this->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.roles.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $this->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $this->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $this->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $this->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];

            // ACTION
            $this->addAction('permission', 'grid.roles.action.permission')
                ->setIcon('pencil-ruler')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // ALLOW
        $this->allowRowsAction('permission', static function (Role $role) : bool {
            return ! $role->isMasterAdmin();
        });
        $this->allowRowsInlineEdit(static function (Role $role) : bool {
            return ! $role->isMasterAdmin();
        });

        return $this;
    }

    public function onInlineAdd(Container $container) : void
    {
        $container->addText('name', 'grid.roles.name')
            ->setRequired('grid.roles.name.req');
        $container->addSelect('isActive', 'grid.roles.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values) : void
    {
        $this->facade->createRole($values->name, boolval($values->isActive));

        $message = new SimpleTranslation('grid.roles.action.flash.inline-add.success "%s"', [$values->name]);
        $this->presenter->flashMessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container) : void
    {
        $container->addText('name', 'grid.roles.name')
            ->setRequired('grid.roles.name.req');
        $container->addSelect('isActive', 'grid.roles.is-active', Constant::DIAL_YES_NO);
    }

    public function onInlineEditDefaults(Container $container, Role $role) : void
    {
        $container->setDefaults([
            'name'     => $role->getName(),
            'isActive' => intval($role->isActive()),
        ]);
    }

    /**
     * @param int|string $id
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit($id, ArrayHash $values) : void
    {
        $this->facade->updateRole(intval($id), $values->name, boolval($values->isActive));
        $message = new SimpleTranslation('grid.roles.action.flash.inline-edit.success "%s"', [$values->name]);
        $this->presenter->flashMessage($message, Flash::SUCCESS);
    }
}
