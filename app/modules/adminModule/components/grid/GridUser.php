<?php

declare(strict_types=1);

namespace App\AdminModule\Components\Grid;

use App\Components\Grid\GridDoctrine;
use App\CoreModule\Presenters\CorePresenter;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use App\Model\Doctrine\Role\RoleFacade;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\Mail\Type\CMailUserAuthorize;
use Skadmin\Mailing\Model\MailService;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use Contributte\ImageStorage\ImageStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Haltuf\Genderer\Genderer;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Translator\SimpleTranslation;
use function implode;
use function intval;
use function sprintf;

class GridUser extends GridDoctrine
{
    /** @var UserFacade */
    private $facade;

    /** @var RoleFacade */
    private $facadeRole;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var MailService */
    private $mailService;

    public function __construct(CorePresenter $presenter, UserFacade $facade, RoleFacade $facadeRole, ImageStorage $imageStorage, MailService $mailService)
    {
        parent::__construct($presenter);
        $this->facade       = $facade;
        $this->facadeRole   = $facadeRole;
        $this->imageStorage = $imageStorage;
        $this->mailService  = $mailService;
    }

    public function create() : GridDoctrine
    {
        // DEFAULT
        $this->setPrimaryKey('id');
        $dataSource = $this->facade->getModelForGrid();
        $this->setDataSource($dataSource);

        // DATA
        $translator = $this->translator;

        $roles = [];
        foreach ($this->facadeRole->getAllRoles() as $role) {
            $roles[$role->getId()] = $role->getName();
        }

        $dialYesNo = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $this->addColumnText('imagePreview', '')
            ->setRenderer(function (User $user) : ?Html {
                if ($user->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$user->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/storage/images/%s', $imageSrc),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $this->addColumnText('userName', 'grid.users.username');
        $this->addColumnText('fullName', 'grid.users.name');
        $this->addColumnText('contact', 'grid.users.contact')
            ->setRenderer(static function (User $user) : Html {
                $contact = new Html();

                $contacts = [];
                if ($user->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($user->getEmail());
                }

                if ($user->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($user->getPhone());
                }

                if ($user->getFacebook() !== '') {
                    $contacts[] = Utils::createFacebookContact($user->getFacebook(), $user->getFullName());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        $this->addColumnText('isActive', 'grid.users.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);
        $this->addColumnText('roles', 'grid.users.roles')
            ->setRenderer(static function (User $user) : Html {
                $htmlRoles = new Html();

                foreach ($user->getRolesObject() as $role) {
                    $htmlRole = Html::el('span', ['class' => 'badge badge-primary mr-2 mb-1 mt-1'])
                        ->setText($role->getName());

                    $htmlRoles->addHtml($htmlRole);
                }
                return $htmlRoles;
            });

        // FILTER
        $this->addFilterText('userName', 'grid.users.username');
        $this->addFilterText('fullName', 'grid.users.name', ['name', 'surname']);
        $this->addFilterText('contact', 'grid.users.contact', ['email', 'phone', 'facebook']);
        $this->addFilterSelect('roles', 'grid.users.roles', Constant::PROMTP_ARR + $roles, 'r.id');
        $this->addFilterSelect('isActive', 'grid.users.is-active', Constant::PROMTP_ARR + $dialYesNo)
            ->setCondition(static function (QueryBuilder $qb, $value) : void {
                $criteria = Criteria::create();
                $criteria->where(Criteria::expr()->eq('isActive', $value));
                $criteria->orWhere(Criteria::expr()->eq('needsApproval', ($value + 1) % 2));

                $qb->addCriteria($criteria);
            });

        // IF USER ALLOWED WRITE
        if ($this->presenter->getUser()->isAllowed('user', 'write')) {
            // ACTION
            $this->addActionCallback('authorize', 'grid.users.action.authorize', [$this, 'gridUserOnAuthorize'])
                ->setClass('btn btn-xs btn-outline-primary ajax');

            $this->addAction('edit', 'grid.users.action.edit')
                ->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary ajax');

            $this->addAction('changePassword', 'grid.users.action.change-password')
                ->setIcon('unlock-alt')
                ->setClass('btn btn-xs btn-primary ajax');

            // TOOLBAR
            $this->addToolbarButton('new', 'grid.users.action.new')
                ->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary ajax');
        }

        // ALLOW
        $this->allowRowsAction('edit', static function (User $user) : bool {
            return ! $user->isLocked();
        });
        $this->allowRowsAction('changePassword', static function (User $user) : bool {
            return ! $user->isLocked();
        });
        $this->allowRowsAction('authorize', static function (User $user) : bool {
            return $user->isNeedsApproval();
        });

        // DETAIL
        $this->setItemsDetail(__DIR__ . '/templates/detailGridUser.latte');

        return $this;
    }

    public function gridUserOnAuthorize(string $userId) : void
    {
        $user = $this->facade->get(intval($userId));
        $this->facade->authorize($user);

        $this->formUserAuthorizeSendEmail($user);

        $this->redrawItem($userId);
    }


    private function formUserAuthorizeSendEmail(User $user) : void
    {
        $href = $this->presenter->link('//Authenticator:default', ['bl' => null]);
        $link = Html::el('a', [
            'href'   => $href,
            'target' => '_blank',
        ])->setText($this->translator->translate('mail-user-authorize.link'));

        $genderer       = new Genderer();
        $cMailAuthorize = new CMailUserAuthorize($genderer->getVocative($user->getFullName()), $user->getEmail(), $href, (string) $link);

        $mailQueue = $this->mailService->addByTemplateType(
            CMailUserAuthorize::TYPE,
            $cMailAuthorize,
            [$user->getEmail()],
            true
        );

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('grid.users.flash.authorize.success %s', $user->getEmail());
            $this->presenter->flashMessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('grid.users.flash.authorize.danger %s', $user->getEmail());
            $this->presenter->flashMessage($message, Flash::DANGER);
        }
    }
}
