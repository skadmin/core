<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\Doctrine\Package\Package;
use App\Model\Doctrine\Package\PackageFacade;
use App\Model\System\ABaseControl;
use App\Router\ARouterFactory;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use function get_class;

final class RouterFactory extends ARouterFactory
{
    /** @var PackageFacade */
    private static $facadePackage;

    public function __construct(PackageFacade $facadePackage)
    {
        self::$facadePackage = $facadePackage;
    }

    public function getSequence() : int
    {
        return 22;
    }

    public function createRouter(RouteList &$routerList) : void
    {
        // admin
        $router   = new RouteList('Admin');
        $router[] = new Route('admin/<package>/<render>[/<id>]', [
            'presenter' => 'Component',
            'action'    => 'default',
            'package'   => [
                Route::FILTER_IN  => static function ($package) : ?ABaseControl {
                    $package = self::$facadePackage->getByWebalize($package);
                    return $package instanceof Package ? $package->getBaseControl() : null;
                },
                Route::FILTER_OUT => static function ($package) : ?string {
                    $package = self::$facadePackage->getByBaseControl(get_class($package));

                    return $package instanceof Package ? $package->getWebalize() : null;
                },
            ],
        ]);

        $router[] = new Route('admin/f-<package>/<render>[/<id>]', [
            'presenter' => 'ComponentFront',
            'action'    => 'default',
            'package'   => [
                Route::FILTER_IN  => static function ($package) : ?ABaseControl {
                    $package = self::$facadePackage->getByWebalize($package);
                    return $package instanceof Package ? $package->getBaseControl() : null;
                },
                Route::FILTER_OUT => static function ($package) : ?string {
                    $package = self::$facadePackage->getByBaseControl(get_class($package));
                    return $package instanceof Package ? $package->getWebalize() : null;
                },
            ],
        ]);
        $router[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');

        $routerList[] = $router;
    }
}
