<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

final class HomepagePresenter extends CoreFrontPresenter
{
    public function actionDefault() : void
    {
        $this->setLayout('hp');
    }

    public function renderDefault() : void
    {
    }
}
