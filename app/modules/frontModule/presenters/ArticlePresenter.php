<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use Exception;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use Skadmin\Translator\SimpleTranslation;

final class ArticlePresenter extends CoreFrontPresenter
{
    /** @var ArticleFacade @inject */
    public $facadeArticle;

    /** @var Article */
    private $article;

    public function actionDefault(string $article) : void
    {
        //if (! $article instanceof Article) { problém s isCurrentLink
        $article = $this->facadeArticle->findByWebalize($article);
        //}

        if (! ($article instanceof Article)) {
            throw new Exception('Article not set');
        }

        $this->article = $article;
        $this->setSeo($article);
    }

    public function renderDefault() : void
    {
        $this->template->title   = new SimpleTranslation('article.detail.title - %s', $this->article->getName());
        $this->template->article = $this->article;
    }
}
