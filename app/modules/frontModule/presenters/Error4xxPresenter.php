<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\Request;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use TypeError;
use const DIRECTORY_SEPARATOR;
use function implode;
use function is_file;
use function sprintf;

final class Error4xxPresenter extends CoreFrontPresenter
{
    /** @var ArticleFacade @inject */
    public $facadeArticle;

    /**
     * @throws BadRequestException
     */
    public function startup() : void
    {
        parent::startup();
        if ($this->getRequest() instanceof Request && $this->getRequest()->isMethod(Request::FORWARD)) {
            return;
        }

        $this->error();
    }

    /**
     * @param BadRequestException|TypeError $exception
     */
    public function renderDefault($exception) : void
    {
        $file = implode(DIRECTORY_SEPARATOR, [__DIR__, '..', 'templates', 'Error4xx', $exception->getCode() . '.latte']);

        if (is_file($file)) {
            $this->setView((string) $exception->getCode());
            $webalize = sprintf('error-%d', $exception->getCode());
        } else {
            $this->setView('4xx');
            $webalize = 'error-4xx';
        }

        $error = $this->facadeArticle->findByWebalize($webalize);

        if (! $error instanceof Article) {
            return;
        }

        $this->template->title = $error->getName();
        $this->template->error = $error;
    }
}
