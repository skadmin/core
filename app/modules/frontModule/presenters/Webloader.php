<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

trait Webloader
{
    protected function createComponentCss() : CssLoader
    {
        return $this->webLoader->createCssLoader('front');
    }

    protected function createComponentJsCookies() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('frontCookies');
    }

    protected function createComponentJsHead() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('frontHead');
    }

    protected function createComponentJsFoot() : JavaScriptLoader
    {
        return $this->webLoader->createJavaScriptLoader('frontFoot');
    }
}
