<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\Router\ARouterFactory;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;

final class RouterFactory extends ARouterFactory
{
    /** @var ArticleFacade */
    private static $facadeArticle;

    public function __construct(ArticleFacade $facadeArticle)
    {
        self::$facadeArticle = $facadeArticle;
    }


    public function createRouter(RouteList &$routerList) : void
    {
        $router = new RouteList('Front');

        $router[] = new Route('<article>/', [
            'presenter' => 'Article',
            'action'    => 'default',
            'article'   => [
                Route::FILTER_IN  => static function ($webalize) : ?string {
                    $article = self::$facadeArticle->findByWebalize($webalize);

                    if ($article !== null) {
                        return $article->isActive() ? $article->getWebalize() : null;
                    }

                    return null;
                },
                Route::FILTER_OUT => static function ($article) {
                    if ($article instanceof Article) {
                        return $article->getWebalize();
                    }

                    return $article;
                },
            ],
        ]);

        $router[] = new Route('<presenter>/<action>/[<id>]', [
            'presenter' => [
                Route::VALUE        => 'Homepage',
                Route::FILTER_TABLE => [], //'presenter' => 'Presenter'
            ],
            'action'    => [
                Route::VALUE        => 'default',
                Route::FILTER_TABLE => [], //'akce' => 'Action',
            ],
            'id'        => null,
            null        => [
                Route::FILTER_IN  => static function (array $params) : array {
                    return self::filterFrontIn($params);
                },
                Route::FILTER_OUT => static function (array $params) : array {
                    return self::filterFrontOut($params);
                },
            ],
        ]);

        $routerList[] = $router;
    }

    /**
     * @param mixed[] $params
     *
     * @return mixed[]
     */
    public static function filterFrontIn(array $params) : array
    {
        switch ($params['presenter']) {
            case 'XXXX':
                //$galleryWebalize = Strings::camelizeToWebalize($params['action']);
                //$gallery         = self::$facadeGallery->findByWebalize($galleryWebalize);
                //if ($gallery) {
                $params['action'] = 'detail';
                //$params['id']     = $galleryWebalize;
                //}
                break;
        }

        return $params;
    }

    /**
     * @param mixed[] $params
     *
     * @return mixed[]
     */
    public static function filterFrontOut(array $params) : array
    {
        switch ($params['presenter']) {
            case 'XXXX':
                if (isset($params['id'])) {
                    //$gallery = self::$facadeGallery->findByWebalize($params['id']);
                    //if ($gallery) {
                    $params['action'] = $params['id'];
                    $params['id']     = null;
                    //}
                }
                break;
        }

        return $params;
    }
}
