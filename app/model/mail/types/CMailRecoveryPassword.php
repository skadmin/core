<?php

declare(strict_types=1);

namespace App\Model\Mail\Type;

use DateTimeInterface;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use function sprintf;

class CMailRecoveryPassword extends CMail
{
    public const TYPE = 'recovery-password';

    /** @var string */
    private $hash;

    /** @var string */
    private $href;

    /** @var string */
    private $link;

    /** @var DateTimeInterface */
    private $validTo;

    public function __construct(string $hash, string $href, string $link, DateTimeInterface $validTo)
    {
        $this->hash    = $hash;
        $this->href    = $href;
        $this->link    = $link;
        $this->validTo = $validTo;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize() : array
    {
        $params = ['hash', 'href', 'link', 'valid-to'];
        $model  = [];

        foreach ($params as $param) {
            $description = sprintf('mail.recovery-password.parameter.%s.description', $param);
            $example     = sprintf('mail.recovery-password.parameter.%s.example', $param);

            $model[] = (new MailTemplateParameter($param, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues() : array
    {
        return [
            new MailParameterValue('hash', $this->getHash()),
            new MailParameterValue('href', $this->getHref()),
            new MailParameterValue('link', $this->getLink()),
            new MailParameterValue('valid-to', $this->getValidTo()),
        ];
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setHash(string $hash) : CMailRecoveryPassword
    {
        $this->hash = $hash;
        return $this;
    }

    public function getHref() : string
    {
        return $this->href;
    }

    public function setHref(string $href) : CMailRecoveryPassword
    {
        $this->href = $href;
        return $this;
    }

    public function getLink() : string
    {
        return $this->link;
    }

    public function setLink(string $link) : CMailRecoveryPassword
    {
        $this->link = $link;
        return $this;
    }

    public function getValidTo() : DateTimeInterface
    {
        return $this->validTo;
    }

    public function setValidTo(DateTimeInterface $validTo) : CMailRecoveryPassword
    {
        $this->validTo = $validTo;
        return $this;
    }
}
