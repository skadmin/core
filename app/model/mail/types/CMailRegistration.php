<?php

declare(strict_types=1);

namespace App\Model\Mail\Type;

use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use function sprintf;

class CMailRegistration extends CMail
{
    public const TYPE = 'registration';

    /** @var string */
    private $name;

    /** @var string */
    private $email;

    /** @var string */
    private $href;

    /** @var string */
    private $link;

    public function __construct(string $name, string $email, string $href, string $link)
    {
        $this->name  = $name;
        $this->email = $email;
        $this->href  = $href;
        $this->link  = $link;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize() : array
    {
        $params = ['name', 'email', 'href', 'link'];
        $model  = [];

        foreach ($params as $param) {
            $description = sprintf('mail.registration.parameter.%s.description', $param);
            $example     = sprintf('mail.registration.parameter.%s.example', $param);

            $model[] = (new MailTemplateParameter($param, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues() : array
    {
        return [
            new MailParameterValue('name', $this->getName()),
            new MailParameterValue('email', $this->getEmail()),
            new MailParameterValue('href', $this->getHref()),
            new MailParameterValue('link', $this->getLink()),
        ];
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name) : CMailRegistration
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function setEmail(string $email) : CMailRegistration
    {
        $this->email = $email;
        return $this;
    }

    public function getHref() : string
    {
        return $this->href;
    }

    public function setHref(string $href) : CMailRegistration
    {
        $this->href = $href;
        return $this;
    }

    public function getLink() : string
    {
        return $this->link;
    }

    public function setLink(string $link) : CMailRegistration
    {
        $this->link = $link;
        return $this;
    }
}
