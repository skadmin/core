<?php

declare(strict_types=1);

namespace App\Model\Mail;

use Skadmin\Mailing\Model\MailService;
use Skadmin\Translator\Translator;

class MailClassBox
{
    /** @var MailService */
    private $mailService;

    /** @var Translator */
    private $translator;

    public function __construct(MailService $mailService, Translator $translator)
    {
        $this->mailService = $mailService;
        $this->translator  = $translator;
    }

    public function getMailService() : MailService
    {
        return $this->mailService;
    }

    public function getTranslator() : Translator
    {
        return $this->translator;
    }
}
