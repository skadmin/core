<?php

declare(strict_types=1);

namespace App\Model\Exception;

use Exception;
use Throwable;
use function sprintf;

class ExceptionNotSetPrivilege extends Exception
{
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(sprintf('Privilege is null [%s]', $message), $code, $previous);
    }
}
