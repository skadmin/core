<?php

declare(strict_types=1);

namespace App\Model\System;

use Skadmin\Translator\SimpleTranslation;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

trait APackageControl
{
    /** @var bool */
    private $isModal = false;

    /** @var bool */
    private $drawBox = true;

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        return '';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [];
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [];
    }

    public function isModal() : bool
    {
        return $this->isModal;
    }

    public function setDrawBox(bool $drawBox = true) : void
    {
        $this->drawBox = $drawBox;
    }
}
