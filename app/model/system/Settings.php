<?php

declare(strict_types=1);

namespace App\Model\System;

final class Settings
{
    public const MENU_SIMPLE_ITEM_INTERNAL = [':Front:Homepage:default' => 'simple-menu-item-internal.index'];
}
