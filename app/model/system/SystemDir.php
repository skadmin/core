<?php

declare(strict_types=1);

/**
 * User: David Skála
 * Date: 01.08.2018
 * Time: 9:17
 */

namespace App\Model\System;

use const DIRECTORY_SEPARATOR;
use function implode;
use function is_dir;
use function mkdir;
use function str_replace;
use function umask;

/**
 * Class WebDir
 */
class SystemDir
{
    /** @var string */
    private $wwwDir;

    /** @var string */
    private $appDir;

    /** @var string */
    private $rfmDir;

    /** @var string */
    private $migrationDir;

    public function __construct(string $wwwDir, string $appDir)
    {
        $this->wwwDir = $wwwDir;
        $this->appDir = $appDir;
        $this->rfmDir = $this->getPathWww(['storage', 'rf', 's']);

        $this->migrationDir = str_replace(DIRECTORY_SEPARATOR . 'www', DIRECTORY_SEPARATOR . 'migrations', $wwwDir);
    }

    public function getMigrationDir() : string
    {
        return $this->migrationDir;
    }

    /**
     * @param string[] $from
     */
    public function getPathWww(array $from = []) : string
    {
        return $this->getPath($this->wwwDir, $from);
    }

    /**
     * @param string[]|string $from
     */
    public function getPathRfm($from = []) : string
    {
        return $this->getPath($this->rfmDir, (array) $from);
    }

    /**
     * @param string[] $from
     */
    private function getPath(string $dir, array $from = []) : string
    {
        return $dir . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $from);
    }

    /**
     * @param string[] $from
     */
    public function getPathApp(array $from = []) : string
    {
        return $this->getPath($this->appDir, $from);
    }

    public function createDir(string $dir, int $mod = 0777) : void
    {
        if (is_dir($dir)) {
            return;
        }

        $oldMod = umask(0);
        mkdir($dir, $mod, true);
        umask($oldMod);
    }
}
