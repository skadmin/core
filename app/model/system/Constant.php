<?php

declare(strict_types=1);

/**
 * User: David Skála
 * Date: 24.07.2019
 * Time: 12:09
 */

namespace App\Model\System;

class Constant
{
    public const LOCAL_CONFIG = [
        '127.0.0.1',
        '::1',
    ];

    public const PROMTP   = '--';
    public const YES      = 1;
    public const NO       = 0;
    public const YES_TEXT = 'yes';
    public const NO_TEXT  = 'no';
    // array
    public const PROMTP_ARR  = [null => '--'];
    public const DIAL_YES_NO = [
        self::NO  => self::NO_TEXT,
        self::YES => self::YES_TEXT,
    ];

    public const HREF_TARGET_BLANK = '_blank';
    public const HREF_TARGET_SELF  = null;
    public const HREF_TARGETS      = [
        self::HREF_TARGET_SELF   => 'href-target.self',
        self::HREF_TARGET_BLANK  => 'href-target.blank',
    ];
}
