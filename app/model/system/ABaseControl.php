<?php

declare(strict_types=1);

namespace App\Model\System;

use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use ReflectionClass;
use function sprintf;

abstract class ABaseControl
{
    public const MODULE_FRONT = 'Front';
    public const MODULE_ADMIN = 'Admin';

    /** @var string|null */
    private $resource = null;

    public function __construct(?string $resource = null)
    {
        $this->resource = $resource;
    }

    abstract public function getMenu() : ArrayHash;

    public function getControlClass(string $module, string $name) : string
    {
        $namespace = (new ReflectionClass($this))->getNamespaceName();
        return sprintf('%s\\Components\\%s\\I%sFactory', $namespace, $module, Strings::firstUpper($name));
    }

    public function getResource() : ?string
    {
        return $this->resource ?? 'base-resource';
    }
}
