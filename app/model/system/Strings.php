<?php

declare(strict_types=1);

namespace App\Model\System;

use function array_map;
use function explode;
use function implode;
use function preg_replace;
use function strtolower;

final class Strings
{
    public static function camelize(?string $_string) : string
    {
        $string = \Nette\Utils\Strings::webalize($_string ?? '');
        $words  = explode('-', $string);
        $words  = array_map('ucfirst', $words);
        return implode('', $words);
    }

    public static function camelizeToWebalize(string $_string) : string
    {
        return strtolower((string) preg_replace('/(?<!^)[A-Z]/', '-$0', $_string));
    }
}
