<?php

declare(strict_types=1);

namespace App\Model\System;

class Flash
{
    public const
        PRIMARY   = 'primary',
        SECONDARY = 'secondary',
        SUCCESS   = 'success',
        DANGER    = 'danger',
        WARNING   = 'warning',
        INFO      = 'info',
        LIGHT     = 'light',
        DARK      = 'dark';
}
