<?php

declare(strict_types=1);

namespace App\Model\System;

use Nette\Utils\Html;
use Nette\Utils\Strings;
use Skadmin\Article\Doctrine\Article\Article;
use function explode;
use function implode;

final class Callback
{
    public static function getHtmlArticle(Article $article, ?string $titleTag = null) : ?Html
    {
        $classes = ['article-title'];

        $titleTag = (string) $titleTag;

        if (Strings::contains($titleTag, '.')) {
            [$title, $_classes] = explode('.', $titleTag);
            foreach (explode(' ', $_classes) as $class) {
                $classes[] = $class;
            }
        } else {
            $title = $titleTag;
        }

        $html = new Html();
        if ($title !== '') {
            $html->addHtml(Html::el($title, ['class' => implode(' ', $classes)])
                ->setText($article->getName()));
        }
        $html->addHtml($article->getContent());

        return $html;
    }
}
