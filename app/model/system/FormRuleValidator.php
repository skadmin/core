<?php

declare(strict_types=1);

namespace App\Model\System;

use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\IControl;
use function in_array;

final class FormRuleValidator
{
    // pravidla jsou specifikovány v js _core/js/nette-form-rule.js
    public const IS_SELECTED = 'App\Model\System\FormRuleValidator::isSelected';

    /**
     * @param mixed $value
     */
    public static function isSelected(IControl $control, $value) : bool
    {
        if ($control instanceof MultiSelectBox) {
            $values = $control->getValue();
        } elseif ($control instanceof SelectBox) {
            $values = [$control->getValue()];
        } else {
            $values = [];
        }

        return in_array($value, $values, true);
    }
}
