<?php

declare(strict_types=1);

namespace App\Model\System;

use Nette\Utils\Html;
use Nette\Utils\Strings;
use Nette\Utils\Validators;
use function chunk_split;
use function count;
use function explode;
use function sprintf;
use function str_replace;
use function trim;

final class Utils
{
    /**
     * @param string|Html $separator
     *
     * @return Html
     */
    public static function createHtmlContact(string $contact, $separator = ',') : Html
    {
        $contacts     = explode(',', $contact);
        $parseContacs = [];

        foreach ($contacts as $_contact) {
            $parseContacs[] = self::createHtmlContactSingle(trim($_contact));
        }

        $resultContact = new Html();

        $count = count($parseContacs);

        foreach ($parseContacs as $_contact) {
            if ($_contact instanceof Html) {
                $resultContact->addHtml($_contact);
            } else {
                $resultContact->addText($_contact);
            }

            if ($count-- <= 1) {
                continue;
            }

            if ($separator instanceof Html) {
                $resultContact->addHtml($separator);
            } else {
                $resultContact->addText($separator);
            }
        }

        return $resultContact;
    }

    /**
     * @return string|Html
     */
    private static function createHtmlContactSingle(string $contact)
    {
        if (Validators::isEmail($contact)) { // E-MAIL
            $icon = Html::el('i', ['class' => 'fas fa-fw fa-sm fa-at']);

            $resultContact = Html::el('a', [
                'href'  => sprintf('mailto:%s', $contact),
                'class' => trim('text-nowrap'),
            ])->addHtml($icon)->addText(' ' . $contact);
        } elseif (Strings::match($contact, '/^(\+420|\+421)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$/') !== null) { // PHONE
            $icon = Html::el('i', ['class' => 'fas fa-fw fa-sm fa-phone-alt']);

            $phoneWithoutSpace = str_replace(' ', '', $contact);

            if (Strings::startsWith($phoneWithoutSpace, '+')) {
                $phoneFormated = str_replace('+', '', $phoneWithoutSpace);
                $phoneFormated = '+' . trim(chunk_split($phoneFormated, 3, ' '));
            } else {
                $phoneFormated = trim(chunk_split($phoneWithoutSpace, 3, ' '));
            }

            $resultContact = Html::el('a', [
                'href'  => sprintf('tel:%s', $phoneWithoutSpace),
                'class' => trim('text-nowrap'),
            ])->addHtml($icon)->addText(' ' . $phoneFormated);
        } else { // ONLY TEXT
            $resultContact = $contact;
        }

        return $resultContact;
    }

    /**
     * @return Html
     */
    public static function createFacebookContact(string $facebookurl, string $defaultName = 'name not found') : Html
    {
        $icon = Html::el('i', ['class' => 'fab fa-fw fa-sm fa-facebook-f']);

        if ($facebookurl !== '' && Strings::contains($facebookurl, 'facebook')) {
            $facebookurl = Strings::lower($facebookurl);

            if (! Strings::startsWith($facebookurl, 'http')) {
                $facebookurl = sprintf('https://%s', $facebookurl);
            }

            $contact = Html::el('a', [
                'href'   => sprintf('%s', $facebookurl),
                'target' => '_blank',
                'class'  => trim('text-nowrap'),
            ])->addHtml($icon)->addText(sprintf(' %s', $defaultName));
        } else {
            $contact = Html::el('span')->addHtml($icon)->addText(' invalide link');
        }

        return $contact;
    }
}
