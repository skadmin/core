<?php

declare(strict_types=1);

namespace App\Model\Security;

use App\Model\Doctrine\Role\Privilege;
use App\Model\Doctrine\Role\RoleFacade;
use App\Model\Doctrine\Role\Source;
use App\Model\Exception\ExceptionNotSetPrivilege;
use App\Model\Exception\ExceptionUndefinedPrivilege;
use Nette\InvalidStateException;
use Nette\Security\Permission;
use Nette\Utils\Arrays;
use Throwable;
use function count;
use function in_array;

class AuthorizatorAdmin extends Permission
{
    /** @var RoleFacade */
    private $facadeRole;

    /** @var Source[] */
    private $resources = [];

    public function __construct(RoleFacade $facadeRole)
    {
        $this->facadeRole = $facadeRole;
        $this->loadPermission();
    }

    private function loadPermission() : void
    {
        $this->removeAllRoles();
        $this->removeAllResources();
        $this->removeAllow();

        try {
            $resources = $this->facadeRole->getAllResources();

            foreach ($resources as $resource) {
                $this->addResource($resource->getName());
            }

            foreach ($this->facadeRole->getAllRoles(true) as $role) {
                $this->addRole($role->getWebalize());

                if ($role->isMasterAdmin()) {
                    $this->allow($role->getWebalize());
                } else {
                    foreach ($role->getPrivileges() as $privilege) {
                        $privileges = $privilege->getAllowPrivilege();

                        if (count($privileges) === 0 || $privilege->getRole()->isMasterAdmin()) {
                            continue;
                        }

                        $this->allow(
                            $privilege->getRole()->getWebalize(),
                            $privilege->getResource()->getName(),
                            $privileges
                        );
                    }
                }
            }

            // load all resource for invalidate
            $this->resources = Arrays::map($this->facadeRole->getAllResources(), static function (Source $resource) {
                return $resource->getName();
            });
        } catch (Throwable $e) {
            return;
        }
    }

    /**
     * @param string|null $role
     * @param string|null $resource
     * @param string|null $privilege
     *
     * @return bool
     */
    public function isAllowed($role = self::ALL, $resource = self::ALL, $privilege = self::ALL) : bool
    {
        if ($privilege === null) {
            throw new ExceptionNotSetPrivilege('Not set privilege');
        } elseif (! in_array($resource, $this->resources, true)) {
            $this->facadeRole->createResource($resource);
            $this->loadPermission();
        }

        try {
            return parent::isAllowed($role, $resource, $privilege);
        } catch (InvalidStateException $e) {
            return false;
        }
    }
}
