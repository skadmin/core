<?php

declare(strict_types=1);

namespace App\Model\Security;

use App\Model\Doctrine\User\UserFacade;
use Nette\Security;
use Nette\SmartObject;

class AuthenticatorAdmin implements Security\IAuthenticator
{
    use SmartObject;

    /** @var UserFacade */
    private $userFacade;

    /** @var Security\Passwords */
    private $password;

    public function __construct(UserFacade $userFacade)
    {
        $this->userFacade = $userFacade;
        $this->password   = new Security\Passwords();
    }

    /**
     * @param mixed[] $credentials
     *
     * @throws Security\AuthenticationException
     */
    public function authenticate(array $credentials) : Security\IIdentity
    {
        [$email, $password] = $credentials;

        $user = $this->userFacade->findByUsername($email);

        if ($user === null) {
            throw new Security\AuthenticationException('authenticator-admin.user-not-found');
        } elseif (! $user->isActive()) {
            throw new Security\AuthenticationException('authenticator-admin.user-is-not-active');
        } elseif (! $this->password->verify($password, $user->getUserPassword())) {
            throw new Security\AuthenticationException('authenticator-admin.password-is-wrong');
        }

        $user->loadUserRoles();
        return $user;
    }
}
