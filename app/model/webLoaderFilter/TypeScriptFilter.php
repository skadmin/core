<?php

declare(strict_types=1);

namespace App\Model\WebLoaderFilter;

use Nette\Utils\Strings;
use WebLoader\Compiler;
use WebLoader\Filter\Process;
use const PATHINFO_EXTENSION;
use function escapeshellarg;
use function file_get_contents;
use function pathinfo;
use function sprintf;
use function substr_replace;

/**
 * TypeScript filter
 */
class TypeScriptFilter
{
    /** @var string */
    private $bin;

    /** @var mixed[] */
    private $env;

    /**
     * @param mixed[] $env
     */
    public function __construct(string $bin = 'tsc', array $env = [])
    {
        $this->bin = $bin;
        $this->env = $env + $_ENV;
        unset($this->env['argv'], $this->env['argc']);
    }

    /**
     * @return false|string
     */
    public function __invoke(string $code, Compiler $compiler, ?string $file = null)
    {
        $file = (string) $file;

        if (Strings::contains($file, 'tsconfig.json')) {
            // outfile must be "app.js"
            $out = substr_replace($file, 'app.js', -13);
            $cmd = sprintf('tsc --project %s', escapeshellarg($file));
            Process::run($cmd, null, null, $this->env);
            $code = file_get_contents($out);
        } elseif (pathinfo($file, PATHINFO_EXTENSION) === 'ts') {
            $out = substr_replace($file, 'js', -2);
            $cmd = sprintf('%s %s --target ES5 --out %s', $this->bin, escapeshellarg($file), escapeshellarg($out));
            Process::run($cmd, null, null, $this->env);
            $code = file_get_contents($out);
        }

        return $code;
    }
}
