<?php

declare(strict_types=1);

namespace App\Model\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class FooCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'skadmin:dash';

    protected function configure() : void
    {
        $this->setName('skadmin:dash')
            ->setDescription('Dash test.')
            ->addOption('dashing', 'd');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        echo 'xx';

        return 1;
    }
}
