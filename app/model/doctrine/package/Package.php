<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Package;

use App\Model\Doctrine\Traits;
use App\Model\System\ABaseControl;
use function class_exists;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Cache(usage="READ_ONLY")
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Package
{
    use Traits\Id;
    use Traits\WebalizeName;
    use Traits\IsActive;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $baseControl = '';

    public function getBaseControl() : ?ABaseControl
    {
        $className = $this->baseControl;
        if (class_exists($className)) {
            return new $className($this->getWebalize());
        }

        return null;
    }
}
