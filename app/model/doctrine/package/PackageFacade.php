<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Package;

use App\Model\Doctrine\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class PackageFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table = Package::class;
    }

    /**
     * @return Package[]
     */
    public function getAll() : array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    protected function get(?int $id = null) : Package
    {
        /** @var Package $package */
        $package = parent::get($id);
        return $package;
    }

    public function getByBaseControl(string $baseControl) : ?Package
    {
        $criteria = ['baseControl' => $baseControl];

        /** @var Package|null $package */
        $package = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $package;
    }

    public function getByWebalize(string $webalize) : ?Package
    {
        $criteria = ['webalize' => $webalize];

        /** @var Package|null $package */
        $package = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $package;
    }
}
