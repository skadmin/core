<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Role;

use App\Model\Doctrine\Traits;
use App\Model\Doctrine\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\Security\Permission;
use function boolval;

/**
 * Class Role
 *
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Cache(usage="NONSTRICT_READ_WRITE")
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Role
{
    use Traits\Id;
    use Traits\WebalizeName;
    use Traits\IsLocked;
    use Traits\IsActive;

    // COLUMNS

    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", options={"default" : 0})
     * @var bool
     */
    private $isMasterAdmin = false;

    // ASOCIACE

    /**
     * @Doctrine\ORM\Mapping\ManyToMany(targetEntity="App\Model\Doctrine\User\User", mappedBy="roles")
     * @var User[]|ArrayCollection
     */
    private $users;

    /**
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="Privilege", mappedBy="role")
     * @Doctrine\ORM\Mapping\Cache(usage="NONSTRICT_READ_WRITE")
     * @var Permission[]|ArrayCollection
     */
    private $privileges;

    public function __construct()
    {
        $this->users      = new ArrayCollection();
        $this->privileges = new ArrayCollection();
    }

    // GETTER

    /**
     * Is master admin?
     */
    public function isMasterAdmin() : bool
    {
        return boolval($this->isMasterAdmin);
    }

    /** @return User[]|mixed */
    public function getUsers()
    {
        /** @var User[] $user */
        $user = $this->users;
        return $user;
    }

    /** @return Privilege[]|mixed */
    public function getPrivileges()
    {
        /** @var Privilege[] $privileges */
        $privileges = $this->privileges;
        return $privileges;
    }

    // SETTER

    /**
     * update Role
     */
    public function updateRole(string $name, bool $isActive) : void
    {
        $this->name = $name;
        $this->setIsActive($isActive);
    }
}
