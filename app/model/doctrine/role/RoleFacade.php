<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Role;

use App\Model\Doctrine\Facade;
use App\Model\System\SystemDir;
use Nette\Utils\FileSystem;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Nette\Utils\Validators;
use Nettrine\ORM\EntityManagerDecorator;
use function intval;
use function is_dir;
use function sprintf;

final class RoleFacade extends Facade
{
    /** @var string */
    private $tableResource;

    /** @var string */
    private $tablePrivilege;

    /** @var SystemDir */
    protected $systemDir;

    public function __construct(EntityManagerDecorator $em, SystemDir $systemDir)
    {
        parent::__construct($em);
        $this->systemDir = $systemDir;

        $this->table          = Role::class;
        $this->tableResource  = Source::class;
        $this->tablePrivilege = Privilege::class;
    }

    public function createRole(string $name, bool $isActive) : ?Role
    {
        if ($this->findByName($name) === null) {
            return $this->updateRole($this->get(), $name, $isActive);
        }

        return null;
    }

    public function findByName(string $name) : ?Role
    {
        return $this->findByWebalize(Strings::webalize($name));
    }

    public function findByWebalize(string $webalize) : ?Role
    {
        $criteria = ['webalize' => $webalize];

        /** @var Role|null $role */
        $role = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $role;
    }

    /**
     * @param Role|int $_role
     */
    public function updateRole($_role, string $name, bool $isActive) : ?Role
    {
        if ($_role instanceof Role) {
            $role = $_role;
        } elseif (Validators::isNumericInt($_role)) {
            $role = $this->get($_role);
        } else {
            return null;
        }

        $role->updateRole($name, $isActive);

        $this->em->persist($role);
        $this->em->flush();

        return $role;
    }

    public function get(?int $id = null) : Role
    {
        if ($id === null) {
            return new Role();
        }

        $role = parent::get($id);

        if ($role === null) {
            return new Role();
        }

        return $role;
    }

    /**
     * @return Role[]
     */
    public function getAllRoles(bool $onlyActive = false) : array
    {
        $criteria = [];
        if ($onlyActive) {
            $criteria['isActive'] = intval($onlyActive);
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    /**
     * @return Source[]
     */
    public function getAllResources() : array
    {
        return $this->em
            ->getRepository($this->tableResource)
            ->findAll();
    }

    public function createResource(string $name) : ?Source
    {
        if ($this->findResourceByName($name) === null) {

            /** @var Source $resource */
            $resource = $this->getResource();
            $resource->createResource($name);

            $this->em->persist($resource);
            $this->em->flush();

            return $resource;
        }

        return null;
    }

    public function findResourceByName(string $name) : ?Source
    {
        $criteria = ['name' => Strings::webalize($name)];

        /** @var Source|null $resource */
        $resource = $this->em
            ->getRepository($this->tableResource)
            ->findOneBy($criteria);

        return $resource;
    }

    public function getResource(?int $id = null) : Source
    {
        if ($id === null) {
            return new Source();
        }

        $resource = parent::getFrom($id, $this->tableResource);

        if ($resource === null) {
            return new Source();
        }

        return $resource;
    }

    /**
     * @param Role|int   $_role
     * @param Source|int $_resource
     * @param string[]   $additionalPrivilege
     */
    public function createPrivilege($_role, $_resource, bool $read, bool $write, bool $delete, array $additionalPrivilege) : ?Privilege
    {
        if ($_role instanceof Role) {
            $role = $_role;
        } else {
            $role = $this->get(intval($_role));
        }

        if ($_resource instanceof Source) {
            $resource = $_resource;
        } else {
            $resource = $this->getResource(intval($_resource));
        }

        if ($role->isLoaded() && $resource->isLoaded()) {
            $privilege = $this->getPrivilege($role, $resource);

            if (! $privilege->isLoaded()) {
                $privilege->createPrivilege($role, $resource);
                $this->em->persist($privilege);
            }

            $privilege->updatePrivilege($read, $write, $delete, $additionalPrivilege);

            $this->em->persist($privilege);
            $this->em->flush();

            // rename cache dir
            $pathCache   = $this->systemDir->getPathApp(['..', 'temp', 'cache', 'nettrine.cache']);
            $randomSufix = sprintf('del-%s', Random::generate());
            $_pathCache  = $this->systemDir->getPathApp(['..', 'temp', 'cache', 'nettrine.cache-del', $randomSufix]);
            if (is_dir($pathCache)) {
                FileSystem::rename($pathCache, $_pathCache);
            }

            return $privilege;
        }

        return null;
    }

    public function getPrivilege(Role $role, Source $resource) : Privilege
    {
        $criteria = [
            'role'     => $role,
            'resource' => $resource,
        ];

        /** @var Privilege|null $privilege */
        $privilege = $this->em
            ->getRepository($this->tablePrivilege)
            ->findOneBy($criteria);

        if ($privilege === null) {
            return new Privilege();
        }

        return $privilege;
    }
}
