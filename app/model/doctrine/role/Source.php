<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Role;

use App\Model\Doctrine\Traits;
use Nette\Utils\Strings;
use function is_array;
use function sprintf;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Table(name="resource")
 * @Doctrine\ORM\Mapping\Cache(usage="NONSTRICT_READ_WRITE")
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Source
{
    use Traits\Id;
    use Traits\Name;
    use Traits\Title;
    use Traits\Description;

    /**
     * @Doctrine\ORM\Mapping\Column(type="array", nullable=true)
     * @var string[]|null
     */
    private $additionalPrivilege = null;

    public function createResource(string $name) : void
    {
        $this->name        = Strings::webalize($name);
        $this->title       = sprintf('role-resource.%s.title', $this->name);
        $this->description = sprintf('role-resource.%s.description', $this->name);
    }

    /**
     * @return array|string[]
     */
    public function getAdditionalPrivilege() : array
    {
        if (! is_array($this->additionalPrivilege)) {
            $this->additionalPrivilege = [];
        }

        return $this->additionalPrivilege;
    }
}
