<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Role;

use App\Model\Doctrine\Traits;
use function array_keys;
use function boolval;
use function in_array;
use function is_array;
use function is_bool;
use function sprintf;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Cache(usage="NONSTRICT_READ_WRITE")
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Privilege
{
    public const READ       = 'read';
    public const WRITE      = 'write';
    public const DELETE     = 'delete';
    public const PRIVILEGES = [
        self::READ,
        self::WRITE,
        self::DELETE,
    ];
    use Traits\Id;

    // COLUMNS

    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", name="p_read", options={"default" : 0})
     * @var bool
     */
    private $read = false;

    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", name="p_write", options={"default" : 0})
     * @var bool
     */
    private $write = false;

    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", name="p_delete", options={"default" : 0})
     * @var bool
     */
    private $delete = false;

    /**
     * @Doctrine\ORM\Mapping\Column(type="array", nullable=true)
     * @var bool[]|null
     */
    private $additionalPrivilege = null;

    // ASOCIACE

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="Role", inversedBy="privileges")
     * @Doctrine\ORM\Mapping\JoinColumn(onDelete="cascade")
     * @var Role
     */
    private $role;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="Source")
     * @Doctrine\ORM\Mapping\JoinColumn(onDelete="cascade")
     * @var Source
     */
    private $resource;


    /**
     * @return string[]
     */
    public function getAllowPrivilege(bool $usePrefix = false) : array
    {
        $allow = [];

        if ($this->isRead()) {
            $allow[] = self::READ;
        }

        if ($this->isWrite()) {
            $allow[] = self::WRITE;
        }

        if ($this->isDelete()) {
            $allow[] = self::DELETE;
        }

        $addPrivileges = $this->getAdditionalPrivilege();

        if (is_array($addPrivileges)) {
            foreach (array_keys($addPrivileges) as $addPrivilege) {
                if ($usePrefix) {
                    $allow[] = sprintf('privilege.%s', $addPrivilege);
                } else {
                    $allow[] = $addPrivilege;
                }
            }
        }

        return $allow;
    }

    /**
     * @param bool|int $read
     * @param bool|int $write
     * @param bool|int $delete
     * @param string[] $additionalPrivilege
     */
    public function updatePrivilege($read, $write, $delete, array $additionalPrivilege) : void
    {
        $this->read   = boolval($read);
        $this->write  = boolval($write);
        $this->delete = boolval($delete);

        $this->additionalPrivilege = [];
        foreach ($additionalPrivilege as $addPrivilege) {
            $this->additionalPrivilege[$addPrivilege] = true;
        }
    }

    public function isRead() : bool
    {
        return boolval($this->read);
    }

    public function isWrite() : bool
    {
        return boolval($this->write);
    }

    public function isDelete() : bool
    {
        return boolval($this->delete);
    }

    public function isPrivilege(string $privilege) : bool
    {
        if (in_array($privilege, self::PRIVILEGES, true)) {
            return boolval($this->{$privilege});
        }

        $result = $this->getAdditionalPrivilege($privilege);

        return is_bool($result) ? $result : false;
    }

    public function getRole() : Role
    {
        return $this->role;
    }

    public function getResource() : Source
    {
        return $this->resource;
    }

    public function createPrivilege(Role $role, Source $resource) : void
    {
        $this->role     = $role;
        $this->resource = $resource;
    }

    /**
     * @return bool[]|bool
     */
    public function getAdditionalPrivilege(?string $privilege = null)
    {
        if (! is_array($this->additionalPrivilege)) {
            $this->additionalPrivilege = [];
        }

        if ($privilege === null) {
            return $this->additionalPrivilege;
        }

        return $this->additionalPrivilege[$privilege] ?? false;
    }
}
