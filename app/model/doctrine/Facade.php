<?php

declare(strict_types=1);

namespace App\Model\Doctrine;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Nettrine\ORM\EntityManagerDecorator;
use function count;
use function implode;
use function intval;
use function sprintf;

abstract class Facade
{
    /** @var string */
    protected $table;

    /** @var EntityManagerDecorator */
    protected $em;

    public function __construct(EntityManagerDecorator $em)
    {
        $this->em = $em;
    }

    public function getModel(?string $table = null) : QueryBuilder
    {
        /*
        if (! $this->table) {
            throw new NotSetTableInFacade();
        }
        */

        if ($table === null) {
            $table = $this->table;
        }

        /** @var EntityRepository $repository */
        $repository = $this->em
            ->getRepository($table);

        return $repository->createQueryBuilder('a');
    }

    /**
     *  $joinTables = [
     *      'language' => ArrayHash::from([
     *          'table' => $this->tableLanguage,
     *          'conditionType' => Expr\Join::WITH,
     *          'condition' => sprintf('%s.language = %s.id', 'mutation', 'language'),
     *      ]),
     *      'translation' => ArrayHash::from([
     *          'table' => $this->table,
     *          'conditionType' => Expr\Join::WITH,
     *          'condition' => sprintf('%s.translation = %s.id', 'mutation', 'translation'),
     *      ]),
     *  ];
     *
     * @param mixed[] $joinTables
     *
     * @throws Exception
     */
    protected function getModelWithJoinTable(string $table, string $tableAlias, array $joinTables) : QueryBuilder
    {
        if (count($joinTables) === 0) {
            throw new Exception('is not specific joined table.');
        }

        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository($table);

        $qb = $repository->createQueryBuilder($tableAlias);

        foreach ($joinTables as $alias => $joinTable) {
            $qb->join($joinTable->table, (string) $alias, $joinTable->conditionType, $joinTable->condition);
        }

        return $qb;
    }

    /**
     * @return mixed
     */
    protected function get(?int $id = null)
    {
        return $this->em
            ->getRepository($this->table)
            ->find($id);
    }

    /*
    public function getPair(string $key, string $value, ?string $where = null)
    {
        $repository = $this->em
            ->getRepository($this->table);

        if ($where !== null) {
            return $repository->findPairs($where, $value, $key);
        }

        return $this->em
            ->getRepository($this->table)
            ->findPairs($value, $key);
    }
    */

    /**
     * @return string[]
     */
    public function getPairs(string $key, string $value) : array
    {
        $select        = sprintf('a.%s AS key, a.%s AS value', $key, $value);
        $doctrineNames = $this->getModel()
            ->select($select)
            ->getQuery()
            ->getArrayResult();

        $names = [];
        foreach ($doctrineNames as $row) {
            $names[$row['key']] = $row['value'];
        }

        return $names;
    }

    /**
     * @return mixed
     */
    protected function getFrom(int $id, string $table)
    {
        return $this->em
            ->getRepository($table ?? $this->table)
            ->find($id);
    }

    /**
     * @param mixed[] $_where
     */
    public function sort(?string $itemId, ?string $prevId, ?string $nextId, array $_where = []) : void
    {
        $tableName  = $this->em->getClassMetadata($this->table)->getTableName();
        $connection = $this->em->getConnection();

        $a_where = ['1 = 1'];
        foreach ($_where as $condition => $value) {
            $a_where[] = sprintf($condition, $value);
        }
        $where = implode(' AND ', $a_where);

        if ($prevId !== null) {
            $before          = $this->get(intval($prevId));
            $currentSequence = $before->getSequence() + 1;

            $connection->executeQuery('SET @sequence := -2');
            $connection->executeQuery(
                sprintf('UPDATE %s SET sequence = @sequence := @sequence + 1 WHERE sequence <= ? AND %s ORDER BY sequence ASC', $tableName, $where),
                [$before->getSequence()]
            );
        } else {
            $currentSequence = 0;
        }

        if ($nextId !== null) {
            $after = $this->get(intval($nextId));

            $connection->executeQuery('SET @sequence := ?', [$after->getSequence() + 1]);
            $connection->executeQuery(
                sprintf('UPDATE %s SET sequence = @sequence := @sequence + 1 WHERE sequence >= ? AND %s ORDER BY sequence ASC', $tableName, $where),
                [$after->getSequence()]
            );
        }

        $current = $this->get(intval($itemId));
        $current->updateSequence($currentSequence);
        $this->em->flush();

        $connection->executeQuery('SET @sequence := -1');
        $connection->executeQuery(sprintf('UPDATE %s SET sequence = @sequence := @sequence + 1 WHERE %s ORDER BY sequence ASC', $tableName, $where));
    }
}
