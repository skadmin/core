<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Setting;

use App\Model\Doctrine\Traits;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 * @Doctrine\ORM\Mapping\Cache(usage="READ_ONLY", region="setting")
 */
class Section
{
    use Traits\Id;
    use Traits\WebalizeName;
    use Traits\Description;

    // description = setting-section.%s.description

    // ASOCIACE

    /**
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="Setting", mappedBy="section")
     * @var Setting[]|ArrayCollection
     */
    private $settings;

    public function __construct()
    {
        $this->settings = new ArrayCollection();
    }

    /**
     * @return Setting[]
     */
    public function getSettings() : array
    {
        return $this->settings->toArray();
    }
}
