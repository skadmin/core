<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Setting;

use App\Model\Doctrine\Traits;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Setting
{
    use Traits\Id;
    use Traits\Name;
    use Traits\Code;
    use Traits\Value;
    use Traits\Description;

    // description = settings-setting.%s.description

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="Section", inversedBy="settings")
     * @Doctrine\ORM\Mapping\JoinColumn(onDelete="cascade")
     * @var Section
     */
    private $section;

    public function getSection() : Section
    {
        return $this->section;
    }

    public function update(string $value) : void
    {
        $this->value = $value;
    }
}
