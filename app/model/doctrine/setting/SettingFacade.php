<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Setting;

use App\Model\Doctrine\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class SettingFacade extends Facade
{
    /** @var string */
    private $tableSection;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table        = Setting::class;
        $this->tableSection = Section::class;
    }

    /**
     * @return Setting[]
     */
    public function getAll() : array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    /**
     * @return Section[]
     */
    public function getAllSections() : array
    {
        return $this->em
            ->getRepository($this->tableSection)
            ->findAll();
    }

    public function update(int $id, string $value) : Setting
    {
        $setting = $this->get($id);

        $setting->update($value);
        $this->em->persist($setting);
        $this->em->flush();

        return $setting;
    }

    protected function get(?int $id = null) : Setting
    {
        /** @var Setting $setting */
        $setting = parent::get($id);
        return $setting;
    }

    public function findByCode(string $code) : ?Setting
    {
        $criteria = ['code' => $code];

        /** @var Setting|null $setting */
        $setting = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $setting;
    }
}
