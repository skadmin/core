<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Facebook
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $facebook = '';

    public function getFacebook() : string
    {
        return $this->facebook;
    }
}
