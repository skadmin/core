<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTimeInterface;
use function sprintf;

/**
 * Trait Term
 *
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
trait Term
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime")
     * @var DateTimeInterface
     */
    private $termFrom;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime")
     * @var DateTimeInterface
     */
    private $termTo;

    public function getTermFrom() : DateTimeInterface
    {
        return $this->termFrom;
    }

    public function getTermTo() : DateTimeInterface
    {
        return $this->termTo;
    }

    public function getTermClever() : string
    {
        if ($this->termFrom->format('Y') === $this->termTo->format('Y')) {
            if ($this->termFrom->format('m') === $this->termTo->format('m')) {
                if ($this->termFrom->format('d') === $this->termTo->format('d')) {
                    return $this->termTo->format('d.m.Y');
                }

                return sprintf('%s - %s', $this->termFrom->format('d.'), $this->termTo->format('d.m.Y'));
            }

            return sprintf('%s - %s', $this->termFrom->format('d.m.'), $this->termTo->format('d.m.Y'));
        }

        return sprintf('%s - %s', $this->termFrom->format('d.m.Y'), $this->termTo->format('d.m.Y'));
    }

    public function getTermWithTimeClever() : string
    {
        if ($this->termFrom->format('Y') === $this->termTo->format('Y')) {
            if ($this->termFrom->format('m') === $this->termTo->format('m')) {
                if ($this->termFrom->format('d') === $this->termTo->format('d')) {
                    return sprintf('%s - %s', $this->termFrom->format('d.m.Y H:i'), $this->termTo->format('H:i'));
                }

                return sprintf('%s - %s', $this->termFrom->format('d. H:i'), $this->termTo->format('d.m.Y H:i'));
            }

            return sprintf('%s - %s', $this->termFrom->format('d.m. H:i'), $this->termTo->format('d.m.Y H:i'));
        }

        return sprintf('%s - %s', $this->termFrom->format('d.m.Y H:i'), $this->termTo->format('d.m.Y H:i'));
    }

    public function getTermFromTo(string $separator = ' - ') : string
    {
        return sprintf('%s%s%s', $this->termFrom->format('d.m.Y'), $separator, $this->termTo->format('d.m.Y'));
    }
}
