<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits\Facade;

use function date;
use function str_pad;

trait VariableSymbol
{
    public function getValidVariableSymbol(?int $prefix = null) : string
    {
        if ($prefix === null) {
            $prefix = date('Ym');
        }

        $variableSymbol = $this->em
                              ->getRepository($this->table)
                              ->createQueryBuilder('a')
                              ->select('max(a.variableSymbol) as variableSymbol')
                              ->where('a.variableSymbol LIKE :variableSymbol')
                              ->setParameter('variableSymbol', $prefix . '%')
                              ->getQuery()
                              ->getSingleResult()['variableSymbol'];

        if ($variableSymbol === null) {
            $variableSymbol = str_pad((string) $prefix, 10, '0');
        } else {
            $variableSymbol++;
        }

        return (string) $variableSymbol;
    }
}
