<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits\Facade;

use Nette\Utils\Strings;
use function sprintf;

trait Webalize
{
    public function getValidWebalize(string $name) : string
    {
        $webalizeName = Strings::webalize($name);
        $webalize     = $webalizeName;
        $cnt          = 2;
        while ($this->existWithWebalize($webalize)) {
            $webalize = sprintf('%s-%d', $webalizeName, $cnt++);
        }

        return $webalize;
    }

    public function existWithWebalize(string $webalize) : bool
    {
        $criteria = ['webalize' => $webalize];

        $tournament = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $tournament !== null;
    }
}
