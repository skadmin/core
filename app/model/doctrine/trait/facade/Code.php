<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits\Facade;

use Nette\Utils\Random;
use Nette\Utils\Strings;

trait Code
{
    public function getValidCode() : string
    {
        $code = Strings::upper(Random::generate());
        while ($this->existWithCode($code)) {
            $code = Strings::upper(Random::generate());
        }

        return $code;
    }

    public function existWithCode(string $code) : bool
    {
        $criteria = ['code' => $code];

        $object = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $object !== null;
    }
}
