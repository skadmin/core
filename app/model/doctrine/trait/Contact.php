<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Contact
{
    use Email;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $phone = '';

    public function getPhone() : string
    {
        return $this->phone;
    }
}
