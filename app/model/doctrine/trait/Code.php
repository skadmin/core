<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function trim;

trait Code
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $code = '';

    public function getCode() : string
    {
        return $this->code;
    }

    public function setCode(string $code) : void
    {
        if (trim($this->code) !== '') {
            return;
        }

        $this->code = $code;
    }
}
