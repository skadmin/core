<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Description
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $description;

    public function getDescription() : string
    {
        return $this->description;
    }
}
