<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTime;
use DateTimeInterface;

/**
 * Trait LastUpdate
 *
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
trait Created
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime")
     * @var DateTimeInterface
     */
    private $createdAt;

    public function getCreatedAt() : DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Gets triggered every time on update
     *
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPrePersistCreated() : void
    {
        $this->createdAt = new DateTime();
    }
}
