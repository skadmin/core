<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait ImagePreview
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string", nullable=true)
     * @var string|null
     */
    private $imagePreview = '';

    public function getImagePreview() : ?string
    {
        if ($this->imagePreview !== null) {
            return $this->imagePreview === '' ? null : $this->imagePreview;
        }
        return $this->imagePreview;
    }
}
