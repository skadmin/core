<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Position
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $position = '';

    public function getPosition() : string
    {
        return $this->position;
    }
}
