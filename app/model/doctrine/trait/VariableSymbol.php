<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait VariableSymbol
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $variableSymbol = '';

    public function getVariableSymbol() : string
    {
        return $this->variableSymbol;
    }

    public function setVariableSymbol(string $variableSymbol) : void
    {
        if ($this->variableSymbol !== '') {
            return;
        }

        $this->variableSymbol = $variableSymbol;
    }
}
