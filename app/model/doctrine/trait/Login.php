<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Login
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $userName;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $userPassword;

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function getUserPassword() : string
    {
        return $this->userPassword;
    }
}
