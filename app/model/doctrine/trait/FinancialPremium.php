<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait FinancialPremium
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $financialPremium = '0';

    public function getFinancialPremium() : string
    {
        return $this->financialPremium;
    }
}
