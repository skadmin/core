<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Status
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $status;

    public function getStatus() : ?int
    {
        return $this->status;
    }
}
