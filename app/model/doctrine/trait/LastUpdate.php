<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTime;
use DateTimeInterface;

/**
 * Trait LastUpdate
 *
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
trait LastUpdate
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $lastUpdateAuthor;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var DateTimeInterface
     */
    private $lastUpdateAt;

    public function getLastUpdateAuthor() : string
    {
        return $this->lastUpdateAuthor;
    }

    public function getLastUpdateAt() : DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    /**
     * Gets triggered every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPreUpdateLastUpdate() : void
    {
        $this->lastUpdateAt = new DateTime();
    }
}
