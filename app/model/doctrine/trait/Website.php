<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function trim;

trait Website
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $website = '';

    public function getWebsite() : string
    {
        return trim($this->website);
    }
}
