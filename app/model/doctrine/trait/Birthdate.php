<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTimeInterface;

/**
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
trait Birthdate
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $birthdate;

    /**
     * @return DateTimeInterface|string|null
     */
    public function getBirthdate(?string $format = null)
    {
        if ($format === null || $this->birthdate === null) {
            return $this->birthdate;
        }

        return $this->birthdate->format($format);
    }
}
