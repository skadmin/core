<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function sprintf;

trait PlaceOnTheMap
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $placeName = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $placeAddress = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $placeGpsLat = ''; // 49.560852

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $placeGpsLng = ''; // 16.291169

    public function getPlaceName() : string
    {
        return $this->placeName;
    }

    public function getPlaceAddress() : string
    {
        return $this->placeAddress;
    }

    public function getPlaceLinkToMap() : string
    {
        return sprintf(
            'https://mapy.cz/zakladni?x=%s&y=%s&z=14&source=coor&id=%s,%s',
            $this->getPlaceGpsLng(),
            $this->getPlaceGpsLat(),
            $this->getPlaceGpsLng(),
            $this->getPlaceGpsLat()
        );
    }

    public function getPlaceGpsLng() : string
    {
        return $this->placeGpsLng;
    }

    public function getPlaceGpsLat() : string
    {
        return $this->placeGpsLat;
    }

    public function isSetPlace() : bool
    {
        return $this->getPlaceGpsLng() !== '' && $this->getPlaceGpsLat() !== '';
    }
}
