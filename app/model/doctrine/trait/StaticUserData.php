<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait StaticUserData
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $staticName = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $staticEmail = '';

    public function getStaticName() : string
    {
        return $this->staticName;
    }

    public function getStaticEmail() : string
    {
        return $this->staticEmail;
    }
}
