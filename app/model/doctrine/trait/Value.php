<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Value
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $value = '';

    public function getValue() : string
    {
        return $this->value;
    }
}
