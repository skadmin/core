<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use Nette\Utils\Strings;
use function trim;

/**
 * Trait WebalizeName
 *
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
trait WebalizeName
{
    use Name;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $webalize = '';

    public function getWebalize() : string
    {
        return $this->webalize;
    }

    public function setWebalize(string $webalize) : void
    {
        if (trim($this->webalize) !== '') {
            return;
        }

        $this->webalize = $webalize;
    }

    /**
     * Fire trigger every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPreUpdateWebalizeName() : void
    {
        $this->setWebalize(Strings::webalize($this->getName()));
    }
}
