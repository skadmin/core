<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Id
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="integer", nullable=FALSE)
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue
     * @var int|null
     */
    private $id;

    public function getId() : ?int
    {
        return $this->id;
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function isLoaded() : bool
    {
        return $this->id > 0;
    }
}
