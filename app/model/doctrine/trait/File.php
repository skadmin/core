<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTime;
use DateTimeInterface;
use function sprintf;

trait File
{
    use Name;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string", nullable=true)
     * @var string|null
     */
    private $identifier = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $hash = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $size = 0;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $mimeType = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     * @var DateTimeInterface
     */
    private $lastUpdateAt;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     * @var DateTimeInterface
     */
    private $createdAt;

    public function getIdentifier() : ?string
    {
        return $this->identifier;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function getSize() : int
    {
        return $this->size;
    }

    public function getSizeInUnit() : string
    {
        $size = $this->size;

        foreach (['kB', 'MB', 'GB', 'TB'] as $unit) {
            $size /= 1024;

            if ($size < 1024) {
                return sprintf('%.02f %s', $size, $unit);
            }
        }

        return sprintf('%.02f %s', $size, $unit);
    }

    public function getMimeType() : string
    {
        return $this->mimeType;
    }

    public function getLastUpdateAt() : DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    public function getCreatedAt() : DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Gets triggered every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     */
    public function onPreUpdateFile() : void
    {
        $this->lastUpdateAt = new DateTime();
    }

    /**
     * Gets triggered every time on update
     *
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPrePersistFile() : void
    {
        $this->lastUpdateAt = new DateTime();
        $this->createdAt    = new DateTime();
    }
}
