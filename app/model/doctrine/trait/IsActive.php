<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function boolval;

trait IsActive
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", options={"default" : 1})
     * @var bool
     */
    private $isActive = true;

    public function isActive() : bool
    {
        return boolval($this->isActive);
    }

    /**
     * @param bool|int $isActive
     */
    public function setIsActive($isActive) : void
    {
        $this->isActive = boolval($isActive);
    }
}
