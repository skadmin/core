<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function sprintf;

trait HumanName
{
    use Name;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $surname = '';

    /**
     * First "surname"
     */
    public function getFullNameReverse() : string
    {
        return $this->getFullName(true);
    }

    /**
     * First "name" OR with $reverse[true] first "surname"
     */
    public function getFullName(bool $reverse = false) : string
    {
        if ($reverse) {
            return sprintf('%s %s', $this->getSurname(), $this->getName());
        }
        return sprintf('%s %s', $this->getName(), $this->getSurname());
    }

    public function getSurname() : string
    {
        return $this->surname;
    }
}
