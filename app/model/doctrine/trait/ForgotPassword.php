<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTime;
use DateTimeInterface;

trait ForgotPassword
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string", nullable=true, length=32)
     * @var ?string
     */
    private $forgotPasswordHash = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var ?DateTimeInterface
     */
    private $forgotPasswordDate = null;

    public function getForgotPasswordHash() : ?string
    {
        return $this->forgotPasswordHash;
    }

    public function setForgotPasswordHash(string $forgotPasswordHash) : void
    {
        $this->forgotPasswordHash = $forgotPasswordHash;
        $forgotPasswordDate       = new DateTime();
        $forgotPasswordDate->modify('+1 hour');
        $this->forgotPasswordDate = $forgotPasswordDate;
    }

    public function getForgotPasswordDate() : ?DateTimeInterface
    {
        return $this->forgotPasswordDate;
    }

    public function resetForgotPassword() : void
    {
        $this->forgotPasswordHash = null;
        $this->forgotPasswordDate = null;
    }
}
