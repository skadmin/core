<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function trim;

trait Hash
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $hash = '';

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setHash(string $hash) : void
    {
        if (trim($this->hash) !== '') {
            return;
        }

        $this->hash = $hash;
    }
}
