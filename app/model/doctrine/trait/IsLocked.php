<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function boolval;

trait IsLocked
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", options={"default" : 0})
     * @var bool
     */
    private $isLocked = false;

    public function isLocked() : bool
    {
        return boolval($this->isLocked);
    }

    /**
     * @param bool|int $isLocked
     */
    public function setIsLocked($isLocked) : void
    {
        $this->isLocked = boolval($isLocked);
    }
}
