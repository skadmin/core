<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Name
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $name = '';

    public function getName() : string
    {
        return $this->name;
    }
}
