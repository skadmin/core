<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Title
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $title;

    public function getTitle() : string
    {
        return $this->title;
    }
}
