<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Content
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $content;

    public function getContent() : string
    {
        return $this->content;
    }
}
