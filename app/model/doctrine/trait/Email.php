<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use Nette\Utils\Validators;

trait Email
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $email = '';


    public function getEmail() : string
    {
        return $this->email;
    }

    public function setEmail(string $email) : void
    {
        $this->email = Validators::isEmail($email) ? $email : '';
    }
}
