<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Nickname
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $nickname = '';

    public function getNickname() : string
    {
        return $this->nickname;
    }
}
