<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Number
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $number = 0;

    public function getNumber() : int
    {
        return $this->number;
    }
}
