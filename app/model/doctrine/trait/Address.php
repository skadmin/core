<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function sprintf;

trait Address
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $address = '';

    public function getAddressLinkToMap() : string
    {
        return sprintf(
            'https://mapy.cz/zakladni?q=%s',
            $this->getAddress()
        );
    }

    public function getAddress() : string
    {
        return $this->address;
    }
}
