<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait FinancialReward
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $financialReward = 0;

    public function getFinancialReward() : int
    {
        return $this->financialReward;
    }
}
