<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use function boolval;

trait IsPublicFacebook
{
    use Facebook;

    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", options={"default" : 0})
     * @var bool
     */
    private $isPublicFacebook = false;

    public function getFacebook(bool $force = false) : string
    {
        if ($force || $this->isPublicFacebook()) {
            return $this->facebook;
        }

        return '';
    }

    public function isPublicFacebook() : bool
    {
        return boolval($this->isPublicFacebook);
    }

    /**
     * @param bool|int $isPublicFacebook
     */
    public function setIsPublicFacebook($isPublicFacebook) : void
    {
        $this->isPublicFacebook = boolval($isPublicFacebook);
    }
}
