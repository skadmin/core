<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

trait Sequence
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $sequence;

    public function getSequence() : ?int
    {
        return $this->sequence;
    }

    public function setSequence(int $sequence) : void
    {
        if ($this->sequence !== null) {
            return;
        }

        $this->sequence = $sequence;
    }

    public function updateSequence(int $sequence) : void
    {
        $this->sequence = $sequence;
    }
}
