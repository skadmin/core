<?php

declare(strict_types=1);

namespace App\Model\Doctrine\Traits;

use DateTimeInterface;
use Nette\Utils\DateTime;

/**
 * Trait Validity
 *
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
trait Validity
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime")
     * @var DateTimeInterface
     */
    private $validityFrom = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $validityTo = null;

    public function getValidityFrom() : DateTimeInterface
    {
        return $this->validityFrom;
    }

    public function getValidityTo(bool $force = false) : ?DateTimeInterface
    {
        if ($force || $this->validityTo) {
            return $this->validityTo;
        }

        return (new DateTime())->modify('+1 year');
    }
}
