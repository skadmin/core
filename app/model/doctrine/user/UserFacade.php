<?php

declare(strict_types=1);

namespace App\Model\Doctrine\User;

use App\Model\Doctrine\Facade;
use App\Model\Doctrine\Role\Role;
use DateTimeInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nette\Security\Passwords;
use Nette\Security\User as LoggedUser;
use Nette\Utils\Random;
use Nettrine\ORM\EntityManagerDecorator;
use function md5;
use function sprintf;
use function time;

final class UserFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = User::class;
    }

    /**
     * @param Role[] $roles
     */
    public function update(int $id, string $name, string $surname, string $phone, string $email, string $facebook, string $address, ?DateTimeInterface $birthdate, string $note, string $experience, bool $isActive, array $roles, LoggedUser $loggedUser) : User
    {
        $user = $this->get($id);
        $user->update($name, $surname, $phone, $email, $facebook, $address, $birthdate, $note, $experience, $isActive, $roles, $loggedUser);

        $this->em->flush();
        return $user;
    }

    public function get(?int $id = null) : User
    {
        if ($id === null) {
            return new User();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new User();
        }

        return $user;
    }

    public function updateAccount(int $id, string $name, string $surname, string $phone, string $email, string $facebook, string $address, ?DateTimeInterface $birthdate, string $note, string $experience, ?string $imagePreview) : User
    {
        $user = $this->get($id);
        $user->updateAccount($name, $surname, $phone, $email, $facebook, $address, $birthdate, $note, $experience, $imagePreview);

        $this->em->flush();
        return $user;
    }

    public function getModelForGrid() : QueryBuilder
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository($this->table);
        return $repository->createQueryBuilder('u')
            ->leftJoin('u.roles', 'r');
    }

    /**
     * @return User[]
     */
    public function getActiveUsers() : array
    {
        $criteria = ['isActive' => true];
        $orderBy  = ['surname' => 'ASC'];

        /** @var User[] $users */
        $users = $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);

        return $users;
    }

    /**
     * @param string $password in plain text
     * @param Role[] $roles
     */
    public function create(string $username, string $password, string $name, string $surname, string $phone, string $email, string $facebook, string $address, ?DateTimeInterface $birthdate, string $note, string $experience, bool $isActive, array $roles, bool $needsApproval = false) : ?User
    {
        if ($this->findByUsername($username) === null) {
            $user         = $this->get();
            $userPassword = (new Passwords())->hash($password);
            $user->create($username, $userPassword, $name, $surname, $phone, $email, $facebook, $address, $birthdate, $note, $experience, $isActive, $roles, $needsApproval);

            $this->em->persist($user);
            $this->em->flush();
            return $user;
        }

        return null;
    }

    public function findByUsername(string $username) : ?User
    {
        $criteria = ['userName' => $username];

        /** @var User|null $user */
        $user = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $user;
    }

    public function findByEmail(string $email) : ?User
    {
        $criteria = ['email' => $email];

        /** @var User|null $user */
        $user = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $user;
    }

    public function findByForgotPasswordHash(string $hash) : ?User
    {
        $criteria = ['forgotPasswordHash' => $hash];

        /** @var User|null $user */
        $user = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $user;
    }

    public function updatePassword(int $id, string $password) : User
    {
        $user = $this->get($id);
        return $this->updatePasswordByUser($user, $password);
    }

    public function updatePasswordByUser(User $user, string $password) : User
    {
        $userPassword = (new Passwords())->hash($password);
        $user->updatePassword($userPassword);

        $this->em->flush();
        return $user;
    }

    public function createForgotPasswordByUser(User $user) : User
    {
        $hash = md5(sprintf('%s%s%s', $user->getId(), time(), Random::generate()));
        $user->setForgotPasswordHash($hash);
        $this->em->flush();

        return $user;
    }

    public function authorize(User $user) : User
    {
        $user->authorize();

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
