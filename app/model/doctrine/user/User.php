<?php

declare(strict_types=1);

namespace App\Model\Doctrine\User;

use App\Model\Doctrine\Role\Role;
use App\Model\Doctrine\Traits;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\Security\IIdentity;
use Nette\Security\User as LoggedUser;
use function count;

//@Doctrine\ORM\Mapping\Cache(usage="NONSTRICT_READ_WRITE", region="admin_user")

/**
 * Class User
 *
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class User implements IIdentity
{
    public const DIR_IMAGE = 'user';
    use Traits\Id;
    use Traits\Login;
    use Traits\ForgotPassword;
    use Traits\HumanName;
    use Traits\Contact;
    use Traits\IsLocked;
    use Traits\IsActive;
    use Traits\Facebook;
    use Traits\Address;
    use Traits\Birthdate;
    use Traits\ImagePreview;

    /**
     * @Doctrine\ORM\Mapping\Column(type="boolean", options={"default" : 0})
     * @var bool
     */
    private $needsApproval = false;

    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $note = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $experience = '';

    // ASOCIACE

    /**
     * @Doctrine\ORM\Mapping\ManyToMany(targetEntity="App\Model\Doctrine\Role\Role", inversedBy="users")
     * @Doctrine\ORM\Mapping\JoinTable(
     *     name="user_role_rel"
     * )
     * @var ArrayCollection
     */
    private $roles;

    /** @var string[] */
    private $rolesLoaded = [];

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return Role[]
     */
    public function getRolesObject() : array
    {
        $roles = [];

        /** @var Role $role */
        foreach ($this->roles as $role) {
            $roles[] = $role;
        }

        return $roles;
    }

    public function loadUserRoles() : void
    {
        $this->rolesLoaded = $this->getRoles();
    }

    /**
     * @return string[]
     */
    public function getRoles() : array
    {
        $roles = [];

        /** @var Role $role */
        foreach ($this->roles as $role) {
            $roles[] = $role->getWebalize();
        }

        if (count($roles) === 0) {
            return $this->rolesLoaded;
        }

        return $roles;
    }

    /**
     * @param Role[] $roles
     */
    public function setRoles(array $roles, ?LoggedUser $loggedUser = null) : void
    {
        $t_roles = $this->roles;

        /** @var Role $role */
        foreach ($t_roles as $role) {
            if (($role->isMasterAdmin() && $this->isLocked())) {
                continue;
            } elseif ($loggedUser !== null && $role->isMasterAdmin() && ! $loggedUser->isInRole($role->getWebalize())) {
                continue;
            }

            $this->roles->removeElement($role);
        }

        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    private function addRole(Role $role) : void
    {
        if ($this->roles->contains($role)) {
            return;
        }

        $this->roles->add($role);
    }

    /**
     * @param Role[] $roles
     */
    public function create(string $userName, string $userPassword, string $name, string $surname, string $phone, string $email, string $facebook, string $address, ?DateTimeInterface $birthdate, string $note, string $experience, bool $isActive, array $roles, bool $needsApproval = false) : void
    {
        $this->userName     = $userName;
        $this->userPassword = $userPassword;
        $this->update($name, $surname, $phone, $email, $facebook, $address, $birthdate, $note, $experience, $isActive, $roles);

        $this->needsApproval = $needsApproval;
    }

    /**
     * @param Role[] $roles
     */
    public function update(string $name, string $surname, string $phone, string $email, string $facebook, string $address, ?DateTimeInterface $birthdate, string $note, string $experience, bool $isActive, array $roles, ?LoggedUser $loggedUser = null) : void
    {
        $this->name    = $name;
        $this->surname = $surname;
        $this->phone   = $phone;
        $this->email   = $email;

        $this->facebook   = $facebook;
        $this->address    = $address;
        $this->birthdate  = $birthdate;
        $this->note       = $note;
        $this->experience = $experience;

        $this->setIsActive($isActive);

        $this->setRoles($roles, $loggedUser);
    }

    public function updateAccount(string $name, string $surname, string $phone, string $email, string $facebook, string $address, ?DateTimeInterface $birthdate, string $note, string $experience, ?string $imagePreview) : void
    {
        $this->name       = $name;
        $this->surname    = $surname;
        $this->phone      = $phone;
        $this->email      = $email;
        $this->facebook   = $facebook;
        $this->address    = $address;
        $this->birthdate  = $birthdate;
        $this->note       = $note;
        $this->experience = $experience;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function updatePassword(string $userPassword) : void
    {
        $this->userPassword = $userPassword;
        $this->resetForgotPassword();
    }

    public function getNote() : string
    {
        return $this->note;
    }

    public function getExperience() : string
    {
        return $this->experience;
    }

    public function isNeedsApproval() : bool
    {
        return $this->needsApproval;
    }

    public function authorize() : void
    {
        $this->needsApproval = false;
        $this->isActive      = true;
    }

    public function getAllowedGames() : ArrayCollection
    {
        return new ArrayCollection();
    }
}
