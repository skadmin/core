<?php

declare(strict_types=1);

namespace App\Model\Grid\Traits;

use App\Components\Grid\GridDoctrine;
use App\Model\System\Constant;
use function count;
use function sprintf;

trait IsActive
{
    /** @var string[] */
    private $replacementIsActive = [];

    private function addColumnIsActive(GridDoctrine &$grid, string $infix) : void
    {
        $grid->addColumnText('isActive', sprintf('grid.%s.is-active', $infix))
            ->setAlign('center')
            ->setReplacement($this->getReplacementIsActive());
    }

    /**
     * @return string[]
     */
    private function getReplacementIsActive() : array
    {
        if (count($this->replacementIsActive) === 0) {
            foreach (Constant::DIAL_YES_NO as $keyDial => $valueDial) {
                $this->replacementIsActive[$keyDial] = $this->translator->translate($valueDial);
            }
        }

        return $this->replacementIsActive;
    }

    private function addFilterIsActive(GridDoctrine &$grid, string $infix) : void
    {
        $grid->addFilterSelect('isActive', sprintf('grid.%s.is-active', $infix), $this->getReplacementIsActive())
            ->setPrompt(Constant::PROMTP);
    }
}
